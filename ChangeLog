2008-02-22  Andrew Cagney  <cagney@redhat.com>

	* MAINTAINERS: Update file format.

2007-04-27  Sami Wagiaalla  <swagiaal@redhat.com>

	* src/jni/org_gnu_glib_Value.c: Removed unnessessary creation of
	GlobalRef when setting the Value to a pointer to a java object;
	Closes bz#407591.

2007-01-07  Andrew Cowie  <andrew@operationaldynamics.com>

	* src/java/org/gnu/glib/*.java:
	Mark classes as deprecated
	* configure.ac:
	Version 0.4.2 (java-gnome 2.16.2 for GNOME 2.17.5)

2006-11-21  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Version 0.4.1 (java-gnome 2.16.1)
	* macros/jg_common.m4:
	Fix build system to work for non-packaging builds. Closes
	bug #377468.

2006-10-24  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Version 0.4.0 (java-gnome 2.16.0)

2006-10-17  Andrew Cowie  <andrew@operationaldynamics.com>

	* Makefile.am:
	* configure.ac:
	* glib-java-uninstalled.pc.in:
	* glib-java.pc.in:
	* src/jni/jg_jnu.c:
	Apply build system patches from John Rice <john.rice@sun.com>
	addressing build problems on Open Solaris. Fixes several glitches and
	inconsistencies, and updating .pc file, and using updated macro
	variables macrobasedir and docbasedir.  Adds new "-unistalled.pc" file
	supporting their internal build requirements. Closes bug #361899.

2006-10-17  Andrew Cowie  <andrew@operationaldynamics.com>

	* macros/jg_common.m4:
	Add macrodir and docbasedir variables to autoconf macros. From John
	Rice <john.rice@sun.com>, closes dependency on bug #360207.

2006-09-16  Sandor Bodo-Merle <sbodomerle@gmail.com>

	* src/jni/glib_java.c: (javatype_from_gtktype):
	* src/jni/org_gnu_glib_Value.c:
	s/sprintf/g_snprintf/. Fix a small memory leak.
	Fixes bug #337741.

2006-08-19  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Version 0.3.2

2006-08-16  Remy Suen  <remy.suen@gmail.com>

	* autogen.sh:
	* Makefile.am: Prevent 'make distcheck' from failing by including
	additional source files and folders in the build process.

2006-08-16  Remy Suen  <remy.suen@gmail.com>

	* Makefile.am: Apply patch from Sandor Bodo-Merle <sbodomerle@gmail.com>
	to help out-of-tree builds.

2006-08-12  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Version 0.3.1, apiversion 0.4

2006-07-16  Stepan Kasal  <kasal@ucw.cz>

	* acros/am_path_docbook.m4; Fix the definition of HAVE_DOCBOOK.

2006-07-09  Remy Suen  <remy.suen@gmail.com>

	* autogen.sh:
	* Makefile.am: Updating the build to include the newly imported glib
	classes.
	* src/java/org/gnu/glib/Boxed.java:
	* src/java/org/gnu/glib/Error.java:
	* src/java/org/gnu/glib/EventMap.java:
	* src/java/org/gnu/glib/EventType.java:
	* src/java/org/gnu/glib/Fireable.java:
	* src/java/org/gnu/glib/GEvent.java:
	* src/java/org/gnu/glib/GObject.java:
	* src/java/org/gnu/glib/Idle.java:
	* src/java/org/gnu/glib/JGException.java:
	* src/java/org/gnu/glib/List.java:
	* src/java/org/gnu/glib/MainLoop.java:
	* src/java/org/gnu/glib/MemStruct.java:
	* src/java/org/gnu/glib/ParmFlags.java:
	* src/java/org/gnu/glib/PropertyNotificationListener.java:
	* src/java/org/gnu/glib/Quark.java:
	* src/java/org/gnu/glib/SpawnError.java:
	* src/java/org/gnu/glib/Timre.java:
	* src/java/org/gnu/glib/Type.java:
	* src/java/org/gnu/glib/TypeInterface.java:
	* src/java/org/gnu/glib/Value.java:
	* src/jni/glib_java.c:
	* src/jni/glib_java.h:
	* src/jni/org_gnu_glib_Boxed.c:
	* src/jni/org_gnu_glib_Error.c:
	* src/jni/org_gnu_glib_GListString.c:
	* src/jni/org_gnu_glib_GObject.c:
	* src/jni/org_gnu_glib_Idle.c:
	* src/jni/org_gnu_glib_List.c:
	* src/jni/org_gnu_glib_MainLoop.c:
	* src/jni/org_gnu_glib_MemStruct.c:
	* src/jni/org_gnu_glib_Quark.c:
	* src/jni/org_gnu_glib_Timer.c:
	* src/jni/org_gnu_glib_Type.c:
	* src/jni/org_gnu_glib_Value.c: New files imported for the glib
	migration.

2006-07-09  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Update API version to 0.4 and release numbering to 0.3.x

2006-07-09  Andrew Cowie  <andrew@operationaldynamics.com>

	* macros/am_path_gcj.m4:
	Use -dumpversion instead of --version for determining what the GCJ
	version is.  Thanks to Joshua Nichols <nichoj@gentoo.org> for
	reporting and suggesting the fix at #345422, which this closes.

2006-05-29  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Version 0.2.5

2006-05-24  Remy Suen  <remy.suen@gmail.com>
	
	* macros/jg_check_nativecompile.m4: Changed the error message to correct
	a typo.

2006-05-24  Remy Suen  <remy.suen@gmail.com>
	
	* macros/jg_check_nativecompile.m4: Updated to acknowledge the
	--with-gcj-compile argument and actually build the native libraries.
	This fixes bug #336149.

2006-05-16  Rob Staudinger  <robert.staudinger@gmail.com>

	reviewed by: Andrew Cowie

	* Makefile.am:
	* macros/jg_common.m4:
	Add possibility to build source jars.

2006-05-11  Remy Suen  <remy.suen@gmail.com>

	* COPYING: Altered the license to be LGPL instead of GPL as it is
	specified in the source files.

2006-04-29  Remy Suen  <remy.suen@gmail.com>

	* src/jni/jg_jnu.c: Changed releaseStringArrayInGList to iterate through
	the list instead of using g_list_nth_data to heighten efficiency.

2006-03-26  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Version 0.2.4

2006-03-21  Andrew Cowie  <andrew@operationaldynamics.com>

	* .cvsignore:
	* src/java/org/gnu/glib/.cvsignore:
	* src/jni/.cvsignore:
	Further ignore generated files. Patch from Emmanuel
	Rodriguez. Closes #334982

2006-03-19  Andrew Cowie  <andrew@operationaldynamics.com>

	* .project:
	Change name element to glib-java. Thanks to Emmanuel Rodriguez for the
	report. Closes #334984


2006-03-14  Andrew Cowie  <andrew@operationaldynamics.com>

	* src/jni/jg_jnu.c:
	Documented and fixed potential crasher in JNU_ThrowByName()

2006-02-28  Adam Jocksch  <ajocksch@redhat.com>

	* Makefile.am (AM_CFLAGS): Added -Isrc/jni to fix out of tree builds.

2006-02-27  Adam Jocksch  <ajocksch@redhat.com>

	* src/jni/jg_jnu.c (JG_JNU_GetEnv): Added void to argument list.
	(jg_process_atexit): Ditto.
	* src/jni/jg_jnu.h: Ditto.
	* src/jni/org_gnu_glib_Struct.c: Added include for
	org_gnu_glib_Struct.h.

	* src/jni/jg_jnu.c (getPointerField): New Function.
	(JG_JNU_GetEnv): Changed type of env to void*.
	(getPointerFromHandle): Now uses getPointerField.
	(getHandleFromPointer): Ditto.
	(getPointerArrayFromHandles): Ditto.
	(getGSListFromHandles): Ditto.
	(getGListFromHandles): Ditto.
	(updateHandle): Ditto.
	(getHandleClass): Ditto.

	* configure.ac: Added check for JAVAH.
	* Makefile.am (AM_CFLAGS): Replace libglibjni_la_CFLAGS.
	(BUILT_SOURCES): Add glib$(apiversion).jar.
	(src/jni/org_gnu_glib_Struct.h): Add rule.
	(src/jni/org_gnu_glib_Struct.lo): Add explicit dependency on .h.
	(CLEANFILES): Add src/jni/org_gnu_glib_Struct.h.

2006-02-12  Andrew Cowie  <andrew@operationaldynamics.com>

	* src/java/org/gnu/glib/Enum.java:
	* src/java/org/gnu/glib/Flags.java:
	* src/java/org/gnu/glib/Handle.java:
	* src/java/org/gnu/glib/Handle32Bits.java:
	* src/java/org/gnu/glib/Handle64Bits.java:
	* src/java/org/gnu/glib/Struct.java:
	Apply Eclipse's "Java Default" style code formatting to all Java
	source files.

2006-01-30  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	Version 0.2.3

2006-01-29  Andrew Cowie  <andrew@operationaldynamics.com>

	* Makefile.am:
	* configure.ac:
	* macros/jg_common.m4:
	Add autoconf foo to allow glib-java to be built on Cygwin. Involves
	setting SOPREFIX and adding cygwin target. Left glib dependency at >=
	2.7.0 as currently set, but otherwise patch as submitted by
	<yselkowitz@users.sourceforge.net>. Closes #324665.

2005-12-06  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	version 0.2.2

2005-11-28  Andrew Cowie  <andrew@operationaldynamics.com>

	* macros/jg_common.m4:
	Fix autoconf macro so --with-javadocs and --without-javadocs both work.
	Patch submitted by Saleem Abdulrasool <compnerd@gentoo.org>

2005-11-26  Ismael Juma  <ismael@juma.me.uk>

	* src/jni/jg_jnu.c:
	* src/jni/jg_jnu.h:
	
	Fix bug #321553, patch from Sandor Bodo-Merle <sbodomerle@gmail.com>.

2005-10-29  Ismael Juma <ismael@juma.me.uk>

	* src/jni/jg_jnu.c (getJavaStringArray): Return NULL if str is NULL. 

2005-10-25  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	version 0.2.1

2005-10-11  Ismael Juma <ismael@juma.me.uk>

	* Makefile.am: Remove Config.java from native-only source list.

2005-10-09  Ismael Juma <ismael@juma.me.uk>

	* Makefile.am: Add Config.java to sources list.

2005-10-04  Andrew Cowie  <andrew@operationaldynamics.com>

	* configure.ac:
	version 0.2.0

2005-09-13  Andrew Cowie  <andrew@operationaldynamics.com>

	* Makefile.am:
	* autogen.sh:
	* configure.ac:
	* glib-java.pc.in:
	* macros/jg_lib.m4:
	* src/java/org/gnu/glib/.cvsignore:
	* src/java/org/gnu/glib/Config.java.in:
	* src/java/org/gnu/glib/Enum.java: (Enum), (Enum.Enum),
	(Enum.getValue), (Enum.hashCode), (Enum.equals):
	* src/java/org/gnu/glib/Flags.java: (Flags), (Flags.Flags),
	(Flags.getValue), (Flags.hashCode), (Flags.equals):
	* src/java/org/gnu/glib/Handle.java: (Handle):
	* src/java/org/gnu/glib/Handle32Bits.java: (Handle32Bits),
	(Handle32Bits.isNull), (Handle32Bits.equals),
	(Handle32Bits.toString), (Handle32Bits.getProxiedObject),
	(Handle32Bits.setProxiedObject):
	* src/java/org/gnu/glib/Handle64Bits.java: (Handle64Bits),
	(Handle64Bits.isNull), (Handle64Bits.equals),
	(Handle64Bits.toString), (Handle64Bits.getProxiedObject),
	(Handle64Bits.setProxiedObject):
	* src/java/org/gnu/glib/Struct.java: (Struct), (Struct.Struct),
	(Struct.getHandle), (Struct.equals), (Struct.hashCode),
	(Struct.setHandle):
	* src/jni/jg_jnu.c:
	* src/jni/org_gnu_javagnome_Struct.c:
	Rename library to be glib-java
	Move org.gnu.javagnome.Handle back to org.gnu.glib.Handle
	* src/java/org/gnu/javagnome/.cvsignore:
	* src/java/org/gnu/javagnome/Config.java.in:
	* src/java/org/gnu/javagnome/Enum.java:
	* src/java/org/gnu/javagnome/Flags.java:
	* src/java/org/gnu/javagnome/Handle.java:
	* src/java/org/gnu/javagnome/Handle32Bits.java:
	* src/java/org/gnu/javagnome/Handle64Bits.java:
	* src/java/org/gnu/javagnome/Struct.java:
	* jg-common.pc.in:
	Deleted

2005-06-05  Ismael Juma  <ismael@juma.me.uk>

	* src/jni/jg_jnu.h: Add more generic versions of the
	getHandleArrayFromPointers, getHandleArrayFromGList and
	getHandleArrayFromGSList functions.
	* src/jni/jg_jnu.c: Likewise.

2005-06-03  Nicholas Rahn  <nick@mobiledude.com>

	* src/jni/jg_jnu.c:
	* src/jni/jg_jnu.h:
	New getGListFromStringArray, releaseStringArrayInGList functions.
	
2005-05-24  Nicholas Rahn  <nick@mobiledude.com>

	* macros/am_path_gcj.m4: Fixes for GCJ detection that only show up 
	when configuring in Windows.

2005-05-12  Ismael Juma  <ismael@juma.me.uk>

	* src/java/org/gnu/javagnome/Struct.java: Add and implement init().
	* src/jni/org_gnu_javagnome_Struct.c: Likewise.
	* src/jni/jg_jnu.h: Add functions.
	* src/jni/jg_jnu.c (jg_process_atexit): Add and implement function.
	(jg_atexit): Likewise.
	Add #include <string.h> to eliminate gcc warning.
	(getPointerFromHandle): Do arguments checking first.
	
2005-05-11  Nicholas Rahn  <nick@mobiledude.com>

	* src/jni/jg_jnu.c: Make sure to set "it = it->next" in the for loops
	of getHandleArrayFromGList and getHandleArrayFromGSList.

2005-05-10  Ismael Juma  <ismael@juma.me.uk>

	* src/jni/jg_jnu.c: Updated various functions to use getHandleClass.
	Replaced the use of g_list_nth_data or g_slist_nth_data with iteration of
	the list instead. This is more efficient.
	(getHandleClass): Added function and implemented caching of Handle class.
	* src/jni/jg_gnu.h (getHandleClass): Added function. 
	* src/java/org/gnu/javagnome/Struct.java (setHandle): Also set proxied
	object.
	* src/java/org/gnu/javagnome/Handle64Bits.java (getProxiedObject): Add
	method.
	(setProxiedObject): Likewise.
	* src/java/org/gnu/javagnome/Handle32Bits.java (getProxiedObject): Likewise.
	(setProxiedObject): Likewise.
	* src/java/org/gnu/javagnome/Handle.java (setProxiedObject): Likewise.
	(getProxiedObject): Likewise.

2005-04-22  Jeffrey Morgan  <kuzman@gmail.com>

	* Makefile.am: installing all javadoc generated files.

2005-04-19	Ismael Juma <ismael@juma.me.uk>

	* src/jni/org_gnu_javagnome_Struct.c: Fix comment.

2005-04-16  Jeffrey Morgan  <kuzman@gmail.com>

	* macros/jg_lib.m4: changed to get variables out of jg_common
	
2005-04-13  Jeffrey Morgan  <kuzman@gmail.com>

	* Makefile.am:
	* configure.ac:
	* jg-common.pc.in:
	Updated to reflect jg-common name

2005-04-12  Jeffrey Morgan  <kuzman@gmail.com>

	* Makefile.am: Renaming package-config file
	* autogen.sh: Renaming project
	* configure.ac: increased glib requirement to 2.7 and renamed
	generated package-config file.
	* java-gnome.pc.in: removed
	* jg-common.pc.in: new file

2005-04-06  Nicholas Rahn  <nick@mobiledude.com>

	* macros/jg_common.m4: added the PLATFORM_CLASSPATH_SEPARATOR.
	* Makefile.am: classpath for java->bytecode modification to use a 
	more platform neutral format.

2005-04-02  Andrew Cowie  <andrew@operationaldynamics.com>

	* macros/jg_common.m4: fixed JNI_INCLUDE on linux for situations
	when people are not building with GCJ.

2005-03-28  Jeffrey Morgan  <kuzman@gmail.com>

	* src/jni/jg_jnu.c:
	* src/jni/jg_jnu.h:
	moved getPointerFromJavaGObject to libgtk-java

2005-03-25  Jeffrey Morgan  <kuzman@gmail.com>

	* configure.ac: setting platform specific variables.

2005-03-25  Jeffrey Morgan  <kuzman@gmail.com>

	* configure.ac: fixed name of jar file so .pc file contians
	correct information.

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* java-gnome.pc.in: fixed typo
	* src/jni/jg_jnu.c: changed class creation to create Handle
	from this project.

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* macros/jg_lib.m4: added check for pkg-config

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* macros/jg_lib.m4: new macro to find these libs
	* Makefile.am: installing new macro file

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* configure.ac: corrected apiversion and added macro dir
	* java-gnome.pc.in: added macro dir
	* Makefile.am: added javadoc creation and fixed install/uninstall

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* configure.ac: moved a lot of checks to jg_common.
	* Makefile.am: renamed a coupld of variables
	* macros/jg_common.m4: new file containing common checks

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* configure.ac: added checks for GLib
	* Makefile.am: setting GLIB_CFLAGS and GLIB_LIBS
	* src/jni/jg_jnu.c: removed GTK specific methods
	* src/jni/jg_jnu.h: removed GTK specific decls

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* autogen.sh: added macros directory back to aclocal
	* AUTHORS: new file
	* configure.ac: added checks for c_const, func_alloc,
	setting platform specific build flags, and checks for 
	java utilities.
	* COPYING:
	* INSTALL:
	* NEWS:
	* macros/ac_prog_jar.m4:
	* macros/ac_prog_javac_works.m4:
	* macros/ac_prog_javac.m4:
	* macros/ac_prog_javadoc.m4:
	* macros/am_path_docbook.m4:
	* am_path_gcj.m4:
	* jg_check_nativecompile.m4:
	new files

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* autogen.sh:  removed reference to macros directory

2005-03-24  Jeffrey Morgan  <kuzman@gmail.com>

	* java-gnome.pc.in: pkg-config file
	* Makefile.am: renamed Makefile.in

2005-03-23  Jeffrey Morgan  <kuzman@gmail.com>

	* Initial creation of new library under java-gnome
	cvs module
