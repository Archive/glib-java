ACLOCAL_AMFLAGS = -I macros

AUTOMAKE_OPTIONS = subdir-objects

BUILT_SOURCES =
CLEANFILES =
ALL_LOCAL_DEPS =

if BUILD_GCJ
lib_LTLIBRARIES = libglibjava.la libglibjni.la 
else
lib_LTLIBRARIES = libglibjni.la
endif

java_gnome_source_files = \
	src/java/org/gnu/glib/Boxed.java \
	src/java/org/gnu/glib/Config.java \
	src/java/org/gnu/glib/Enum.java \
	src/java/org/gnu/glib/Error.java \
	src/java/org/gnu/glib/EventMap.java \
	src/java/org/gnu/glib/EventType.java \
	src/java/org/gnu/glib/Fireable.java \
	src/java/org/gnu/glib/Flags.java \
	src/java/org/gnu/glib/GEvent.java \
	src/java/org/gnu/glib/GListString.java \
	src/java/org/gnu/glib/GObject.java \
	src/java/org/gnu/glib/Handle.java \
	src/java/org/gnu/glib/Handle32Bits.java \
	src/java/org/gnu/glib/Handle64Bits.java \
	src/java/org/gnu/glib/Idle.java \
	src/java/org/gnu/glib/JGException.java \
	src/java/org/gnu/glib/List.java \
	src/java/org/gnu/glib/MainLoop.java \
	src/java/org/gnu/glib/MemStruct.java \
	src/java/org/gnu/glib/ParmFlags.java \
	src/java/org/gnu/glib/PropertyNotificationListener.java \
	src/java/org/gnu/glib/Quark.java \
	src/java/org/gnu/glib/Struct.java \
	src/java/org/gnu/glib/Timer.java \
	src/java/org/gnu/glib/Type.java \
	src/java/org/gnu/glib/TypeInterface.java \
	src/java/org/gnu/glib/Value.java

# Force all the .class files to be built very early in the process --
# in particular before any attempt to generate .h files from those
# classes

BUILT_SOURCES += glib$(apiversion).jar

# Only need to make the .h file dependant on the main .class file, if
# any other implied dependency (such as referenced via include) is
# changed, the .class file will be updated automatically.

jni_gnome_source_files = src/jni/jg_jnu.c \
	src/jni/glib_java.c
include Makefile.jni

if BUILD_GCJ
libglibjava_la_SOURCES = $(java_gnome_source_files)
libglibjava_la_GCJFLAGS = -fjni -I$(srcdir)/src/java -Isrc/java
libglibjava_la_LDFLAGS = -no-undefined -release $(apiversion)
endif

libglibjni_la_SOURCES = $(jni_gnome_source_files) \
	src/jni/jg_jnu.h \
	src/jni/glib_java.h
# Don't use libglibjni_la_CFLAGS here; it forces automake to generate
# an individual build rule for every file .lo file (except,
# unfortunatly, when automake can see an existing rul1e for the file --
# such as the above .h.lo: rule).
AM_CFLAGS = $(GLIB_CFLAGS) $(GOBJECT_CFLAGS) $(PLATFORM_CFLAGS) $(JNI_INCLUDES) -Isrc/jni
libglibjni_la_LDFLAGS = $(GLIB_LIBS) $(GOBJECT_LIBS) $(PLATFORM_LDFLAGS) -no-undefined -release $(apiversion)

glib_jardir = $(jardir)
glib_jar_DATA = glib$(apiversion).jar

glib_jar_class_files = $(java_gnome_source_files:.java=.class)

$(glib_jar_class_files): %.class: %.java
	$(JAVAC) -classpath "src/java$(PLATFORM_CLASSPATH_SEPARATOR)$(srcdir)/src/java" -d src/java $<

# all_jar_class_files includes inner-class generated .class files
# the sed command protects dollar signs in file names from shell expansion
all_jar_class_files = $(shell find src/java -name '*.class' | sed 's%\$$%\\$$%g')

glib$(apiversion).jar: $(glib_jar_class_files)
	$(JAR) cf $@ $(patsubst src/java/%.class,-C src/java %.class,$(all_jar_class_files))

if BUILD_SRCJAR

CLEANFILES += glib$(apiversion)-src.jar
ALL_LOCAL_DEPS += glib$(apiversion)-src.jar

# Config.java is generated and therefore under $(builddir) rather $(srcdir)
# therefore it's added to the jar separately
srcjar_files=$(shell echo "$(java_gnome_source_files)" | sed -e 's|src/java/org/gnu/glib/Config.java||g')

glib$(apiversion)-src.jar:
	$(JAR) cf $@ $(patsubst src/java/%.java,-C $(top_srcdir)/src/java %.java,$(srcjar_files))
	$(JAR) uf $@ -C src/java org/gnu/glib/Config.java

glibjavasrcjardir = $(srcjardir)
glibjavasrcjar_DATA = glib$(apiversion)-src.jar

endif

# install the jg_jnu.h header file - needed by other bindings
glibjnudir = $(includedir)/glib-java
glibjnu_DATA = src/jni/jg_jnu.h \
	src/jni/glib_java.h

# install pkg-config file
pkgconfigdir = $(libdir)/pkgconfig
pkgconfig_DATA = glib-java.pc

# install the macros used by other java-gnome packages
glibmacrodir = $(macrobasedir)/glib-java/macros
glibmacro_DATA = \
	macros/jg_common.m4 \
	macros/jg_check_nativecompile.m4 \
	macros/am_path_gcj.m4 \
	macros/am_path_docbook.m4 \
	macros/ac_prog_javadoc.m4 \
	macros/ac_prog_javac_works.m4 \
	macros/ac_prog_javac.m4 \
	macros/ac_prog_jar.m4 \
	macros/jg_lib.m4

# build api docs
apidir = $(docbasedir)/glib-java-$(VERSION)/api

if BUILD_JAVADOC
all-local: $(top_builddir)/doc-stamp
else
all-local:
endif

if BUILD_JAVADOC
doc-stamp: $(java_gnome_source_files)
	$(JAVADOC) \
		-d doc/api \
		$(JAVADOC_OPTIONS) \
		-sourcepath $(srcdir)/src/java \
		-windowtitle "glib-java $(VERSION) API Reference" \
		-doctitle "glib-java $(VERSION) API Reference" \
		-footer "$(JAVADOC_FOOTER)" \
		org.gnu.glib
	touch doc-stamp
else
doc-stamp:
	touch doc-stamp
endif

# install other docs
docdir = $(docbasedir)/glib-java-$(VERSION)
doc_DATA = $(srcdir)/AUTHORS $(srcdir)/COPYING \
	$(srcdir)/NEWS $(srcdir)/README $(srcdir)/INSTALL

all_dest_javadoc_html_files = $(patsubst doc/api/%,%,$(shell find doc/api -type f))
dest_javadoc_dirs = $(patsubst doc/api/%,%,$(shell cd doc/api; find . -type d))

install-data-hook:
# install versionless jar symlink
	cd $(DESTDIR)$(glib_jardir) && \
		mv -f glib$(apiversion).jar glib$(apiversion)-$(VERSION).jar && \
		$(LN_S) glib$(apiversion)-$(VERSION).jar glib$(apiversion).jar

# install api documentation
if BUILD_JAVADOC
	$(mkinstalldirs) $(DESTDIR)$(apidir)
	@for dir in $(dest_javadoc_dirs); \
	do \
	   $(mkinstalldirs) $(DESTDIR)$(apidir)/$${dir}; \
	done
	@for file in $(all_dest_javadoc_html_files); \
	do \
	   $(INSTALL_DATA) doc/api/$${file} $(DESTDIR)$(apidir)/$${file}; \
	done
endif

uninstall-local:
	-rm -f $(DESTDIR)$(glib_jardir)/glib$(apiversion)-$(VERSION).jar
	-rm -rf $(DESTDIR)$(datadir)/doc/glib-java-$(VERSION)

BUILT_SOURCES += \
	glib-java.pc \
	src/java/org/gnu/glib/Config.java

EXTRA_DIST = \
	macros/jg_common.m4 \
	macros/jg_check_nativecompile.m4 \
	macros/am_path_gcj.m4 \
	macros/am_path_docbook.m4 \
	macros/ac_prog_javadoc.m4 \
	macros/ac_prog_javac_works.m4 \
	macros/ac_prog_javac.m4 \
	macros/ac_prog_jar.m4 \
	macros/jg_lib.m4 \
	src/java/org/gnu/glib/Config.java.in

# clean files and directories
CLEANFILES += \
	doc-stamp \
	src/java/org/gnu/glib/Config.class \
	$(all_jar_class_files) \
	$(shell find doc/api -name '*.html') \
	doc/api/package-list \
	doc/api/stylesheet.css \
	doc/api/resources/inherit.gif \
	glib$(apiversion).jar

clean-local:
	-rm -rf doc/api

# distclean files and directories
DISTCLEANFILES = \
	$(srcdir)/Makefile.jni \
	$(srcdir)/aclocal.m4 \
	$(srcdir)/compile \
	$(srcdir)/config.guess \
	$(srcdir)/config.sub \
	$(srcdir)/depcomp \
	$(srcdir)/install-sh \
	$(srcdir)/ltmain.sh \
	$(srcdir)/missing

distclean-local:
	-rm -rf $(srcdir)/autom4te.cache
