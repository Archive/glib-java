dnl
AC_DEFUN([JG_CHECK_NATIVECOMPILE],[
dnl Check for a GCJ native compile option
AC_ARG_WITH(gcj_compile,[  --without-gcj-compile    Binary builds of Java-GNOME
with gcj compiler will not be made],
        gcj_compile="$withval", gcj_compile="yes")

if test $gcj_compile = "yes"; then
        AM_PATH_GCJ(3.0.0, , AC_ERROR(GCJ version 3.0.0 or greater is required))
fi

AM_CONDITIONAL(BUILD_GCJ, test $gcj_compile = "yes")

])

