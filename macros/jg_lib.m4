AC_DEFUN([JG_LIB],[

if test -z "$PKG_CONFIG"; then
    AC_PATH_PROG(PKG_CONFIG, pkg-config, no)
fi

AC_MSG_CHECKING(for glib-java jar file)
JGJAR=`$PKG_CONFIG --variable classpath glib-java`
AC_MSG_RESULT($JGJAR)
AC_SUBST(JGJAR)

AC_MSG_CHECKING(for glib-java library)
JGJAVA_LIBS=`$PKG_CONFIG --libs glib-java`
AC_MSG_RESULT($JGJAVA_LIBS)
AC_SUBST(JGJAVA_LIBS)

AC_MSG_CHECKING(for glib-java cflags)
JGJAVA_CFLAGS=`$PKG_CONFIG --cflags glib-java`
AC_MSG_RESULT($JGJAVA_CFLAGS)
AC_SUBST(JGJAVA_CFLAGS)

AC_MSG_CHECKING(for glib-java jni library)
JGJNI_LIBS=`$PKG_CONFIG --variable jnilibs glib-java`
AC_MSG_RESULT($JGJNI_LIBS)
AC_SUBST(JGJNI_LIBS)

])
