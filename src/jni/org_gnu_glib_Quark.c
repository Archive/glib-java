/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib.h>
#include "jg_jnu.h"

#include "org_gnu_glib_Quark.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.glib.Quark
 * Method:    g_quark_from_string
 * Signature: (java.lang.String)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Quark_g_1quark_1from_1string 
  (JNIEnv *env, jclass cls, jstring str) 
{
    const gchar* str_g;
    GQuark quark;
		
    str_g = (*env)->GetStringUTFChars(env, str, 0);
    
    quark = g_quark_from_string (str_g);
    
    (*env)->ReleaseStringUTFChars(env, str, str_g);
    return (jint)quark;
}

/*
 * Class:     org.gnu.glib.Quark
 * Method:    g_quark_try_string
 * Signature: (java.lang.String)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Quark_g_1quark_1try_1string 
  (JNIEnv *env, jclass cls, jstring str) 
{
    const gchar* str_g;
    GQuark quark;
		
    str_g = (*env)->GetStringUTFChars(env, str, 0);
    
    quark = g_quark_try_string (str_g);
    
    (*env)->ReleaseStringUTFChars(env, str, str_g);
    return (jint)quark;
}

/*
 * Class:     org.gnu.glib.Quark
 * Method:    g_quark_to_string
 * Signature: (I)java.lang.String
 */
JNIEXPORT jstring JNICALL Java_org_gnu_glib_Quark_g_1quark_1to_1string 
  (JNIEnv *env, jclass cls, jint quark) 
{
    GQuark quark_g;
    const gchar *str;
	
    quark_g = (GQuark)quark;
    
    str = g_quark_to_string (quark_g);
    
    return (*env)->NewStringUTF(env, str);
}

#ifdef __cplusplus
}

#endif
