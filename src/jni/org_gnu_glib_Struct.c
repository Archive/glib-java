/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <string.h>
#include "jg_jnu.h"
#include "org_gnu_glib_Struct.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org_gnu_glib_Struct
 * Method:    getNullHandle
 * Signature: ()Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Struct_getNullHandle
(JNIEnv *env, jclass cls)
{
    return getHandleFromPointer(env, NULL);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_Struct_init
(JNIEnv *env, jclass cls)
{
	g_atexit(jg_process_atexit);	    
}

#ifdef __cplusplus
}

#endif
