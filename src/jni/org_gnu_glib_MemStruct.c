/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib.h>
#include <glib-object.h>
#include "glib_java.h"

#ifndef _Included_org_gnu_glib_MemStruct
#define _Included_org_gnu_glib_MemStruct
#include "org_gnu_glib_MemStruct.h"
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_org_gnu_glib_MemStruct_init
  (JNIEnv *env, jclass cls)
{
	initMemoryManagement();
    jg_atexit((GVoidFunc) processPendingStruct);
}
   
JNIEXPORT void JNICALL Java_org_gnu_glib_MemStruct_nativeFinalize
  (JNIEnv *env, jclass cls, jobject handle)
{
    nativeFinalizeStruct(env, handle);
}

#ifdef __cplusplus
}
#endif
#endif
