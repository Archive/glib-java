/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib.h>
#include <glib-object.h>
#include "glib_java.h"

#ifndef _Included_org_gnu_glib_Boxed
#define _Included_org_gnu_glib_Boxed
#include "org_gnu_glib_Boxed.h"
#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_org_gnu_glib_Boxed_init
  (JNIEnv *env, jclass cls)
{
	initMemoryManagement();
    jg_atexit((GVoidFunc) processPendingGBoxed);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_Boxed_nativeFinalize
  (JNIEnv *env, jclass cls, jobject handle)
{
    nativeFinalizeGBoxed(env, handle);
}

/*
 * Class:     org_gnu_glib_Boxed
 * Method:    nativeFinalize
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
/*
JNIEXPORT void JNICALL Java_org_gnu_glib_Boxed_nativeFinalize
  (JNIEnv *env, jclass cls, jobject handle, jobject freefunc)
{
	JGBoxed *boxed;
	
	boxed = g_new(JGBoxed, 1);
	boxed->boxed = (gpointer)getPointerFromHandle(env, handle);
	boxed->free = (GBoxedFreeFunc)getPointerFromHandle(env, freefunc);
	nativeFinalize(env, JG_GBOXED, getHandleFromPointer(boxed));
}
*/

#ifdef __cplusplus
}
#endif
#endif
