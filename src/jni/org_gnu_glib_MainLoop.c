/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <jg_jnu.h>
#include <glib.h>
#include "glib_java.h"

#ifndef _Included_org_gnu_glib_MainLoop
#define _Included_org_gnu_glib_MainLoop
#include "org_gnu_glib_MainLoop.h"
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     org_gnu_glib_MainLoop
 * Method:    g_main_loop_new
 * Signature: (Lorg/gnu/glib/Handle;Z)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_MainLoop_g_1main_1loop_1new
  (JNIEnv *env, jclass cls, jobject context, jboolean is_running)
{
    GMainContext *context_g;
    GMainLoop *loop_g;
	
    context_g = getPointerFromHandle(env, context);
  	
    loop_g = g_main_loop_new(context_g, (gboolean) is_running);
  	
    return getStructHandle(env, loop_g, NULL, (JGFreeFunc)g_main_loop_unref);
}

/*
 * Class:     org_gnu_glib_MainLoop
 * Method:    g_main_loop_run
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_MainLoop_g_1main_1loop_1run
  (JNIEnv *env, jclass cls, jobject loop)
  {
  	GMainLoop *loop_g;
  	
  	loop_g = (GMainLoop *) getPointerFromHandle(env, loop);
  	
  	g_main_loop_run(loop_g);
  }

/*
 * Class:     org_gnu_glib_MainLoop
 * Method:    g_main_loop_quit
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_MainLoop_g_1main_1loop_1quit
  (JNIEnv *env, jclass cls, jobject loop)
  {
  	GMainLoop *loop_g;
  	loop_g = (GMainLoop *) getPointerFromHandle(env, loop);
  	
  	g_main_loop_quit(loop_g);
  }

/*
 * Class:     org_gnu_glib_MainLoop
 * Method:    g_main_loop_is_running
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_glib_MainLoop_g_1main_1loop_1is_1running
  (JNIEnv *env, jclass cls, jobject loop)
  {
  	GMainLoop *loop_g;
  	gboolean value;

  	loop_g = (GMainLoop *) getPointerFromHandle(env, loop);
  	
  	value = g_main_loop_is_running(loop_g);
  	
  	return value;
  }

#ifdef __cplusplus
}
#endif
#endif
