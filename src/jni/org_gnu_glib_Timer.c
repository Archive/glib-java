/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include "jg_jnu.h"
#include <glib.h>

#include "org_gnu_glib_Timer.h"
#ifdef __cplusplus
extern "C" {
#endif

static jfieldID  targetFID;

static void cleanup(JNIEnv *env, jobject obj) {
    // reset handle field
    jclass cls = (*env)->GetObjectClass(env, obj);
    jfieldID fid = (*env)->GetFieldID(env, cls, "handle", "I");

    //check of the pointer has already been freed
    jint handleValue = (*env)->GetIntField(env, obj, fid);
    if(handleValue != 0){
      (*env)->SetIntField(env, obj, fid, (jint)0);
      
      // delete back pointer
      (*env)->DeleteGlobalRef(env, obj);
    }
}

static gboolean fire_method_invoker(gpointer data)
{
    JNIEnv *env = JG_JNU_GetEnv();
    jobject obj = (jobject)data;
    jboolean exceptionThrown;
    
    jobject target = (*env)->GetObjectField(env, obj, targetFID);
    jclass cls = (*env)->GetObjectClass(env, target);
    jmethodID mid = (*env)->GetMethodID(env, cls, "fire", "()Z");

    jboolean keepFiring = (*env)->CallBooleanMethod(env, target, mid);
    (*env)->DeleteLocalRef(env, cls);
    (*env)->DeleteLocalRef(env, target);

    exceptionThrown = (*env)->ExceptionCheck(env);
    if (exceptionThrown) {
	// We cannot throw this exception, since this timer was called by GLib.
	(*env)->ExceptionDescribe(env);  // ExceptionDescribe clears exception
	keepFiring = JNI_FALSE;
    }

    if (keepFiring == JNI_FALSE) {
	cleanup(env, obj);
    }
    return keepFiring;
}

/*
 * Class:     org_gnu_glib_Timer
 * Method:    start_timer
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Timer_start_1timer
    (JNIEnv *env, jobject obj, jint interval)
{
	guint timer;
	gpointer data;
	
	data = (*env)->NewGlobalRef(env, obj);
	
	timer = g_timeout_add((gint)interval, 
			       fire_method_invoker, 
			       data);
	
    return (jint)timer;
}

/*
 * Class:     org_gnu_glib_Timer
 * Method:    stop_timer
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Timer_stop_1timer 
    (JNIEnv *env, jobject obj, jint handle)
{
    cleanup(env, obj);
    
    g_source_remove(handle);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_Timer_initIDs 
    (JNIEnv *env, jclass cls)
{
    targetFID = (*env)->GetFieldID(env, cls,
				   "target", "Lorg/gnu/glib/Fireable;");
}

#ifdef __cplusplus
}
#endif
