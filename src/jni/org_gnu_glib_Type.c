/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2006 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib.h>
#include "jg_jnu.h"

#include "org_gnu_glib_Type.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * A type to allow us to create pointers to java objects. 
 * This could get complicated...
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1JAVA_1OBJECT
    (JNIEnv *env, jclass cls) 
{
	return (jint) G_TYPE_POINTER; /** We'll just have to hope that Gtk never uses that number of types. */
}

/*
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1POINTER
    (JNIEnv *env, jclass cls) 
{
	return (jint) G_TYPE_POINTER;
}


/*
 * Class:     org.gnu.glib.Type
 * Method:    get_INVALID
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1INVALID
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_INVALID;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_NONE
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1NONE
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_NONE;
}


/*
 * Class:     org.gnu.glib.Type
 * Method:    get_INTERFACE
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1INTERFACE
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_INTERFACE;
}


/*
 * Class:     org.gnu.glib.Type
 * Method:    get_CHAR
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1CHAR
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_CHAR;
}


/*
 * Class:     org.gnu.glib.Type
 * Method:    get_BOOLEAN
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1BOOLEAN
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_BOOLEAN;
}


/*
 * Class:     org.gnu.glib.Type
 * Method:    get_INT
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1INT
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_INT;
}


/*
 * Class:     org.gnu.glib.Type
 * Method:    get_LONG
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1LONG
    (JNIEnv *env, jclass cls) 
{
	/*
	 * Note we use signed 64 bit wide integer values as 
	 * matches the Java platforms definiton for the long
	 * datatype
	 */
	return (jint)G_TYPE_INT64;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_FLAGS
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1FLAGS
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_FLAGS;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_FLOAT
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1FLOAT
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_FLOAT;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_DOUBLE
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1DOUBLE
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_DOUBLE;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_STRING
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1STRING
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_STRING;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_BOXED
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1BOXED
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_BOXED;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_PARAM
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1PARAM
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_PARAM;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    get_OBJECT
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_get_1OBJECT
    (JNIEnv *env, jclass cls) 
{
	return (jint)G_TYPE_OBJECT;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    g_type_init
 * Signature: ()V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Type_g_1type_1init 
    (JNIEnv *env, jclass cls) 
{
	g_type_init ();
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    g_type_name
 * Signature: (I)java.lang.String
 */
JNIEXPORT jstring JNICALL Java_org_gnu_glib_Type_g_1type_1name 
    (JNIEnv *env, jclass cls, jint type) 
{
    GType type_g = (GType)&type;
    const gchar *result_g;
    
    result_g = g_type_name (type_g);
    
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    g_type_qname
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_g_1type_1qname 
    (JNIEnv *env, jclass cls, jint type) 
{
    GType type_g = (GType)&type;
    gint value;
    
    value = g_type_qname (type_g);
    
    return (jint)value;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    g_type_from_name
 * Signature: ([B)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_g_1type_1from_1name 
    (JNIEnv *env, jclass cls, jstring name) 
{
	const gchar *name_g;
	GType result;
	
    name_g = (*env)->GetStringUTFChars(env, name, 0);
    
    result = g_type_from_name (name_g);
    
    (*env)->ReleaseStringUTFChars(env, name, name_g);
    return (jint)result;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    g_type_parent
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_g_1type_1parent 
    (JNIEnv *env, jclass cls, jint type) 
{
    GType type_g = (GType)&type;
    GType parent;
    
    parent = g_type_parent (type_g);
    
    return (jint)parent;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    g_type_depth
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Type_g_1type_1depth 
    (JNIEnv *env, jclass cls, jint type) 
{
    GType type_g = (GType)&type;
    gint depth;
    
    depth = g_type_depth (type_g);
    
    return depth;
}

/*
 * Class:     org.gnu.glib.Type
 * Method:    g_type_is_a
 * Signature: (II)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_glib_Type_g_1type_1is_1a (JNIEnv *env, jclass cls, jint type, jint type_is_a) 
{
    GType type_g = (GType)type;
    GType type_is_a_g = (GType)type_is_a;
    gboolean value;
    
    value = g_type_is_a(type_g, type_is_a_g);
    
    return value;
}


#ifdef __cplusplus
}

#endif
