/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <jg_jnu.h>
#include "glib_java.h"

#include "org_gnu_glib_GObject.h"
#ifdef __cplusplus
extern "C"                             c
{
#endif

typedef struct {
  JNIEnv *env;
  jobject obj;  /* object to recieve the signal */
  jclass class;  /* class to recieve the signal for static methods */
  jmethodID methodID;  /* methodID of callback method */
  jobject methodData;  /* user defined data to pass to callback method */
  jboolean isStatic;  /* is the callback a static method? */
  jboolean isBoolReturn; /* does the callback method return a boolean value */
  jboolean isStringReturn; /* does the callback method return a string value */
  jboolean shouldCopyIfBoxed; /* should the incoming object from a signal, if type boxed, be kept copied*/
} CallbackData;

typedef struct {
  GClosure closure;
  CallbackData *cbdata;
} JGClosure;

/* This method actually recieves all callbacks in the system.  It 
 * recieves a CallbackData struct as its' data.  It then determines
 * what Java method to call and invokes it passing the correct parameters */
static void
jg_signal_cb(GClosure *closure,
	     GValue *return_value,
	     guint n_param_values,
	     const GValue *param_values,
	     gpointer invocation_hint,
	     gpointer marshal_data)
{
  jvalue *jargs;
  JGClosure *jgc = (JGClosure *)closure;
  CallbackData *cbdata;
  guint i;
  jmethodID mid;
  jobject localObj;
  jobject peer;
  jthrowable exc;
  gboolean b;
  const gchar* str;
  int jargsLen = n_param_values;
  GType value_type;
  
  cbdata = jgc->cbdata;
  if (cbdata == NULL) {
    g_critical("Java-GNOME - unable to determine the callback method.\n");
    return;
  }
  
  if (cbdata->methodData != NULL) {
  	jargsLen++;
  }
  
  jargs = alloca(jargsLen * sizeof(jvalue));

  /* build the parameters for the callback */
  for(i = 1; i < n_param_values; i++) {
  	value_type = G_VALUE_TYPE(&param_values[i]);
    switch(G_TYPE_FUNDAMENTAL(value_type)) {
    case G_TYPE_CHAR:
      jargs[i-1].c = g_value_get_char(&param_values[i]);
      break;
    case G_TYPE_UCHAR:
      jargs[i-1].c = g_value_get_uchar(&param_values[i]);
      break;
    case G_TYPE_BOOLEAN:
      b = g_value_get_boolean(&param_values[i]);
      jargs[i-1].z = (b == TRUE) ? JNI_TRUE : JNI_FALSE;
      break;
    case G_TYPE_INT:
      jargs[i-1].i = g_value_get_int(&param_values[i]);
      break;
    case G_TYPE_UINT:
      jargs[i-1].i = g_value_get_uint(&param_values[i]);
      break;
    case G_TYPE_ENUM:
      jargs[i-1].i = g_value_get_enum(&param_values[i]);
      break;
    case G_TYPE_FLAGS:
      jargs[i-1].i = g_value_get_flags(&param_values[i]);
      break;
    case G_TYPE_LONG:
      jargs[i-1].j = g_value_get_long(&param_values[i]);
      break;
    case G_TYPE_ULONG:
      jargs[i-1].j = g_value_get_ulong(&param_values[i]);
      break;
    case G_TYPE_FLOAT:
      jargs[i-1].f = g_value_get_float(&param_values[i]);
      break;
    case G_TYPE_DOUBLE:
      jargs[i-1].d = g_value_get_double(&param_values[i]);
      break;
    case G_TYPE_STRING:
      str = g_value_get_string(&param_values[i]);
      localObj = (*cbdata->env)->NewStringUTF(cbdata->env, str);
      jargs[i-1].l = localObj;
      break;
    case G_TYPE_OBJECT:
      peer = getGObjectHandle(cbdata->env, 
                              g_value_get_object(&param_values[i]));
      jargs[i-1].l = peer;
      break;
    case G_TYPE_BOXED:
      // MEM_MGT_NOTE: we don't know what GBoxed{Copy,Free}Func to use here!
        
      //We do the copying manually here and the GBoxed will be freed
      //automatically by calling g_boxed_free
      
      if(cbdata->shouldCopyIfBoxed == JNI_TRUE)
          peer = getGBoxedHandle(cbdata->env,
                                 g_boxed_copy(value_type, g_value_get_boxed(&param_values[i])),
                             value_type, NULL, NULL);
      else
          peer = getGBoxedHandle(cbdata->env, g_value_get_boxed(&param_values[i]), -1, NULL, NULL);
      
      jargs[i-1].l = peer;
      break;
    case G_TYPE_POINTER:
      // MEM_MGT_NOTE: we don't know what free func to use here!
      peer = getHandleFromPointer(cbdata->env, g_value_get_pointer(&param_values[i]));
      jargs[i-1].l = peer;
      break;
    default:
      peer = getGObjectHandle(cbdata->env, 
                              g_value_get_object(&param_values[i]));
      jargs[i-1].l = peer;
      break;
    }
  }
  
  if (cbdata->methodData != NULL) {
    jargs[jargsLen].l = (jobject)cbdata->methodData;
  }

  mid = cbdata->methodID;

  if (cbdata->isBoolReturn == JNI_TRUE) {
    if (cbdata->isStatic == JNI_FALSE) {
	    b = (* cbdata->env)->CallBooleanMethodA(cbdata->env, 
					      cbdata->obj, 
					      mid, 
					      jargs);
    }
    else {
      b = (* cbdata->env)->CallStaticBooleanMethodA(cbdata->env,
						    cbdata->class,
						    mid,
						    jargs);
    }
    if (return_value) {
      	if (b == JNI_TRUE) {
        	g_value_set_boolean(return_value, TRUE);
      	}
      	else  {
        	g_value_set_boolean(return_value, FALSE);
      	}
    }
  }
  
  else if (cbdata->isStringReturn == JNI_TRUE)	{
  	const gchar* ret_chars;
  	jstring ret_string;
  	if (cbdata->isStatic == JNI_FALSE)	{
  		ret_string = (jstring) (* cbdata->env)->CallObjectMethodA(cbdata->env,
  								cbdata->obj,
  								mid,
  								jargs);
  	}
  	else	{
  		ret_string = (jstring) (* cbdata->env)->CallObjectMethodA(cbdata->env,
  								cbdata->class,
  								mid,
  								jargs);
  	}
  	if (return_value)	{
  		ret_chars = (*cbdata->env)->GetStringUTFChars(cbdata->env, ret_string, 0);
  		g_value_set_string(return_value, ret_chars);
  		(*cbdata->env)->ReleaseStringUTFChars(cbdata->env, ret_string, ret_chars);
  	}
  }
  	
  else {
    if (cbdata->isStatic == JNI_FALSE)
      (* cbdata->env)->CallVoidMethodA(cbdata->env, 
				       cbdata->obj, 
				       mid, 
				       jargs);
    else
      (* cbdata->env)->CallStaticVoidMethodA(cbdata->env,
					     cbdata->class,
					     mid,
					     jargs);
  }
  
  /* Check if an exception occured in the callback */
  exc = (* cbdata->env)->ExceptionOccurred(cbdata->env);
  if (exc) {
	/* Print stack trace */
	(* cbdata->env)->ExceptionDescribe(cbdata->env);
	/* clear the exception, so we can continue. */		
    (* cbdata->env)->ExceptionClear(cbdata->env);
  }
}

static void
jg_cbdata_destroy(gpointer data, GClosure *closure)
{
  JGClosure *jgclosure = (JGClosure *)closure;
  CallbackData *cbdata = jgclosure->cbdata;
  
  if (cbdata->obj)
    (* cbdata->env)->DeleteGlobalRef(cbdata->env, cbdata->obj);
  if (cbdata->class)
    (* cbdata->env)->DeleteGlobalRef(cbdata->env, cbdata->class);
  
  g_free(data);
}

/* Create a GClosure */
static GClosure *
jg_closure_new(void)
{
  GClosure *closure;

  closure = g_closure_new_simple(sizeof(JGClosure), NULL);
  g_closure_add_finalize_notifier(closure, NULL, jg_cbdata_destroy);
  g_closure_set_marshal(closure, jg_signal_cb);
  return closure;
}

static void
jg_arg_signature(GString *str, GType argtype)
{
  char *as = NULL;
  char ac = '\0';
  gboolean free_as = FALSE;


  switch(G_TYPE_FUNDAMENTAL(argtype)) {
  case G_TYPE_NONE:
    ac = 'V';
    break;  
  case G_TYPE_CHAR:
  case G_TYPE_UCHAR:
    ac = 'C';
    break;
  case G_TYPE_BOOLEAN:
    ac = 'Z';
    break;
  case G_TYPE_INT:
  case G_TYPE_UINT:
    ac = 'I';
    break;
  case G_TYPE_ENUM:
    ac = 'I';
    break;
  case G_TYPE_FLAGS:
    ac = 'I';
    break;
    case G_TYPE_LONG:
  case G_TYPE_ULONG:
    ac = 'J';
    break;
  case G_TYPE_FLOAT:
    ac = 'F';
    break;
  case G_TYPE_DOUBLE:
    ac = 'D';
    break;
  case G_TYPE_STRING:
    as = (char*) "java/lang/String";
    break;
  case G_TYPE_BOXED:
    as = (char*) "org/gnu/glib/Handle";
    break;
  case G_TYPE_POINTER:
    as = (char*) "org/gnu/glib/Handle";
    break;
  case G_TYPE_OBJECT:
    as = (char*) "org/gnu/glib/Handle";
    break;
  default:
    as = (char*) "org/gnu/glib/Handle";
    break;
  case G_TYPE_INVALID:
    g_error("Type %d - should not be reached", (gint)argtype);
    break;
  }
  
  if(ac)
    g_string_append_c(str, ac);
  if(as) {
    g_string_append(str, "L");
    g_string_append(str, as);
    g_string_append(str, ";");
  }
  
  if(free_as)
    g_free(as);
}

/* This method determines the Java signature for a callback method */
static char *
jg_signal_signature(GSignalQuery siginfo, jobject method_data)
{
  GString *tmpstr;
  char *retval;
  guint i;

  tmpstr = g_string_new(NULL);

  g_string_append_c(tmpstr, '(');

  /* add all of the gtk signal parameters to the string */
  for(i = 0; i < siginfo.n_params; i++) {
    GType argtype = siginfo.param_types[i];
    jg_arg_signature(tmpstr, argtype);
  }

  /* here is where we add the actual method data if it exists */
  if (method_data)
    g_string_append(tmpstr, "Ljava/lang/Object;");

  /* and now, the return type */
  g_string_append_c(tmpstr, ')');
  jg_arg_signature(tmpstr, siginfo.return_type);
  
  retval = tmpstr->str;
  g_string_free(tmpstr, FALSE);
  
  return retval;
}

/* This method constructs the CallbackData struct for a given
 * instance callback */
static CallbackData *
jg_cbdata_new(JNIEnv *env, 
	      jobject obj, 
	      jobject connect_to, 
	      jobject method_data,
	      const char *signame, 
	      const char* sigfunc,
	      jboolean shouldCopyIfBoxed)
{
  CallbackData *cbdata;
  GObject *gtko;
  GSignalQuery siginfo;
  guint sigid;
  char *sigsig;
  jthrowable exc;
  jclass Exception;
  
  //  gtko = (GObject*)getPointerFromHandle(env, obj);
  gtko = (GObject*)getPointerFromJavaGObject(env, obj);
  sigid = g_signal_lookup(signame, G_OBJECT_TYPE(gtko));
  g_assert(sigid > 0);
  g_signal_query(sigid, &siginfo);
  
  sigsig = jg_signal_signature(siginfo, method_data);
  cbdata = g_new(CallbackData, 1);


  /* add the target object to the struct */
  cbdata->obj = (*env)->NewGlobalRef(env, connect_to);
  cbdata->class = NULL;
  cbdata->shouldCopyIfBoxed = shouldCopyIfBoxed;
  if (G_TYPE_FUNDAMENTAL(siginfo.return_type) == G_TYPE_BOOLEAN)
    cbdata->isBoolReturn = JNI_TRUE;
  else
    cbdata->isBoolReturn = JNI_FALSE;

  if (G_TYPE_FUNDAMENTAL(siginfo.return_type) == G_TYPE_STRING)
    cbdata->isStringReturn = JNI_TRUE;
  
  else
    cbdata->isStringReturn = JNI_FALSE;
    
  /* add the method data to the struct if it exists */
  if (method_data != NULL)
    cbdata->methodData = (*env)->NewGlobalRef(env, method_data);
  else
    cbdata->methodData = NULL;

  /* add the methodid to the struct */
  cbdata->methodID = 
    (*env)->GetMethodID(env, (*env)->GetObjectClass(env, connect_to), 
			sigfunc, sigsig);
  exc = (*env)->ExceptionOccurred(env);
  if (exc) {
    g_warning("Java-GNOME - cannot find callback method %s in the specified object with signature %s\n",
	   sigfunc, sigsig);
    (*env)->ExceptionClear(env);
	Exception = (* env)->FindClass(env, "java/lang/RuntimeException");
    (* env)->ThrowNew(env, Exception, "" );
    return NULL;
  }

  /* add the env to the struct */
  cbdata->env = env;

  /* set the static flag */
  cbdata->isStatic = JNI_FALSE;

  g_free(sigsig);
  return cbdata;
}


/* This method constructs the CallbackData struct for a given
 * instance callback */
static CallbackData *
jg_static_cbdata_new(JNIEnv *env, 
		     jobject obj, 
		     jclass cbref, 
		     jobject method_data,
		     const char *signame, 
		     const char* sigfunc)
{
  CallbackData *cbdata;
  GObject *gtko;
  GSignalQuery siginfo;
  guint sigid;
  char *sigsig;
  jthrowable exc;
  jclass Exception;
  
  gtko = (GObject*)getPointerFromJavaGObject(env, obj);
  sigid = g_signal_lookup(signame, G_OBJECT_TYPE(gtko));
  g_assert(sigid > 0);
  g_signal_query(sigid, &siginfo);
  
  sigsig = jg_signal_signature(siginfo, method_data);
  
  cbdata = g_new(CallbackData, 1);
  
  /* add the target object to the struct */
  cbdata->class = (*env)->NewGlobalRef(env, cbref);
  cbdata->obj = NULL;
  
  /* add the method data to the struct if it exists */
  if (method_data != NULL)
    cbdata->methodData = (*env)->NewGlobalRef(env, method_data);
  else
    cbdata->methodData = NULL;
  
  /* add the methodid to the struct */
  cbdata->methodID = 
    (*env)->GetStaticMethodID(env, cbref, sigfunc, sigsig);
  
  exc = (*env)->ExceptionOccurred(env);
  if (exc) {
    g_warning("Java-GNOME - cannot find static callback method %s in the specified class with signature %s\n", sigfunc, sigsig);
    (*env)->ExceptionClear(env);
	Exception = (* env)->FindClass(env, "java/lang/RuntimeException");
    (* env)->ThrowNew(env, Exception, "" );
    return NULL;
  }
  
  /* add the env to the struct */
  cbdata->env = env;
  
  /* set the static flag */
  cbdata->isStatic = JNI_TRUE;
  
  g_free(sigsig);
  return cbdata;
}


static jint create_callback(JNIEnv *env, 
                            jobject obj, 
                            jstring sigstring, 
                            jstring func, 
                            jobject cbrecv, 
                            jobject data,
                            jboolean shouldCopyIfBoxed)
{ 
	jint retval;
  	const char *signame;
  	const char *sigfunc;
  	CallbackData *cbdata;
  	GObject *go;
  	guint sigid;
  	GQuark detail = 0;
  	GClosure *closure;
  	jclass Exception;

	go = (GObject*)getPointerFromJavaGObject(env, obj);
	signame = (*env)->GetStringUTFChars(env, sigstring, NULL);
	sigfunc = (*env)->GetStringUTFChars(env, func, NULL);
  
	if (!g_signal_parse_name(signame, G_OBJECT_TYPE(go),
			   &sigid, &detail, TRUE)) {
    	g_critical("Java-GNOME - unknown signal name %s, function %s, object %s.\n", signame, sigfunc, G_OBJECT_TYPE_NAME(go));
    	
		Exception = (* env)->FindClass(env, "java/lang/RuntimeException");
    	(* env)->ThrowNew(env, Exception, "" );
  		(*env)->ReleaseStringUTFChars(env, sigstring, signame);
  		(*env)->ReleaseStringUTFChars(env, func, sigfunc);  
    	return 0;
  	}
  	cbdata = jg_cbdata_new(env, obj, cbrecv, data, signame, sigfunc, shouldCopyIfBoxed);

  	if (cbdata == NULL) {
		(*env)->ReleaseStringUTFChars(env, sigstring, signame);
		(*env)->ReleaseStringUTFChars(env, func, sigfunc);  
    	return 0;
  	}

  	closure = jg_closure_new();
  	((JGClosure *)closure)->cbdata = cbdata;

	retval = g_signal_connect_closure_by_id(go,
					  sigid, detail, closure, FALSE);

	(*env)->ReleaseStringUTFChars(env, sigstring, signame);
	(*env)->ReleaseStringUTFChars(env, func, sigfunc);
	return retval;
}


static jint create_static_callback(JNIEnv *env, 
                                   jobject obj, 
                                   jstring sigstring, 
                                   jstring func, 
                                   jclass cbrecv, 
                                   jobject data)
{ 
	jint retval;
	const char *signame;
	const char *sigfunc;
	CallbackData *cbdata;
	GObject *go;
	guint sigid;
	GQuark detail = 0;
	GClosure *closure;
	jclass Exception;
  
	go = (GObject*)getPointerFromJavaGObject(env, obj);
	signame = (*env)->GetStringUTFChars(env, sigstring, NULL);
	sigfunc = (*env)->GetStringUTFChars(env, func, NULL);
  
	if (!g_signal_parse_name(signame, G_OBJECT_TYPE(go),
			   &sigid, &detail, TRUE)) {
    	g_critical("Java-GNOME - unknown signal name %s, function %s.\n", signame, sigfunc);
    	
		Exception = (* env)->FindClass(env, "java/lang/RuntimeException");
    	(* env)->ThrowNew(env, Exception, "" );
  		(*env)->ReleaseStringUTFChars(env, sigstring, signame);
  		(*env)->ReleaseStringUTFChars(env, func, sigfunc);  
    	return 0;
  	}
	cbdata = jg_static_cbdata_new(env, obj, cbrecv, data, signame, sigfunc);

  	if (cbdata == NULL) {
		(*env)->ReleaseStringUTFChars(env, sigstring, signame);
		(*env)->ReleaseStringUTFChars(env, func, sigfunc);  
    	return 0;
  	}
  	
  	closure = jg_closure_new();
  	((JGClosure *)closure)->cbdata = cbdata;
  	retval = g_signal_connect_closure_by_id(getPointerFromJavaGObject(env, obj),
					  sigid, detail, closure, FALSE);
  
  (*env)->ReleaseStringUTFChars(env, sigstring, signame);
  (*env)->ReleaseStringUTFChars(env, func, sigfunc);  
  return retval;
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_init
  (JNIEnv *env, jclass cls)
{
	initMemoryManagement();
    jg_atexit((GVoidFunc) processPendingGObject);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_nativeFinalize
  (JNIEnv *env, jclass cls, jobject handle)
{
    nativeFinalizeGObject(env, handle);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_nativeCollect
  (JNIEnv *env, jclass cls)
{
    processPendingGObject();
    processPendingGBoxed();
    processPendingStruct();
}

JNIEXPORT void JNICALL 
Java_org_gnu_glib_GObject_g_1object_1ref(JNIEnv *env, jclass cls, jobject handle)
{
	g_object_ref(getPointerFromHandle(env, handle));
}

JNIEXPORT void JNICALL 
Java_org_gnu_glib_GObject_g_1object_1unref(JNIEnv *env, jclass cls, jobject handle)
{
	g_object_unref(getPointerFromHandle(env, handle));
}


/*
 * Class:     org_gnu_glib_GObject
 * Method:    addEventHandler
 * Signature: (Ljava/lang/String;Lgtk/GtkObject;)I
 */
JNIEXPORT jint JNICALL
Java_org_gnu_glib_GObject_addEventHandler__Ljava_lang_String_2Ljava_lang_Object_2
(JNIEnv *env, jobject obj, jstring sigstring, jobject cbrecv)
{
  return create_callback(env, obj, sigstring, sigstring, cbrecv, NULL, TRUE);
}

/*
 * Class:     org_gnu_glib_GObject
 * Method:    addEventHandler
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)I
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_glib_GObject_addEventHandler__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_Object_2Z
(JNIEnv *env, jobject obj, jstring sigstring, jstring func, jobject cbrecv, jboolean shouldCopyIfBoxed)
{ 
	return create_callback(env, obj, sigstring, func, cbrecv, NULL, shouldCopyIfBoxed);
}


/*
 * Class:     org_gnu_glib_GObject
 * Method:    addEventHandler
 * Signature: (Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Z;)I
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_glib_GObject_addEventHandler__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2
(JNIEnv *env, jobject obj, jstring sigstring, jobject cbrecv, jobject data)
{
  return create_callback(env, obj, sigstring, sigstring, cbrecv, data, TRUE);
}


/*
 * Class:     org_gnu_glib_GObject
 * Method:    addEventHandler
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_glib_GObject_addEventHandler__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2
(JNIEnv *env, jobject obj, jstring sigstring, jstring func, jobject cbrecv, jobject data)
{ 
  return create_callback(env, obj, sigstring, func, cbrecv, data, TRUE);
}


/*
 * Class:     org_gnu_glib_GObject
 * Method:    addEventHandler
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;)I
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_glib_GObject_addEventHandler__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_Class_2
(JNIEnv *env, jobject obj, jstring sigstring, jstring func, jclass class)
{
  return create_static_callback(env, obj, sigstring, func, class, NULL);
}

/*
 * Class:     org_gnu_glib_GObject
 * Method:    addEventHandler
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;)I
 */
JNIEXPORT jint JNICALL 
Java_org_gnu_glib_GObject_addEventHandler__Ljava_lang_String_2Ljava_lang_String_2Ljava_lang_Class_2Ljava_lang_Object_2
(JNIEnv *env, jobject obj, jstring sigstring, jstring func, jclass class, jobject data)
{
  return create_static_callback(env, obj, sigstring, func, class, data);
}



/*
 * Class:     org_gnu_glib_GObject
 * Method:    removeEventHandler
 * Signature: (I)V
 */
JNIEXPORT void JNICALL
Java_org_gnu_glib_GObject_removeEventHandler
    (JNIEnv *env, jobject obj, jint sigid)
{
	GObject *obj_g;
	
	obj_g = (GObject*)getPointerFromJavaGObject(env, obj);
	
  	g_signal_handler_disconnect(obj_g, sigid);
}

/*
 * Class:     org_gnu_glib_GObject/
 * Method:    getData
 * Signature: (ILjava/lang/String;)Ljava/lang/Object;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_GObject_getData
  (JNIEnv *env, jobject obj, jobject handle, jstring key)
{
	gchar *k;
	GObject *handle_g;
	JGRef *ref;
	
    k = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);
    handle_g = (GObject *)getPointerFromHandle(env, handle);
    
    ref = (JGRef*) g_object_get_data(handle_g, k);
    
    (*env)->ReleaseStringUTFChars(env, key, k);
    if (ref == NULL)
        return NULL;
   	else
        return ref->object;
}

static void jg_gobject_destroy(gpointer data)
{
	JGRef *ref = (JGRef*) data;
	(* ref->env)->DeleteGlobalRef( ref->env, ref->object);
	g_free( ref );
}

/*
 * Class:     org_gnu_glib_GObject
 * Method:    setData
 * Signature: (ILjava/lang/String;Ljava/lang/Object;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_setData
    (JNIEnv *env, jclass cls, jobject handle, jstring key, jobject data)
{
	GObject *handle_g;
	const gchar *k;
	JGRef *ref;
	
    handle_g = (GObject *)getPointerFromHandle(env, handle);
    k = (gchar*)(*env)->GetStringUTFChars(env, key, NULL);

    ref = g_new( JGRef, 1 );
    ref->env = env;
    ref->object = (* env)->NewWeakGlobalRef(env, data);

    g_object_set_data_full(handle_g, k, ref, jg_gobject_destroy ) ;
    
    (*env)->ReleaseStringUTFChars(env, key, k);
}

static void logFunc(const gchar* logDomain, GLogLevelFlags logLevel, const gchar* message, gpointer userData)
{
    jclass gobject;
    jmethodID stackHandler;
    JNIEnv *env = (JNIEnv*)userData;
    jstring msg;
	
    (*env)->ExceptionClear(env);
    msg = (*env)->NewStringUTF(env, message);
	
    gobject = (*env)->FindClass(env, "org/gnu/glib/GObject");
    stackHandler = (*env)->GetStaticMethodID(env, gobject, "printStackTrace", "(Ljava/lang/String;)V");
    (*env)->CallStaticVoidMethod(env, gobject, stackHandler, msg);
}

/*
 * Class:     org_gnu_glib_GObject
 * Method:    registerLogHandler
 * Signature: ()V
 */ 
JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_registerLogHandler
    (JNIEnv *env, jclass cls)
{
	g_log_set_handler(NULL, G_LOG_LEVEL_WARNING | G_LOG_LEVEL_CRITICAL |
			G_LOG_FLAG_FATAL | G_LOG_FLAG_RECURSION, logFunc, (gpointer)env);
	g_log_set_handler ("GLib", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL
                     | G_LOG_FLAG_RECURSION, logFunc, (gpointer) env);
	g_log_set_handler ("Gnome", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL
                     | G_LOG_FLAG_RECURSION, logFunc, (gpointer) env);
	g_log_set_handler ("Gtk", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL
                     | G_LOG_FLAG_RECURSION, logFunc, (gpointer) env);
	g_log_set_handler ("Gconf", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL
                     | G_LOG_FLAG_RECURSION, logFunc, (gpointer) env);
	g_log_set_handler ("Glade", G_LOG_LEVEL_MASK | G_LOG_FLAG_FATAL
                     | G_LOG_FLAG_RECURSION, logFunc, (gpointer) env);
}

JNIEXPORT jobject JNICALL Java_org_gnu_glib_GObject_g_1object_1new
    (JNIEnv *env, jclass cls, jint object_type)
{
	GObject *obj;
	GType object_type_g;
	
    object_type_g = (GType)object_type;
    
    obj = g_object_new(object_type_g, NULL, NULL);
    
    return getGObjectHandle(env, obj);
}

JNIEXPORT jobject JNICALL Java_org_gnu_glib_GObject_g_1object_1class_1find_1property
    (JNIEnv *env, jclass cls, jobject oclass, jstring property_name)
{
    GObjectClass *oclass_g;
    const gchar *property_name_g;
    GParamSpec *property;
	
    oclass_g = 
        G_OBJECT_GET_CLASS((GObject *)getPointerFromHandle(env, oclass));
    property_name_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, property_name, 0);

    property = g_object_class_find_property(oclass_g, property_name_g);

    (*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
    // MEM_MGT_NOTE: OK. See GObject#hasGtkProperty
    return getHandleFromPointer(env, property);	
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_g_1object_1set_1property
    (JNIEnv *env, jclass cls, jobject object, jstring property_name, jobject value)
{
	GObject *object_g;
	const gchar *property_name_g;
	const GValue *value_g;
	
    object_g = (GObject *)getPointerFromHandle(env, object);
    property_name_g = (const gchar *)(*env)->GetStringUTFChars(env, property_name, 0);
    value_g = (const GValue *)getPointerFromHandle(env, value);
    
    g_object_set_property(object_g, property_name_g, value_g);
    
    (*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
}

JNIEXPORT jobject JNICALL Java_org_gnu_glib_GObject_g_1object_1get_1property
    (JNIEnv *env, jclass cls, jobject object, jstring property_name)
{
    GObject *object_g;
    const gchar *property_name_g;
    GParamSpec *pspec;
    GValue *value_g;
	
    object_g = (GObject *)getPointerFromHandle(env, object);
    property_name_g = 
        (const gchar *)(*env)->GetStringUTFChars(env, property_name, 0);

    pspec = g_object_class_find_property(G_OBJECT_GET_CLASS(object_g), 
                                         property_name_g);
    if ( !pspec ) {
        (*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
        return NULL;
    }
    value_g = g_malloc( sizeof( GValue ) );
    value_g->g_type = 0;
    value_g = g_value_init(value_g, pspec->value_type);

    // Get the property.
    g_object_get_property(object_g, property_name_g, value_g);
    
    (*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
    return getStructHandle(env, value_g, NULL, g_free);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_g_1object_1freeze_1notify
(JNIEnv *env, jclass cls, jobject object)
{
	GObject *object_g;
	
    object_g = (GObject *)getPointerFromHandle(env, object);
    
    g_object_freeze_notify(object_g);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_g_1object_1notify
    (JNIEnv *env, jclass cls, jobject object, jstring property_name)
{
	GObject *object_g;
	const gchar *property_name_g;
	
    object_g = (GObject *)getPointerFromHandle(env, object);
    property_name_g = (const gchar *)(*env)->GetStringUTFChars(env, property_name, 0);
    
    g_object_notify(object_g, property_name_g);
    
    (*env)->ReleaseStringUTFChars(env, property_name, property_name_g);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_g_1object_1thaw_1notify
    (JNIEnv *env, jclass cls, jobject object)
{
	GObject *object_g;
	
    object_g = (GObject *)getPointerFromHandle(env, object);
    
    g_object_thaw_notify(object_g);
}

static JGFuncCallbackRef *propertyNotifRef = NULL;

static void propertyNotificationCB(GObject *gobject, GParamSpec *arg1,
                                   gpointer data)
{
    JGFuncCallbackRef *ref = (JGFuncCallbackRef*) data;
    (* ref->env)->CallVoidMethod(ref->env, 
                                 ref->obj, 
                                 ref->methodID,
                                 getGObjectHandle(ref->env, gobject),
                                 (*ref->env)->NewStringUTF(ref->env, 
                                                           arg1->name));
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_connectNotifySignal
    (JNIEnv *env, jclass cls, jobject objhandle, jobject object, jstring callback)
{
	GObject *object_g;
	const char *funcname;
	const char *sig = "(Lorg/gnu/glib/Handle;Ljava/lang/String;)V";
	
    funcname = (*env)->GetStringUTFChars(env, callback, NULL);
    object_g = (GObject *)getPointerFromHandle(env, objhandle);

    propertyNotifRef = g_new( JGFuncCallbackRef, 1 );
    propertyNotifRef->env = env;
    propertyNotifRef->obj = (* env)->NewGlobalRef(env, object);

    // Get method id for the callback method name.
    propertyNotifRef->methodID = 
        (*env)->GetMethodID(env, 
                            (*env)->GetObjectClass(env, propertyNotifRef->obj),
                            funcname, sig );
    if ( propertyNotifRef->methodID == NULL ) {
        g_warning( "Can't find %s%s.\n", funcname, sig );
        g_free( propertyNotifRef );
        propertyNotifRef = NULL;
        (*env)->ReleaseStringUTFChars(env, callback, funcname);
        return;
    }
    g_object_connect(object_g, "signal::notify", 
                     propertyNotificationCB, propertyNotifRef, NULL);

    (*env)->ReleaseStringUTFChars(env, callback, funcname);
}

JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_disconnectNotifySignal
    (JNIEnv *env, jclass cls, jobject object)
{
	GObject *object_g;
	
    if ( propertyNotifRef ) {
        object_g = (GObject *)getPointerFromHandle(env, object);
        
        g_object_disconnect(object_g, "any_signal::notify",
                            propertyNotificationCB, propertyNotifRef, NULL);
        g_free( propertyNotifRef );
    }
}

JNIEXPORT jint JNICALL Java_org_gnu_glib_GObject_getGTypeOfProperty
    (JNIEnv* env, jclass cls, jobject object, jstring prop) 
{
	GObject *object_g;
	const gchar *prop_g;
	GParamSpec *pspec;
	
    object_g = (GObject *)getPointerFromHandle(env, object);
    prop_g = (const gchar *)(*env)->GetStringUTFChars(env, prop, 0);

    pspec = g_object_class_find_property(G_OBJECT_GET_CLASS(object_g), prop_g);
    
    (*env)->ReleaseStringUTFChars(env, prop, prop_g);
    if ( !pspec )
        return -1;
    return pspec->value_type;
}

JNIEXPORT jobject JNICALL Java_org_gnu_glib_GObject_instantiateJGObjectFromGType
    (JNIEnv* env, jclass cls, int type, jobject handle) 
{
    jthrowable exc;
    const gchar* jtype;
    jclass clazz;
    jobject localObj;
    jmethodID handlemid;
    
    jtype = javatype_from_gtktype((GType)type);
    clazz = (* env)->FindClass(env, jtype);
    exc = (*env)->ExceptionOccurred(env);
    if (exc) {
        g_warning( "Can't find class: %s\n", jtype );
    	(*env)->ExceptionDescribe(env);
    	(*env)->ExceptionClear(env);
        return NULL;
    }
    
    if (clazz == NULL) {
        clazz = (* env)->FindClass(env, "org/gnu/glib/GObject");
        if (clazz == NULL) {
            // Should never happen!
            return NULL;
        }
    }
    
    g_free((gpointer)jtype);
    
    localObj = (* env)->AllocObject(env, clazz);
    handlemid = (* env)->GetMethodID(env, clazz, 
    		"setHandle", "(Lorg/gnu/glib/Handle;)V");
    exc = (*env)->ExceptionOccurred(env);
    if (exc) {
        g_warning( "No such method: setHandle(Lorg/gnu/glib/Handle;)V\n" );
    	(*env)->ExceptionDescribe(env);
    	(*env)->ExceptionClear(env);
        return NULL;
    }

    (* env)->CallVoidMethod(env, localObj, handlemid, handle);
    return localObj;
}

JNIEXPORT jint JNICALL Java_org_gnu_glib_GObject_getIntFromHandle
    (JNIEnv* env, jclass cls, jobject handle) 
{
	void *handle_g;
	
    handle_g = getPointerFromHandle(env, handle);
    return (jint) *(gint*)handle;
}

JNIEXPORT jstring JNICALL Java_org_gnu_glib_GObject_getStringFromHandle
    (JNIEnv* env, jclass cls, jobject handle) 
{
	void *handle_g;
	
    handle_g = getPointerFromHandle(env, handle);
    return (*env)->NewStringUTF(env, (const char *)handle_g );
}


#ifdef ZERO
/*
 * My test property setting method.  :-)
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_GObject_setTestProperty
    (JNIEnv* env, jclass cls, jobject object, jstring prop, jobject val) 
{
	GObject *object_g;
	const gchar *prop_g;
	gpointer val_g;
	GParamSpec *pspec;
	GValue *gval;
	
    object_g = (GObject *)getPointerFromHandle(env, object);
    prop_g = (const gchar *)(*env)->GetStringUTFChars(env, prop, 0);
    val_g = (gpointer)getPointerFromHandle(env, val);

    pspec = g_object_class_find_property(G_OBJECT_GET_CLASS(object_g), prop_g);
    if ( !pspec ) {
   		(*env)->ReleaseStringUTFChars(env, prop, prop_g);
        return;
    }

    gval = (GValue *) g_malloc(sizeof(GValue));
    gval->g_type = 0;
    gval = g_value_init( gval, pspec->value_type );

    if ( G_VALUE_HOLDS_BOXED( gval ) ) {
        g_value_set_boxed( gval, val_g );
    } else if ( G_VALUE_HOLDS_OBJECT( gval ) ) {
        g_value_set_object( gval, val_g );
    } else if ( G_VALUE_HOLDS_POINTER( gval ) ) {
        g_value_set_pointer( gval, val_g );
    } else {
		(*env)->ReleaseStringUTFChars(env, prop, prop_g);
        return;
    }
    g_object_set_property(object_g, prop_g, gval );

	(*env)->ReleaseStringUTFChars(env, prop, prop_g);
}
#endif

#ifdef __cplusplus
}

#endif
