/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib.h>
#include <jg_jnu.h>
#include "glib_java.h"

#include "org_gnu_glib_Error.h"
#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * Class:     org.gnu.glib.Error
 * Method:    getDomain
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Error_getDomain 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    GError *obj_g;
    jint value;
	
    obj_g = (GError *)getPointerFromHandle(env, obj);
    
    value = (jint) obj_g->domain;
    
    return value;
}

/*
 * Class:     org.gnu.glib.Error
 * Method:    getCode
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Error_getCode 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    GError *obj_g;
    jint value;
	
    obj_g = (GError *)getPointerFromHandle(env, obj);
    
    value = (jint) obj_g->code;
    
    return value;
}

/*
 * Class:     org.gnu.glib.Error
 * Method:    getMessage
 * Signature: (Lorg/gnu/glib/Handle;)java/Lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_gnu_glib_Error_getMessage 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    GError *obj_g;
    const gchar *result_g;
	
    obj_g = (GError *)getPointerFromHandle(env, obj);
    
    result_g = obj_g->message;
    
    return (*env)->NewStringUTF(env, result_g);
}

/*
 * Class:     org.gnu.glib.Error
 * Method:    g_error_new_literal
 * Signature: (Lorg/gnu/glib/Handle;Ijava.lang.String;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Error_g_1error_1new_1literal 
  (JNIEnv *env, jclass cls, jint domain, jint code, jstring message) 
{
    //GQuark *domain_g;
    GQuark domain_g;
    const gchar* message_g;
    GError *value;
	
    //domain_g = (GQuark *)getPointerFromHandle(env, domain);
    domain_g = (GQuark)domain;
    message_g = (*env)->GetStringUTFChars(env, message, 0);
    
    value = g_error_new_literal(domain_g, code, message_g);
    
    (*env)->ReleaseStringUTFChars(env, message, message_g);
    return getStructHandle(env, value, NULL, (JGFreeFunc)g_error_free);
}

#ifdef __cplusplus
}

#endif
