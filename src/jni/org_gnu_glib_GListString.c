/* 
 * Java wrapper for GList of strings.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 * 
 * Author: Dan Bornstein, danfuzz@milk.com
 * Copyright 2000 Dan Bornstein, all rights reserved. 
 */

#include <jni.h>
#include "jg_jnu.h"

#include "org_gnu_glib_GListString.h"
#ifdef __cplusplus
extern "C" {
#endif


/*
 * NOTE: This class is deprecated.  Use the standard Java collection
 * classes or arrays instead.
 */


/* ----------------------------------------------------------------------------
 * helper functions 
 */

/* Free the data pointer associated with a GList.
 *
 * data: the pointer to free
 * unused: unused, but required for the g_list_foreach protocol
 */
static void freeGListData (gpointer data, gpointer unused)
{
    g_free(data);
}

/* ----------------------------------------------------------------------------
 * native method declarations
 */

/* Native code for gnu.gtk.GList.new0(). */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_GListString_new0
    (JNIEnv *env, jobject obj)
{
    return (jobject) NULL;
}

/* Native code for gnu.gtk.GList.finalize0(). */
JNIEXPORT void JNICALL Java_org_gnu_glib_GListString_finalize0
    (JNIEnv *env, jobject obj, jobject handle)
{
	GList *peer;
	
    if (handle != 0) {
		peer = (GList *) getPointerFromHandle(env, handle);

		g_list_foreach (peer, &freeGListData, NULL);
		g_list_free (peer);
    }
}

/* Native code for gnu.gtk.GList.append0(). */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_GListString_append0
    (JNIEnv *env, jobject obj, jobject handle, jstring str)
{
	GList *peer;
	gchar *newstring;
	
    peer = (GList*)getPointerFromHandle(env, handle);
    newstring = (gchar *)(*env)->GetStringUTFChars(env, str, 0);
    
    peer = g_list_append(peer, newstring);
    g_assert(peer != 0);
    
    (*env)->ReleaseStringUTFChars(env, str, newstring);
    return updateHandle(env, handle, peer);
}

#ifdef __cplusplus
}
#endif
