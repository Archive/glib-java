/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2006 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib.h>
#include <jg_jnu.h>
#include "glib_java.h"

#include "org_gnu_glib_Value.h"
#ifdef __cplusplus
extern "C" 
{
#endif

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_init
 * Signature: (I)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Value_g_1value_1init 
  (JNIEnv *env, jclass cls, jint type) 
{
    GValue *value_g;
	
    value_g = (GValue *) g_malloc(sizeof(GValue));
    value_g->g_type = 0;
    value_g = g_value_init (value_g,(GType)type);
    
    jobject handle = getStructHandle(env, value_g, NULL, g_free);
    return handle;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_type
 * Signature: (I)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Value_g_1value_1type 
  (JNIEnv *env, jclass cls, jobject value)
{
    GValue *value_g;
    GType type;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    type = G_VALUE_TYPE(value_g);
    
    return type;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_copy
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Value_g_1value_1copy 
  (JNIEnv *env, jclass cls, jobject srcValue) 
{
    // This might be buggy!  Might we need to allocate destValue?

    GValue *srcValue_g;
    GValue *destValue = NULL;
	
    srcValue_g = (GValue *)getPointerFromHandle(env, srcValue);
	
    g_value_copy (srcValue_g, destValue);
    
    return getStructHandle(env, destValue, NULL, g_free);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_reset
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Value_g_1value_1reset 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    value_g = g_value_reset(value_g);
    
    // Just return the original value, since creating a new Handle
    // with getStructHandle would cause 2 Handles to refer to the same
    // GValue struct.
    return value;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_unset
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1unset 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    g_value_unset (value_g);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_char
 * Signature: (Lorg/gnu/glib/Handle;B)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1char 
  (JNIEnv *env, jclass cls, jobject value, jbyte vChar) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);

    g_value_set_char (value_g, (gchar)vChar);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_char
 * Signature: (Lorg/gnu/glib/Handle;)B
 */
JNIEXPORT jbyte JNICALL Java_org_gnu_glib_Value_g_1value_1get_1char 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gchar vchar;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    vchar = g_value_get_char (value_g);
    
    return (jbyte)vchar;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_boolean
 * Signature: (Lorg/gnu/glib/Handle;Z)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1boolean 
  (JNIEnv *env, jclass cls, jobject value, jboolean vBoolean) 
{
    GValue *value_g;

    value_g = (GValue *)getPointerFromHandle(env, value);
    
    g_value_set_boolean (value_g, (gboolean)vBoolean);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_boolean
 * Signature: (Lorg/gnu/glib/Handle;)Z
 */
JNIEXPORT jboolean JNICALL Java_org_gnu_glib_Value_g_1value_1get_1boolean 
(JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gboolean result;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    result = g_value_get_boolean (value_g);
    
    return result;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_int
 * Signature: (Lorg/gnu/glib/Handle;I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1int 
   (JNIEnv *env, jclass cls, jobject value, jint vInt) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    g_value_set_int (value_g, (gint32)vInt);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_int
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Value_g_1value_1get_1int 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gint result = 0;
    gboolean result_good = TRUE;
	
    value_g = (GValue *)getPointerFromHandle(env, value);

    if (G_VALUE_HOLDS_INT(value_g)) {
        result = g_value_get_int (value_g);
    } else if (G_VALUE_HOLDS_UINT(value_g)) {
        result = g_value_get_uint (value_g);
    } else if (G_VALUE_HOLDS_ENUM(value_g)) {
        result = g_value_get_enum (value_g);
    } else {
        result_good = FALSE;
    }

    if (FALSE == result_good) {
    	JNU_ThrowByName(env, "java.lang.IllegalArgumentException",
                "Value does not hold an integer.");
    }

    return result;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_long
 * Signature: (Lorg/gnu/glib/handleJ)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1long 
  (JNIEnv *env, jclass cls, jobject value, jlong vLong) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    /*
     * Java's longs are defined as 64 bit wide signed integer values
     * This corresponds to gint64 in glib/gobject terms.
     */

    g_value_set_int64(value_g, (gint64) vLong);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_long
 * Signature: (Lorg/gnu/glib/Handle;)J
 */
JNIEXPORT jlong JNICALL Java_org_gnu_glib_Value_g_1value_1get_1long 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    jlong result = 0;
    jboolean result_good = TRUE;
	
    value_g = (GValue *)getPointerFromHandle(env, value);

    if (G_VALUE_HOLDS_LONG(value_g)) {
        result = g_value_get_long (value_g);
    } else if (G_VALUE_HOLDS_ULONG(value_g)) {
        result = g_value_get_ulong (value_g);
    } else if (G_VALUE_HOLDS_INT64(value_g)) {
        result = g_value_get_int64 (value_g);
    } else if (G_VALUE_HOLDS_UINT64(value_g)) {
        result = g_value_get_uint64 (value_g);
    } else {
        result_good = FALSE;
    }
    
    if (FALSE == result_good) {
    	JNU_ThrowByName(env, "java.lang.IllegalArgumentException",
                "Value does not hold a long.");
    }

    return result;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_float
 * Signature: (Lorg/gnu/glib/Handle;D)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1float 
  (JNIEnv *env, jclass cls, jobject value, jdouble vFloat) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);

    g_value_set_float (value_g, (gfloat)vFloat);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_float
 * Signature: (Lorg/gnu/glib/Handle;)D
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_glib_Value_g_1value_1get_1float 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gfloat result;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    result = g_value_get_float (value_g);
    
    return result;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_double
 * Signature: (Lorg/gnu/glib/Handle;D)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1double 
  (JNIEnv *env, jclass cls, jobject value, jdouble vDouble) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);

    g_value_set_double (value_g, (gdouble)vDouble);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_double
 * Signature: (Lorg/gnu/glib/Handle;)D
 */
JNIEXPORT jdouble JNICALL Java_org_gnu_glib_Value_g_1value_1get_1double 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gdouble result;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    result = g_value_get_double (value_g);
    
    return result;
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_string
 * Signature: (Lorg/gnu/glib/Handle;Ljava.lang.String)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1string 
  (JNIEnv *env, jclass cls, jobject value, jstring vString) 
{
    GValue *value_g;
    const gchar *vString_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    vString_g = (*env)->GetStringUTFChars(env, vString, 0);
    g_value_set_string (value_g, vString_g);
    
    (*env)->ReleaseStringUTFChars(env, vString, vString_g);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_string
 * Signature: (Lorg/gnu/glib/Handle;)Ljava.lang.String
 */
JNIEXPORT jstring JNICALL Java_org_gnu_glib_Value_g_1value_1get_1string 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    const gchar *result;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    result = g_value_get_string ((const GValue*)value_g);
    
    return (*env)->NewStringUTF(env, result);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_boxed
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1boxed 
  (JNIEnv *env, jclass cls, jobject value, jobject obj) 
{
    GValue *value_g;
    gpointer *obj_g;
	
    value_g = (GValue*)getPointerFromHandle(env, value);
    obj_g = (gpointer*)getPointerFromHandle(env, obj);
	
    g_value_set_boxed (value_g, obj_g);
}

/*
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Value_g_1value_1get_1boxed 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gpointer result;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    
    result = (gpointer)g_value_get_boxed (value_g);
    
    return getGBoxedHandle(env, result, -1, NULL, g_free);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_java_object
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1java_1object 
  (JNIEnv *env, jclass cls, jobject value, jobject obj) 
{
    GValue *value_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
	
    if ( G_VALUE_HOLDS_OBJECT(value_g) )
        g_value_set_object (value_g, (gpointer)getPointerFromHandle(env, obj));
    else if ( G_VALUE_HOLDS_POINTER(value_g) )
        // g_value_set_pointer (value_g, (*env)->NewGlobalRef(env, obj));
        g_value_set_pointer (value_g,  obj);
    else if ( G_VALUE_HOLDS_BOXED(value_g) )
        //  g_value_set_pointer (value_g, (*env)->NewGlobalRef(env, obj));
        g_value_set_pointer (value_g,obj);
    else
        g_value_set_pointer (value_g, (*env)->NewGlobalRef(env, obj));
}

/*
 * Function used by Java_org_gnu_glib_Value_g_1value_1get_1java_1object
 * to return a new java.lang.Integer object with the value integer_j
 */
static jobject createNewIntegerInstance(JNIEnv *env, jint integer_j) 
{
    jclass integerClass = (*env)->FindClass(env, "java/lang/Integer");
    if (integerClass == NULL)
        return (jobject) NULL;
    jmethodID constructor = (*env)->GetMethodID(env, integerClass,
                                                "<init>", "(I)V");
    if (constructor == NULL)
        return (jobject) NULL;
    return (*env)->NewObject(env, integerClass, constructor, integer_j);
}

/*
 * Function used by Java_org_gnu_glib_Value_g_1value_1get_1java_1object
 * to return a new java.lang.Long object with the value long_j
 */
static jobject createNewLongInstance(JNIEnv *env, jlong long_j) 
{
    jclass longClass = (*env)->FindClass(env, "java/lang/Long");
    if (longClass == NULL)
        return (jobject) NULL;
    jmethodID constructor = (*env)->GetMethodID(env, longClass,
                                                "<init>", "(J)V");
    if (constructor == NULL)
        return (jobject) NULL;
    return (*env)->NewObject(env, longClass, constructor, long_j);
}

/*
 * Function used by Java_org_gnu_glib_Value_g_1value_1get_1java_1object
 * to return a new java.lang.Float object with the value float_j
 */
static jobject createNewFloatInstance(JNIEnv *env, jfloat float_j) 
{
    jclass floatClass = (*env)->FindClass(env, "java/lang/Float");
    if (floatClass == NULL)
        return (jobject) NULL;
    jmethodID constructor = (*env)->GetMethodID(env, floatClass,
                                                "<init>", "(F)V");
    if (constructor == NULL)
        return (jobject) NULL;
    return (*env)->NewObject(env, floatClass, constructor, float_j);
}

/*
 * Function used by Java_org_gnu_glib_Value_g_1value_1get_1java_1object
 * to return a new java.lang.Double object with the value double_j
 */
static jobject createNewDoubleInstance(JNIEnv *env, jdouble double_j) 
{
    jclass doubleClass = (*env)->FindClass(env, "java/lang/Double");
    if (doubleClass == NULL)
        return (jobject) NULL;
    jmethodID constructor = (*env)->GetMethodID(env, doubleClass,
                                                "<init>", "(D)V");
    if (constructor == NULL)
        return (jobject) NULL;
    return (*env)->NewObject(env, doubleClass, constructor, double_j);
}

/*
 * Function used by Java_org_gnu_glib_Value_g_1value_1get_1java_1object
 * to return a new java.lang.Boolean object with the value boolean_j
 */
static jobject createNewBooleanInstance(JNIEnv *env, jboolean boolean_j) 
{
    jclass booleanClass = (*env)->FindClass(env, "java/lang/Boolean");
    if (booleanClass == NULL)
        return (jobject) NULL;
    jmethodID constructor = (*env)->GetMethodID(env, booleanClass,
                                                "<init>", "(Z)V");
    if (constructor == NULL)
        return (jobject) NULL;
    return (*env)->NewObject(env, booleanClass, constructor, boolean_j);
}

#define SIGNATURE_BUF_SIZE	64
/*
 * Function used by Java_org_gnu_glib_Value_g_1value_1get_1java_1object
 * to return a java.lang.Enum object with the value enum_int
 */
static jobject getEnumInstance(JNIEnv *env, GValue *value_g) 
{
    char *enumClassName = javaobject_from_gtktype(G_VALUE_TYPE(value_g));
    char internSignature[SIGNATURE_BUF_SIZE];

    jclass enumClass = (*env)->FindClass(env, enumClassName);
    if (enumClass == NULL)
        return (jobject) NULL;
    g_snprintf(internSignature, SIGNATURE_BUF_SIZE, "(I)L%s;", enumClassName);
    jmethodID internMethod = (*env)->GetStaticMethodID(env, enumClass,
                                                       "intern", 
                                                       internSignature);
    if (internMethod == NULL)
        return (jobject) NULL;
    return (*env)->CallStaticObjectMethod(env, enumClass, internMethod,
                                          (jint) g_value_get_enum(value_g));
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_java_object
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Value_g_1value_1get_1java_1object 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gpointer pointer_g;
    jint integer_j;
    jlong long_j;
    jfloat float_j;
    jdouble double_j;
    jboolean boolean_j;

    value_g = (GValue *)getPointerFromHandle(env, value);
    
    if ( G_VALUE_HOLDS_OBJECT(value_g) ) {
        pointer_g = (gpointer) (g_value_get_object (value_g));
        return getGObjectHandle(env, pointer_g);
    } else if ( G_VALUE_HOLDS_POINTER(value_g) ) {
        pointer_g = (gpointer) (g_value_get_pointer (value_g));
        // MEM_MGT_NOTE: is this a leak?
        return (jobject)pointer_g;
    } else if ( G_VALUE_HOLDS_BOXED(value_g) ) {
        pointer_g = (gpointer) (g_value_get_boxed (value_g));
    } else if (G_VALUE_HOLDS_INT(value_g)) {
        /* create a new java.lang.Integer object with the right value */
        integer_j = (jint) g_value_get_int(value_g);
        return createNewIntegerInstance(env, integer_j);
    } else if (G_VALUE_HOLDS_UINT(value_g)) {
        /* create a new java.lang.Integer object with the right value */
        integer_j = (jint) g_value_get_uint(value_g);
        return createNewIntegerInstance(env, integer_j);
    } else if (G_VALUE_HOLDS_LONG(value_g)) {
        /* create a new java.lang.Long object with the right value */
        long_j = (jlong) g_value_get_long(value_g);
        return createNewLongInstance(env, long_j);
    } else if (G_VALUE_HOLDS_ULONG(value_g)) {
        /* create a new java.lang.Long object with the right value */
        long_j = (jlong) g_value_get_ulong(value_g);
        return createNewLongInstance(env, long_j);
    } else if (G_VALUE_HOLDS_INT64(value_g)) {
        /* create a new java.lang.Long object with the right value */
        long_j = (jlong) g_value_get_uint64(value_g);
        return createNewLongInstance(env, long_j);
    } else if (G_VALUE_HOLDS_FLOAT(value_g)) {
        /* create a new java.lang.Float object with the right value */
        float_j = (jfloat) g_value_get_float(value_g);
        return createNewFloatInstance(env, float_j);
    } else if (G_VALUE_HOLDS_DOUBLE(value_g)) {
        /* create a new java.lang.Double object with the right value */
        double_j = (jdouble) g_value_get_double(value_g);
        return createNewDoubleInstance(env, double_j);
    } else if (G_VALUE_HOLDS_BOOLEAN(value_g)) {
        /* create a new java.lang.Boolean object with the right value */
        boolean_j = (jboolean) g_value_get_boolean(value_g);
        return createNewBooleanInstance(env, boolean_j);
    } else if (G_VALUE_HOLDS_ENUM(value_g)) {
        /* create a new java.lang.Boolean object with the right value */
        integer_j = (jint) g_value_get_enum(value_g);
        return getEnumInstance(env, value_g);

    } else {
        pointer_g = (gpointer) (g_value_get_pointer (value_g));
    }
    
    // MEM_MGT_NOTE: need to decide what type of handle to create?
    return getHandleFromPointer(env, pointer_g);
}


/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_set_pointer
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Value_g_1value_1set_1pointer 
  (JNIEnv *env, jclass cls, jobject value, jobject ptr) 
{
    GValue *value_g;
    gpointer ptr_g;
	
    value_g = (GValue *)getPointerFromHandle(env, value);
    ptr_g = (gpointer)getPointerFromHandle(env, ptr);
    
    g_value_set_pointer (value_g, ptr);
}

/*
 * Class:     org.gnu.glib.Value
 * Method:    g_value_get_pointer
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_Value_g_1value_1get_1pointer 
  (JNIEnv *env, jclass cls, jobject value) 
{
    GValue *value_g;
    gpointer ptr_g;

    value_g = (GValue *)getPointerFromHandle(env, value);
    
    ptr_g = (gpointer)g_value_get_pointer (value_g);
    
    // MEM_MGT_NOTE: need to decide what type of handle to create.
    return getHandleFromPointer(env, ptr_g);
}


#ifdef __cplusplus
}

#endif
