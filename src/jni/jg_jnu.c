/* 
 * JNI utility functions, from the book "The Java Native Interface" by
 * Sheng Liang.
 */

#include "jg_jnu.h"
#include <stddef.h>
#include <glib/gmem.h>
#include <glib.h>
#include <string.h>


#ifdef __cplusplus
extern "C" {
#endif

static JavaVM *cached_jvm;

static jfieldID
getPointerField (JNIEnv *env)
{
  static jfieldID pointerField = NULL;
  if (pointerField == NULL) {
    jclass handleClass = getHandleClass(env);
    if (handleClass == NULL)
      return NULL;
# if GLIB_SIZEOF_VOID_P == 8
    pointerField = (*env)->GetFieldID(env, handleClass, 
				      "pointer", "J");
#  define GET_POINTER_FIELD GetLongField
#  define SET_POINTER_FIELD SetLongField
#  define JPOINTER jlong
# elif GLIB_SIZEOF_VOID_P == 4
    pointerField = (*env)->GetFieldID(env, handleClass, 
				      "pointer", "I");
#  define GET_POINTER_FIELD GetIntField
#  define SET_POINTER_FIELD SetIntField
#  define JPOINTER jint
# else
#	error "Your system has an unsupported pointer size"
#endif
  }
  return pointerField;
}

/** From "The Java Native Interface", section 8.4.1 */
JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *jvm, void *reserved)
{
    cached_jvm = jvm;
    return JNI_VERSION_1_2;
}

/** From "The Java Native Interface", section 8.4.1 */
JNIEnv *JG_JNU_GetEnv(void)
{
  void *env;
  (*cached_jvm)->GetEnv(cached_jvm, &env, JNI_VERSION_1_2);
  return env;
}

/** Oringally from "The Java Native Interface", section 6.1.2; modified
 * to fix a potential crasher. */
void JNU_ThrowByName(JNIEnv *env, const char *name, const char *msg)
{
    /*
     * It turns out different Java implemenations are finicky about the syntax
     * of the string used to lookup a class. "Ljava/lang/Blah;" makes GCJ
     * unhappy; "java.lang.Blah" makes Sun Java barf. The JNI standard actually
     * specifies "java/lang/Blah" only.
     *
     * If the class can't be found (or the above problem causes a misfire) then
     * ClassNotFoundException is raised by the FindClass() call, and null is
     * returned.
     *
     * We could probably check the syntax ourselves, but the class loader is
     * going to do so for us anyway.
     */
    jclass cls = (*env)->FindClass(env, name);

    if (cls != NULL) {
        (*env)->ThrowNew(env, cls, msg);
        /*
         * And, since its valid, we need to free the local jclass ref that
         * FindClass() gave us...
         */
        (*env)->DeleteLocalRef(env, cls);
    }
}

gchar** getStringArray(JNIEnv *env, jobjectArray anArray) 
{
    jsize len = (*env)->GetArrayLength(env, anArray);
    gchar **strArray = g_malloc0((len+1) * sizeof(gchar*));
    int index;
    jstring aString;

    for (index = 0; index < len; index++) {
        aString = (jstring)(*env)->GetObjectArrayElement(env, anArray, 
                                                         (jsize)index);
        strArray[index] = (gchar*)(*env)->GetStringUTFChars(env, 
                                                            aString, NULL);
    }
    strArray[len] = NULL;
	
    return strArray;
}

void freeStringArray(JNIEnv *env, jobjectArray anArray, gchar** str)
{
    jsize len = (*env)->GetArrayLength(env, anArray);
    int index;
    jstring aString;

    for (index = 0; index < len; index++) {
        aString = (jstring)(*env)->GetObjectArrayElement(env, anArray, 
                                                         (jsize)index);
        (*env)->ReleaseStringUTFChars(env, aString, str[index]);
    }
}

jobjectArray getJavaStringArray(JNIEnv *env, const gchar* const * str) {
	if (str == NULL)	{
		return NULL;	
	}
    int size = 0;
    // Length of array.
    while( str[size] ) { size++; }
    //
    jclass stringCls = (*env)->FindClass(env, "java/lang/String" );
    jobjectArray ret = (*env)->NewObjectArray(env, size, stringCls, NULL);
    int i = 0;
    for( i = 0; i < size; i++ ) {
        (*env)->SetObjectArrayElement(env, ret, i, (*env)->NewStringUTF(env, str[i]));
    }
    return ret;
}

jobjectArray getSList(JNIEnv *env, GSList* list)
{
    jobjectArray ar;
    int i = 0;
    jclass handleClass;
    handleClass = getHandleClass(env);
	
    if (NULL == list)
        return NULL;
    ar = (*env)->NewObjectArray(env, g_slist_length(list), handleClass, NULL);
    while (list!=NULL) {
        (*env)->SetObjectArrayElement(env, ar, i, getHandleFromPointer(env, list->data));
        list = g_slist_next(list);
        i++;
    }
    return ar;
}

jobjectArray getList(JNIEnv *env, GList* list)
{
    jobjectArray ar;
    int i = 0;
    jclass handleClass;
    handleClass = getHandleClass(env);

    if (NULL == list)
        return NULL;
    ar = (*env)->NewObjectArray(env, g_list_length(list), handleClass, NULL);
    while (list!=NULL) {
        (*env)->SetObjectArrayElement(env, ar, i, getHandleFromPointer(env, list->data));
        list = g_list_next(list);
        i++;
    }
    return ar;
}

void* 
getPointerFromHandle (JNIEnv* env, jobject handle) 
{
  void* pointer;
  
  if (!handle)
    return NULL;
  
  jfieldID pointerField = getPointerField (env);
  if (pointerField == NULL)
    return NULL;
  
  pointer = (void*)(*env)->GET_POINTER_FIELD(env, handle, pointerField);
  return pointer;
}


jobject
getHandleFromPointer (JNIEnv* env, void* pointer)
{
  static jmethodID constructor = NULL;
  jobject handle;
  
  jclass handleClass = getHandleClass(env);
  if (handleClass == NULL)
    return NULL;
  
  jfieldID pointerField = getPointerField (env);
  if (pointerField == NULL)
    return NULL;
  
  if (constructor == NULL) {
    constructor = (*env)->GetMethodID(env, handleClass, "<init>", "()V");
    if (constructor == NULL)
      return NULL;
  }
  handle = (*env)->NewObject(env, handleClass, constructor);
  
  (*env)->SET_POINTER_FIELD(env, handle, pointerField, (JPOINTER) pointer);
  return handle;
}

void** getPointerArrayFromHandles(JNIEnv* env, jobjectArray handles)
{
  int index = 0;
  void** ret = NULL;
  jclass handleClass;
  jsize len = (*env)->GetArrayLength(env, handles);
  jobject obj;
  handleClass = getHandleClass(env);
  
  if (handleClass == NULL)
    return NULL;
  ret = g_malloc(sizeof(void*)*len);
  
  jfieldID pointerField = getPointerField (env);
  if (pointerField == NULL)
    return NULL;
  
  for (index = 0; index < len; index++) {
    obj = (*env)->GetObjectArrayElement(env, handles, index);
    ret[index] = (void*)(*env)->GET_POINTER_FIELD(env, obj, pointerField);
  }
  return ret;
}

/*
 * Copy the contents pointed to by the handles into an array of structures.
 * The parameter update_handles should be "true" if you want to update the 
 * handles to refer to the corresponding new locations in the array.  The
 * parameter delete_originals should be "true" if you want to delete the memory
 * pointed to by the original handles.
 */
void* getArrayFromHandles(JNIEnv* env, jobjectArray handles, 
                          int size_of_object, gboolean update_handles,
                          gboolean delete_originals)
{
    int index = 0;
    void *ret = NULL;
    jsize len = (*env)->GetArrayLength(env, handles);
    jobject obj;

    ret = g_malloc(size_of_object * len);
    void *newloc = ret;
    void *origloc = NULL;
    for (index = 0; index < len; index++) {
        obj = (*env)->GetObjectArrayElement(env, handles, index);
        origloc = getPointerFromHandle(env, obj);
        memcpy(newloc, origloc, size_of_object);

        if (update_handles)
            updateHandle(env, obj, newloc);

        newloc += size_of_object;

        if (delete_originals)
            g_free(origloc);
    }
    return ret;
}

jobjectArray getHandleArrayFromPointers(JNIEnv* env, void** pointer, int numPtrs)
{
    return getHandlesFromPointers(env, pointer, numPtrs, getHandleFromPointer);
}

jobjectArray getHandlesFromPointers(JNIEnv* env, void** pointer, int numPtrs,
		GetHandleFunc function)
{
	int index = 0;
    jobjectArray ret;
    jclass handleClass;
    handleClass = getHandleClass(env);
	
    ret = (*env)->NewObjectArray(env, numPtrs, handleClass, NULL);
    for (index = 0; index < numPtrs; index++)
    {
    	jobject handle = function(env, pointer[index]);
    	(*env)->SetObjectArrayElement(env, ret, index, handle);
    }
    return ret;
}

jobjectArray getHandleArrayFromGList(JNIEnv* env, GList* list)
{
	return getHandlesFromGList(env, list, getHandleFromPointer);
}

jobjectArray getHandlesFromGList(JNIEnv* env, GList* list, GetHandleFunc function)
{
	int index = 0;
    GList *it;
    jobjectArray ret;
    jclass handleClass;
    handleClass = getHandleClass(env);
	
    ret = (*env)->NewObjectArray(env, g_list_length(list), handleClass, NULL);
    for (it = list; it != NULL; it = it->next, ++index)
    {
    	jobject handle = function(env, it->data);
    	(*env)->SetObjectArrayElement(env, ret, index, handle);
    }
    return ret;
}

jobjectArray getHandleArrayFromGSList(JNIEnv* env, GSList* list)
{
    return getHandlesFromGSList(env, list, getHandleFromPointer);
}

jobjectArray getHandlesFromGSList(JNIEnv* env, GSList* list, GetHandleFunc
		function)
{
	int index = 0;
    GSList *it;
    jobjectArray ret;
    jclass handleClass;
    handleClass = getHandleClass(env);
	
    ret = (*env)->NewObjectArray(env, g_slist_length(list), handleClass, NULL);
    for (it = list; it != NULL; it = it->next, ++index)
    {
    	jobject handle = function(env, it->data);
    	(*env)->SetObjectArrayElement(env, ret, index, handle);
    }
    return ret;	
}

jobjectArray getHandleArray(JNIEnv* env, int length)
{
    jobjectArray ret;
    jclass handleClass;
    handleClass = getHandleClass(env);
	
    ret = (*env)->NewObjectArray(env, length, handleClass, NULL);
    return ret;
}

GSList* getGSListFromHandles(JNIEnv* env, jobjectArray handles)
{
  if (!handles)
    return NULL;

  int index = 0;
  GSList* ret = NULL;
  jclass handleClass;
  jsize len = (*env)->GetArrayLength(env, handles);
  jobject obj;
  handleClass = getHandleClass(env);
  
  if (handleClass == NULL)
    return NULL;
  
  jfieldID pointerField = getPointerField (env);
  if (pointerField == NULL)
    return NULL;

  for (index = 0; index < len; index++) {
    obj = (*env)->GetObjectArrayElement(env, handles, index);
    ret = g_slist_append(ret, (gpointer)(*env)->GET_POINTER_FIELD(env, obj, pointerField));
  }
  return ret;
}

GList* getGListFromHandles(JNIEnv* env, jobjectArray handles)
{
  if (!handles)
    return NULL;
  
  int index = 0;
  GList* ret = NULL;
  jclass handleClass;
  jsize len = (*env)->GetArrayLength(env, handles);
  jobject obj;
  handleClass = getHandleClass(env);
  
  if (handleClass == NULL)
    return NULL;

  jfieldID pointerField = getPointerField (env);
  if (pointerField == NULL)
    return NULL;
  
  for (index = 0; index < len; index++) {
    obj = (*env)->GetObjectArrayElement(env, handles, index);
    ret = g_list_append(ret, (gpointer)(*env)->GET_POINTER_FIELD(env, obj, pointerField));
  }
  return ret;
}

GList* getGListFromStringArray(JNIEnv* env, jobjectArray strings) {
    if (!strings)
        return NULL;
    
    int index = 0;
    GList* ret = NULL;
    jsize len = (*env)->GetArrayLength(env, strings);

    jstring strobj;
    gchar *newstring;

    for(index = 0; index < len; index++ ) {
        strobj = (jstring)(*env)->GetObjectArrayElement(env, strings, index);
        newstring = (gchar *)(*env)->GetStringUTFChars(env, strobj, 0);
        ret = g_list_append(ret, newstring);
    }

    return ret;
}

void releaseStringArrayInGList(JNIEnv* env, jobjectArray strings, 
                               GList* list) {
	jstring aString;
	GList *it;
    int index = 0;
    for (it = list; it != NULL; it = it->next) {
        aString = (jstring)(*env)->GetObjectArrayElement(env, strings, index);
        (*env)->ReleaseStringUTFChars(env, aString, (gchar*)it->data);
        index++;
    }
}

jobject
updateHandle(JNIEnv* env, jobject handle, void* pointer)
{
  jclass handleClass;
  handleClass = getHandleClass(env);
  
  if (handleClass == NULL)
    return handle;

  jfieldID pointerField = getPointerField (env);
  if (pointerField == NULL)
    return handle;
  (*env)->SET_POINTER_FIELD (env, handle, pointerField, (JPOINTER) pointer);
  return handle;
}

jclass
getHandleClass (JNIEnv* env)
{
  static jclass handleCls = NULL;
  if (handleCls == NULL)	{
    jclass localHandleCls = NULL;
# if GLIB_SIZEOF_VOID_P == 8
    localHandleCls = (*env)->FindClass(env, "org/gnu/glib/Handle64Bits");
# elif GLIB_SIZEOF_VOID_P == 4
    localHandleCls = (*env)->FindClass(env, "org/gnu/glib/Handle32Bits");
# else
#	error "Your system has an unsupported pointer size"
#endif
    if (localHandleCls == NULL)
      return NULL;
    
    handleCls = (* env)->NewGlobalRef(env, localHandleCls);
    (* env)->DeleteLocalRef(env, localHandleCls);
  }
  
  return handleCls;
}

static GStaticMutex atexit_lock = G_STATIC_MUTEX_INIT;
static GSList *atexit_list = NULL;

void jg_process_atexit(void)
{
	g_static_mutex_lock (&atexit_lock);
	GSList *it;
	for (it = atexit_list; it != NULL; it = it->next)
	{
		GVoidFunc func = (GVoidFunc) it->data;
		func();
	}
	g_slist_free(atexit_list);
	atexit_list = NULL;
	g_static_mutex_unlock (&atexit_lock);
}

void jg_atexit(GVoidFunc func)
{
	g_static_mutex_lock (&atexit_lock);
	atexit_list = g_slist_append(atexit_list, (gpointer) func);
	g_static_mutex_unlock (&atexit_lock);
}

#ifdef __cplusplus
}
#endif
