/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include "jg_jnu.h"
#include <glib.h>

#include "org_gnu_glib_Idle.h"
#ifdef __cplusplus
extern "C" {
#endif

static jfieldID  targetFID;

static void cleanup(JNIEnv *env, jobject obj) {
    // reset id field
    jclass cls = (*env)->GetObjectClass(env, obj);
    jfieldID fid = (*env)->GetFieldID(env, cls, "sourceID", "I");

    //check of the pointer has already been freed
    jint sourceIDValue = (*env)->GetIntField(env, obj, fid);
    if(sourceIDValue != 0){
      (*env)->SetIntField(env, obj, fid, (jint)0);
      
      // delete back pointer
      (*env)->DeleteGlobalRef(env, obj);
    }
}

static gboolean fire_method_invoker(gpointer data)
{
    JNIEnv *env = JG_JNU_GetEnv();
    jobject obj = (jobject)data;
    jboolean exceptionThrown;

    jobject target = (*env)->GetObjectField(env, obj, targetFID);
    jclass cls = (*env)->GetObjectClass(env, target);
    jmethodID mid = (*env)->GetMethodID(env, cls, "fire", "()Z");

    jboolean keepFiring = (*env)->CallBooleanMethod(env, target, mid);
    (*env)->DeleteLocalRef(env, cls);
    (*env)->DeleteLocalRef(env, target);

    exceptionThrown = (*env)->ExceptionCheck(env);
    if (exceptionThrown) {
	// We cannot throw this exception, since this idle was called by GLib.
	(*env)->ExceptionDescribe(env);  // ExceptionDescribe clears exception
	keepFiring = JNI_FALSE;
    }

    if (keepFiring == JNI_FALSE) {
	cleanup(env, obj);
    }
    return keepFiring;
}

/*
 * Class:     org_gnu_glib_Idle
 * Method:    start_idle
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_Idle_start_1idle
    (JNIEnv *env, jobject obj, jint priority)
{
	guint idle;
	gpointer data;
	
	data = (*env)->NewGlobalRef(env, obj);
	
	idle = g_idle_add_full((gint)priority, 
			       fire_method_invoker, 
			       data, NULL);
	
    return (jint)idle;
}

/*
 * Class:     org_gnu_glib_Idle
 * Method:    stop_idle
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Idle_stop_1idle 
    (JNIEnv *env, jobject obj, jint id)
{
    cleanup(env, obj);
    
    g_source_remove((guint)id);
}


/*
 * Class:     org_gnu_glib_Idle
 * Method:    set_priority
 * Signature: (I)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_Idle_set_1priority
    (JNIEnv *env, jobject obj, jint id, jint priority)
{
    GSource * source;

    /* Only look at the default main context for now */
    source = g_main_context_find_source_by_id(NULL, (guint)id);    
    g_source_set_priority(source, (gint)priority);
}


JNIEXPORT void JNICALL Java_org_gnu_glib_Idle_initIDs 
    (JNIEnv *env, jclass cls)
{
    targetFID = (*env)->GetFieldID(env, cls,
				   "target", "Lorg/gnu/glib/Fireable;");
}

#ifdef __cplusplus
}
#endif
