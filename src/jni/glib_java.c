/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <stddef.h>
#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <jni.h>
#include <jg_jnu.h>
#include "glib_java.h"


/* 
 * HashTable (and a lock) to hold JGStruct objects keyed on the
 * JGStruct->thestruct. Used in get{GBoxed,Struct}Handle and
 * processPending{GBoxed,Struct}.
 */
static GHashTable *struct_hash = NULL;
static GStaticMutex struct_hash_lock = G_STATIC_MUTEX_INIT;

/*
 * Locks and lists for use by processPending* and nativeFinalize*
 * memory management functions.
 */
static GStaticMutex gobject_unrefs_lock = G_STATIC_MUTEX_INIT;
static GSList *gobject_unrefs_list = NULL;
static GStaticMutex gboxed_unrefs_lock = G_STATIC_MUTEX_INIT;
static GSList *gboxed_unrefs_list = NULL;
static GStaticMutex struct_unrefs_lock = G_STATIC_MUTEX_INIT;
static GSList *struct_unrefs_list = NULL;


/*
 * This function tries to translate the name of a glib type
 * into the name of a java class
 */
char * javaobject_from_gtktype(GType argtype) {

    switch(argtype) {
    /*
        case G_TYPE_INVALID:
        case G_TYPE_NONE:
        case G_TYPE_INTERFACE:
    */
        case G_TYPE_CHAR:
        case G_TYPE_UCHAR:
            return "java/lang/Byte";
        case G_TYPE_BOOLEAN:
            return "java/lang/Boolean";
        case G_TYPE_INT:
        case G_TYPE_UINT:
            return "java/lang/Integer";
        case G_TYPE_LONG:
        case G_TYPE_ULONG:
        case G_TYPE_INT64:
        case G_TYPE_UINT64:
            return "java/lang/Long";
        case G_TYPE_ENUM:
            return "org/gnu/glib/Enum";
        case G_TYPE_FLAGS:
            return "org/gnu/glib/Flags";
        case G_TYPE_FLOAT:
            return "java/lang/Float";
        case G_TYPE_DOUBLE:
            return "java/lang/Double";
        case G_TYPE_STRING:
            return "java/lang/String";
        /*
        case G_TYPE_POINTER:
            return "";
        */
        case G_TYPE_BOXED:
            return "org/gnu/glib/Boxed";
        /*
        case G_TYPE_PARAM:
        case G_TYPE_OBJECT:
            return "";
        */
        default:
            return javatype_from_gtktype(argtype);
    }
}

#define JAVATYPE_BUF_SIZE	50
/*
 * This function tries to translate the name of a glib type
 * into the name of a java type
 */
char * javatype_from_gtktype(GType argtype) {

    char *javatype = g_malloc(JAVATYPE_BUF_SIZE);
    const char *tmp;

    tmp = g_type_name(argtype);
    if (strncmp(tmp, "Gtk", 3) == 0) {
        if (strlen(tmp) > 3)
            tmp = &tmp[3];
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "org/gnu/gtk/%s", tmp);
    }
    else if (strncmp(tmp, "Gdk", 3) == 0) {
        if (strlen(tmp) > 3)
            tmp = &tmp[3];
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "org/gnu/gdk/%s", tmp);
    }
    else if (strncmp(tmp, "Gno", 3) == 0) {
        if (strlen(tmp) > 5)
            tmp = &tmp[5];
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "org/gnu/gnome/%s", tmp);
    }
    else if (strncmp(tmp, "Pan", 3) == 0) {
        if (strlen(tmp) > 5)
            tmp = &tmp[5];
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "org/gnu/pango/%s", tmp);
    }
    else if (strncmp(tmp, "Atk", 3) == 0) {
        if (strlen(tmp) > 3)
            tmp = &tmp[3];
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "org/gnu/atk/%s", tmp);
    }
    else if (strncmp(tmp, "Html", 4) == 0) {
        if ( strlen(tmp) > 4)
            tmp = &tmp[4];
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "org/gnu/gtkhtml/HTML%s", tmp);
    }
    else if (strncmp(tmp, "Dom", 3) == 0) {
        if ( strlen(tmp) > 3)
            tmp = &tmp[3];
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "org/gnu/gtkhtml/dom/Dom%s", tmp);
    }
    else
        g_snprintf(javatype, JAVATYPE_BUF_SIZE, "%s", tmp);

    return javatype;
}

void* getPointerFromJavaGObject(JNIEnv* env, jobject gobject)
{
    static jmethodID method = NULL;
    jclass goclass;

    goclass = (*env)->FindClass(env, "org/gnu/glib/GObject");
    if (method == NULL) {
	    method = (*env)->GetMethodID(env, goclass, 
                                 "getHandle", "()Lorg/gnu/glib/Handle;");
		if (method == NULL)
			return NULL;
    }
    jobject handle = (*env)->CallObjectMethod(env, gobject, method);

    return getPointerFromHandle(env, handle);
}

jobjectArray getGObjectHandlesFromGSListAndRef(JNIEnv* env, GSList* list)
{
	return getHandlesFromGSList(env, list, (GetHandleFunc) getGObjectHandleAndRef);
}

jobjectArray getGObjectHandlesFromGListAndRef(JNIEnv* env, GList* list)
{
	return getHandlesFromGList(env, list, (GetHandleFunc) getGObjectHandleAndRef);
}

jobjectArray getGObjectHandlesFromPointersAndRef(JNIEnv* env, void** pointer,
		int numPtrs, GetHandleFunc hndlFunc)
{
	return getHandlesFromPointers(env, pointer, numPtrs,
		(GetHandleFunc) getGObjectHandleAndRef);	
}

jobjectArray getGObjectHandlesFromGList(JNIEnv* env, GList* list)
{
	return getHandlesFromGList(env, list, (GetHandleFunc) getGObjectHandle);
}

jobjectArray getGObjectHandlesFromGSList(JNIEnv* env, GSList* list)
{
	return getHandlesFromGSList(env, list, (GetHandleFunc) getGObjectHandle);
}

jobjectArray getGObjectHandlesFromPointers(JNIEnv* env, void** pointer, int numPtrs)
{
	return getHandlesFromPointers(env, pointer, numPtrs, (GetHandleFunc) getGObjectHandle);
}

jobjectArray getGBoxedHandlesFromPointers(JNIEnv* env, 
                                          void** pointer, int numPtrs,
                                          GetHandleFunc hndlFunc)
{
    return getHandlesFromPointers(env, pointer, numPtrs, hndlFunc);
}

jobjectArray getGBoxedHandlesFromGList(JNIEnv* env, GList *list, GetHandleFunc
		handleFunc)	{
	return getHandlesFromGList(env, list, handleFunc);			
}
		
jobjectArray getGBoxedHandlesFromGSList(JNIEnv* env, GSList* list,
		GetHandleFunc hndlFunc)
{
	return getHandlesFromGSList(env, list, hndlFunc);
}

jobjectArray getStructHandlesFromGList(JNIEnv* env, GList* list,
		GetHandleFunc hndlFunc)
{
	return getHandlesFromGList(env, list, hndlFunc);
}
		
jobjectArray getStructHandlesFromGSList(JNIEnv* env, GSList* list,
		GetHandleFunc hndlFunc)
{
	return getHandlesFromGSList(env, list, hndlFunc);
}
		
jobjectArray getStructHandlesFromPointers(JNIEnv* env, void** pointer,
		int numPtrs, GetHandleFunc hndlFunc)
{
	return getHandlesFromPointers(env, pointer, numPtrs, hndlFunc);
}

/*
 * Memory management functions.
 * @since v2.8
 */

void initMemoryManagement() {
	if (struct_hash == NULL)	{
    	struct_hash = g_hash_table_new( NULL, NULL );
	}
}

JGRef* getData(GObject* object)
{
    const gchar *key;
    key = "JGObject";
    return (JGRef*) g_object_get_data(object, key);
}

JGRef* setData(JNIEnv *env, GObject *object, jobject data)
{
    JGRef *ref;
    gchar *key;

    key = "JGObject";
    ref = g_new( JGRef, 1 );
    ref->env = env;
    ref->object = (* env)->NewGlobalRef(env, data);
    g_object_set_data(object, key, ref) ;

    return ref;
}

void toggleNotify(gpointer data, GObject *object, gboolean is_last_ref)
{	
    JGRef *ref;
    jobject tempObject;
    ref = (JGRef *) data;
	
    if (is_last_ref) {	
        tempObject = (* ref->env)->NewWeakGlobalRef(ref->env, ref->object);
        (* ref->env)->DeleteGlobalRef(ref->env, ref->object);
        ref->object = tempObject;
    } else {
        tempObject = (* ref->env)->NewGlobalRef(ref->env, ref->object);
        (* ref->env)->DeleteWeakGlobalRef(ref->env, ref->object);
        ref->object = tempObject; 
    }
}

void initGObject(GObject *object, JGRef *ref)
{
   	g_object_add_toggle_ref (object, toggleNotify, ref);
   	g_object_unref(object);
}

jobject getGObjectHandle(JNIEnv* env, GObject* object)
{
    jobject handle;
    JGRef *ref;

	if (object == NULL)
		return NULL;
	
    ref = getData(object);

    if (ref == NULL) {
        handle = getHandleFromPointer(env, object);
        ref = setData(env, object, handle);
        initGObject(object, ref);
        return handle;
    }

    return ref->object;
}

jobject getGObjectHandleAndRef(JNIEnv* env, GObject* object)
{
	jobject handle;
    JGRef *ref;

	if (object == NULL)
		return NULL;
		
    ref = getData(object);

    if (ref == NULL) {
        handle = getHandleFromPointer(env, object);
        ref = setData(env, object, handle);
        g_object_add_toggle_ref (object, toggleNotify, ref);
        return handle;
    }

    return ref->object;
}

jobject getPersistentGObjectHandle(JNIEnv* env, GObject* object)
{
	JGRef *ref;
	jobject handle;
	
	if (object == NULL)
		return NULL;
	
	ref = getData(object);
	
	if (ref == NULL) {
		handle = getHandleFromPointer(env, object);
		setData(env, object, handle);
		return handle;
	}
	return ref->object;	
}

jobject getGBoxedHandle(JNIEnv* env, gpointer box, GType type, 
                        GBoxedCopyFunc copy, GBoxedFreeFunc free) {
    gpointer b;
    jobject handle;
    JGStruct *jgs;
    
    if (box == NULL)
    	return NULL;
    
    g_static_mutex_lock(&struct_hash_lock);
    jgs = g_hash_table_lookup(struct_hash, box);
    g_static_mutex_unlock(&struct_hash_lock);
    
    if (jgs != NULL) {
    	return jgs->handle;
    }
    
    if (copy) {
        b = copy( box );
    }
    else {
        b = box;
    }

    jgs = g_new( JGStruct, 1 );
    jgs->thestruct = b;
    jgs->type = type;
    jgs->free = free;
    handle = getHandleFromPointer(env, b);
    jgs->handle = (*env)->NewWeakGlobalRef(env, handle);

    g_static_mutex_lock( &struct_hash_lock );
    g_hash_table_insert( struct_hash, b, jgs );
    g_static_mutex_unlock( &struct_hash_lock );

    return handle;
}

jobject getStructHandle(JNIEnv* env, gpointer thestruct,
                        JGCopyFunc copy, JGFreeFunc free) {
    return getGBoxedHandle( env, thestruct, -1, 
                     (GBoxedCopyFunc)copy, (GBoxedFreeFunc)free );
}

void updateStructHandle(JNIEnv* env, jobject handle, gpointer thestruct,
						JGFreeFunc free)	{
    JGStruct *jgs;
 
    if (thestruct == NULL)
    	return;
    
    g_static_mutex_lock(&struct_hash_lock);
    jgs = g_hash_table_lookup(struct_hash, thestruct);
    g_static_mutex_unlock(&struct_hash_lock);
    
    if (jgs != NULL) {
    	return;
    }

    jgs = g_new( JGStruct, 1 );
    jgs->thestruct = thestruct;
    jgs->free = free;
    handle = updateHandle(env, handle, thestruct);
    jgs->handle = (*env)->NewWeakGlobalRef(env, handle);

    g_static_mutex_lock( &struct_hash_lock );
    g_hash_table_insert( struct_hash, thestruct, jgs );
    g_static_mutex_unlock( &struct_hash_lock );						
} 

guint processPendingGObject()
{
    GSList *it;
    GObject *object;
    JGRef *ref;

    g_static_mutex_lock (&gobject_unrefs_lock);

    for(it = gobject_unrefs_list; it != NULL; it = it->next) {
        object = it->data;
        
        if (object == NULL)	{
        	g_critical("NULL pointer in GObject finalization queue.");
        	return 0;	
        }
        
        ref = getData(object);
       	
       	g_object_remove_toggle_ref(object, toggleNotify, ref);
        
        g_free(ref);
    }
    g_slist_free(gobject_unrefs_list);
    gobject_unrefs_list = NULL;

    g_static_mutex_unlock (&gobject_unrefs_lock);
    return 0;
}

guint processPendingGBoxed() {
    g_static_mutex_lock( &gboxed_unrefs_lock );
    g_static_mutex_lock( &struct_hash_lock );

    GSList *it;
    for(it = gboxed_unrefs_list; it != NULL; it = it->next)
    {
        gpointer *b = it->data;
    	JGStruct *ref = g_hash_table_lookup( struct_hash, b );
        if ( ref ) {
            g_hash_table_remove( struct_hash, b );
            if ( ref->free ) {
                ref->free( b );
            } else if ( ref->type != -1 && G_TYPE_IS_BOXED( ref->type ) ) {
                g_boxed_free( ref->type, b );
            } else {
                // Don't free cause we don't know how or don't want to.
            }
            g_free( ref );
        } else {
            g_warning( "  UNKNOWN in hash: %p\n", b );
        }
    }

    g_slist_free(gboxed_unrefs_list);
    gboxed_unrefs_list = NULL;

    g_static_mutex_unlock( &struct_hash_lock );
    g_static_mutex_unlock( &gboxed_unrefs_lock );
    return 0;
}

guint processPendingStruct() {
    g_static_mutex_lock( &struct_unrefs_lock );
    g_static_mutex_lock( &struct_hash_lock );

    GSList *it;
    for(it = struct_unrefs_list; it != NULL; it = it->next)
    {
        gpointer *b = it->data;
    	JGStruct *ref = g_hash_table_lookup( struct_hash, b );
        if ( ref ) {
            g_hash_table_remove( struct_hash, b );
            if ( ref->free ) {
                ref->free( b );
            } else if ( G_TYPE_IS_BOXED( ref->type ) ) {
                g_boxed_free( ref->type, b );
            } else {
                // Don't free cause we don't know how or don't want to.
            }
            g_free( ref );
        } else {
            g_warning( "  UNKNOWN in hash: %p\n", b );
        }
    }

    g_slist_free(struct_unrefs_list);
    struct_unrefs_list = NULL;

    g_static_mutex_unlock( &struct_hash_lock );
    g_static_mutex_unlock( &struct_unrefs_lock );
    return 0;
}

void nativeFinalizeGObject( JNIEnv* env, jobject handle ) {
    GObject *object = (GObject *) getPointerFromHandle(env, handle);

    g_static_mutex_lock (&gobject_unrefs_lock);
    gobject_unrefs_list = g_slist_append(gobject_unrefs_list, object);
    g_static_mutex_unlock (&gobject_unrefs_lock);

    g_idle_add((GSourceFunc) processPendingGObject, NULL);
}

void nativeFinalizeGBoxed( JNIEnv* env, jobject handle ) {

    gpointer b = getPointerFromHandle( env, handle );

    g_static_mutex_lock( &gboxed_unrefs_lock );
    gboxed_unrefs_list = g_slist_append( gboxed_unrefs_list, b );
    g_static_mutex_unlock( &gboxed_unrefs_lock );

    g_idle_add((GSourceFunc)processPendingGBoxed, NULL );
}

void nativeFinalizeStruct( JNIEnv* env, jobject handle ) {

    gpointer b = getPointerFromHandle( env, handle );

    g_static_mutex_lock( &struct_unrefs_lock );
    struct_unrefs_list = g_slist_append( struct_unrefs_list, b );
    g_static_mutex_unlock( &struct_unrefs_lock );

    g_idle_add((GSourceFunc)processPendingStruct, NULL );
}

GType* getGTypesFromJArray(JNIEnv* env, jint size, jintArray typesArray,
		jint** returnPointerArray)
{
	GType *types_g;
	int i;
	jint* tempTypes;

	types_g = g_new(GType, size);
	tempTypes = (*env)->GetIntArrayElements(env, typesArray, NULL);
	for (i = 0; i < size; ++i)
		types_g[i] = (GType) tempTypes[i];

	if (returnPointerArray != NULL)
		*returnPointerArray = tempTypes;

	return types_g;
}
