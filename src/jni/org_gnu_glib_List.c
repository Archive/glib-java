/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <glib.h>
#include "jg_jnu.h"

#include "org_gnu_glib_List.h"
#ifdef __cplusplus
extern "C" 
{
#endif


/*
 * NOTE: This class is deprecated.  Use the standard Java collection
 * classes or arrays instead.
 */


/*
 * Class:     org.gnu.glib.List
 * Method:    getData
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_getData 
  (JNIEnv *env, jclass cls, jobject obj) 
{
    GList *obj_g;
	
    obj_g = (GList *)getPointerFromHandle(env, obj);
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, obj_g->data);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_append
 * Signature: (Lorg/gnu/glib/Handle;I)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1append
  (JNIEnv *env, jclass cls, jobject list, jlong data) 
{
    GList *list_g;
    
    list_g = (GList *)getPointerFromHandle(env, list);

    list_g = g_list_append(list_g, (gpointer)data);
	
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_prepend
 * Signature: (Lorg/gnu/glib/Handle;I)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1prepend
  (JNIEnv *env, jclass cls, jobject list, jlong data) 
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);

    list_g = g_list_prepend (list_g, (gpointer)data);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_insert
 * Signature: (Lorg/gnu/glib/Handle;II)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1insert
(JNIEnv *env, jclass cls, jobject list, jlong data, jint position) 
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);

    list_g = g_list_insert (list_g, (gpointer)data, (gint32)position);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_remove
 * Signature: (Lorg/gnu/glib/Handle;I)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1remove 
  (JNIEnv *env, jclass cls, jobject list, jlong data) 
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    list_g = g_list_remove (list_g, (gpointer)data);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_remove_all
 * Signature: (Lorg/gnu/glib/Handle;I)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1remove_1all 
  (JNIEnv *env, jclass cls, jobject list, jlong data) 
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    list_g = g_list_remove_all (list_g, (gpointer)data);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_free
 * Signature: (Lorg/gnu/glib/Handle;)V
 */
JNIEXPORT void JNICALL Java_org_gnu_glib_List_g_1list_1free 
  (JNIEnv *env, jclass cls, jobject list)
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    g_list_free (list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_alloc
 * Signature: ()Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1alloc 
  (JNIEnv *env, jclass cls) 
{
    GList *list;
	
    list = g_list_alloc();
	
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_length
 * Signature: (Lorg/gnu/glib/Handle;)I
 */
JNIEXPORT jint JNICALL Java_org_gnu_glib_List_g_1list_1length 
  (JNIEnv *env, jclass cls, jobject list) 
{
    GList *list_g;
    gint len;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    len = g_list_length(list_g);
    
    return len;
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_first
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1first 
  (JNIEnv *env, jclass cls, jobject list) 
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    list_g = g_list_first (list_g);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_last
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1last 
  (JNIEnv *env, jclass cls, jobject list)
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    list_g = g_list_last (list_g);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_previous
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1previous 
  (JNIEnv *env, jclass cls, jobject list) 
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    list_g = g_list_previous (list_g);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

/*
 * Class:     org.gnu.glib.List
 * Method:    g_list_next
 * Signature: (Lorg/gnu/glib/Handle;)Lorg/gnu/glib/Handle;
 */
JNIEXPORT jobject JNICALL Java_org_gnu_glib_List_g_1list_1next 
  (JNIEnv *env, jclass cls, jobject list)
{
    GList *list_g;
	
    list_g = (GList *)getPointerFromHandle(env, list);
    
    list_g = g_list_next (list_g);
    
    // MEM_MGT_NOTE: Class is deprecated so we can ignore.
    return getHandleFromPointer(env, list_g);
}

#ifdef __cplusplus
}

#endif
