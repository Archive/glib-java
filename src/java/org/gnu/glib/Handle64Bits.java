/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.glib;

/**
 * Holds pointer to native peer on 64 bit platforms.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class does not have an equivalent in java-gnome 4.0.
 */
public class Handle64Bits implements Handle {

    long pointer;

    private Struct proxiedObject;

    public boolean isNull() {
        return pointer == 0;
    }

    public boolean equals(Handle other) {
        if (other instanceof Handle64Bits)
            return pointer == ((Handle64Bits) other).pointer;
        return false;
    }

    public String toString() {
        return "Handle64Bits - pointer = " + String.valueOf(pointer);
    }

    public Struct getProxiedObject() {
        return proxiedObject;
    }

    public void setProxiedObject(Struct object) {
        this.proxiedObject = object;
    }
}
