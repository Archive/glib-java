/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;


/**
 * Value is a polymorphic type that can hold values of any other type. This is
 * used internally and should not be exposed outside of the library.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class does have an equivalent in java-gnome 4.0,
 *             but it serves a rather different purpose.
 *             See <code>org.gnome.glib.Value</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Value extends MemStruct {

    private Object javaval = null;

    /**
     * Construct a new Value from a given org.gnu.glib.Type.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Value(Type type) {
        super(Value.g_value_init(type.getTypeHandle()));
    }

    /**
     * Construct a new Value object using a handle to a native object.
     * 
     * @param aHandle
     *            The handle
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Value(Handle aHandle) {
        super(aHandle);
    }

    /**
     * Create a copy of this Value object.
     * 
     * @return An object of type Value that contains the same data as the
     *         current object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Value copy() {
        Handle dest = Value.g_value_copy(getHandle());
        return new Value(dest);
    }

    /**
     * Used internally by Java-Gnome to set a string value
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setString(String value) {
        if (value == null)
            value = "";
        g_value_set_string(getHandle(), value);
    }

    /**
     * Used internally by Java-Gnome
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getString() {
        return g_value_get_string(getHandle());
    }

    /**
     * Used internally by Java-Gnome to set a boolean value
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBoolean(boolean value) {
        g_value_set_boolean(getHandle(), value);
    }

    /**
     * Used internally by Java-Gnome
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getBoolean() {
        return g_value_get_boolean(getHandle());
    }

    /**
     * Used internally by Java-Gnome to set an integer value
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setInteger(int value) {
        g_value_set_int(getHandle(), value);
    }

    /**
     * Used internally by Java-Gnome
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getInt() {
        return g_value_get_int(getHandle());
    }

    /**
     * Used internally by Java-Gnome to set a long value
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLong(long value) {
        g_value_set_long(getHandle(), value);
    }

    /**
     * Used internally by Java-Gnome
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public long getLong() {
        return g_value_get_long(getHandle());
    }

    /**
     * Used internally by Java-Gnome to set a float value
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFloat(float value) {
        g_value_set_float(getHandle(), value);
    }

    /**
     * Used internally by Java-Gnome
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getFloat() {
        return g_value_get_float(getHandle());
    }

    /**
     * Used internally by Java-Gnome to set a double value
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDouble(double value) {
        g_value_set_double(getHandle(), value);
    }

    /**
     * Used internally by Java-Gnome
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getDouble() {
        return g_value_get_double(getHandle());
    }

    public Boxed getBoxed() {
        Handle hndl = g_value_get_boxed(getHandle());
        if (hndl != null) {
            Boxed ret = Boxed.getBoxedFromHandle(hndl);
            if (ret == null) {
                return new Boxed(hndl);
            } else {
                return ret;
            }
        }
        return null;
    }

    public void setBoxed(Boxed value) {
        g_value_set_boxed(getHandle(), value.getHandle());
    }

    /**
     * Set the data held by this Value object with the given Object. Objects
     * will be set directly in the C GValue structure. Other "non-Glib" objects
     * will also be kept locally in the Java object.
     * <p>
     * <b>NOTE</b>: You probably don't want to use this directly. Prefer using
     * a convenience method such as: {@link GObject#setJavaObjectProperty}.
     * 
     * @param obj
     *            The object to set as the data value for this Value instance.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setJavaObject(Object obj) {
        if (obj instanceof Struct) {
            g_value_set_java_object(getHandle(), ((Struct) obj).getHandle());
        } else {
            javaval = obj;
            g_value_set_java_object(getHandle(), obj);
        }
    }

    /**
     * Get the data held by this Value object.
     * <p>
     * <b>NOTE</b>: You probably don't want to use this directly. Prefer using
     * a convenience method such as: {@link GObject#getJavaObjectProperty}.
     * 
     * @return The data value held by this Value instance. If the data is held
     *         in the C GValue structure, the returned object is an instance of
     *         {@link Handle}. If the data is held locally in the Java object,
     *         that is returned, otherwise <tt>null</tt> is returned.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Object getJavaObject() {
        Object obj = g_value_get_java_object(getHandle());
        if (obj == null) {
            return javaval;
        } else if (obj instanceof Handle) {
            return ((Handle) obj).getProxiedObject();
        } else {
            return obj;
        }
        /*
         * Object obj = g_value_get_java_object( getHandle() ); if (obj
         * instanceof Handle) { Handle hndl = (Handle) obj; if ( hndl == null ||
         * hndl.isNull() ) { return javaval; } else { return hndl; } } return
         * obj;
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
         */
    }

    native static final protected Handle g_value_init(int type);

    native static final protected int g_value_type(Handle value);

    native static final protected Handle g_value_copy(Handle srcValue);

    native static final protected Handle g_value_reset(Handle value);

    native static final protected void g_value_unset(Handle value);

    native static final protected void g_value_set_char(Handle value, byte vChar);

    native static final protected byte g_value_get_char(Handle value);

    native static final protected void g_value_set_boolean(Handle value,
            boolean vBoolean);

    native static final protected boolean g_value_get_boolean(Handle value);

    native static final protected void g_value_set_int(Handle value, int vInt);

    native static final protected int g_value_get_int(Handle value);

    native static final protected void g_value_set_long(Handle value, long vLong);

    native static final protected long g_value_get_long(Handle value);

    native static final protected void g_value_set_float(Handle value,
            double vFloat);

    native static final protected double g_value_get_float(Handle value);

    native static final protected void g_value_set_double(Handle value,
            double vDouble);

    native static final protected double g_value_get_double(Handle value);

    native static final protected void g_value_set_string(Handle value,
            String vString);

    native static final protected String g_value_get_string(Handle value);

    native static final protected void g_value_set_boxed(Handle value,
            Handle box);

    native static final protected Handle g_value_get_boxed(Handle value);

    native static final protected void g_value_set_java_object(Handle value,
            Object obj);

    native static final protected Object g_value_get_java_object(Handle value);

    native static final protected void g_value_set_pointer(Handle value,
            Handle ptr);

    native static final protected Handle g_value_get_pointer(Handle value);
    /**
     * Test if the value contains an {@link Enum}-derived class.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    // native protected boolean g_value_is_enum(Handle value);
    /**
     * Gets the
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    // native protected String g_value_get_type_name(Handle value);
}
