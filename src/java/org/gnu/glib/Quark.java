/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 * A Quark is an association between a String and an integer identifier. Given
 * either the String or the Quark it is possible to retrieve the other. This
 * object is used primarily inside of the bindings. The external interface
 * should use a standard Java type like Property.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Quark</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Quark {
    /*
     * Implementation note: Quarks are defined in C as:
     * 
     * typedef guint32 GQuark
     * 
     * So they are neither structs, GBoxed or GObjects and thus do not inherit
     * from Struct.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */

    private int handle;

    /**
     * Create a Quark object.
     * 
     * @param string
     *            The string value associated with this Quark.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Quark(String string) {
        this(Quark.g_quark_from_string(string));
    }

    /**
     * This is the method that allows one to construct a Quark once a native
     * peer has been provided.
     * 
     * @param handle
     *            The native peer that was returned from a call to the native
     *            libraries.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Quark(int handle) {
        this.handle = handle;
    }

    /**
     * Retrieve the string value associated with this Quark.
     * 
     * @return The string value
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final String getString() {
        return Quark.g_quark_to_string(handle);
    }

    /**
     * Find a Quark that is associated with the given string. If one is not
     * found a null value is returned.
     * 
     * @param string
     *            The string value to use for the search
     * @return The Quark value associated with the string or null if one is not
     *         found.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Quark findQuark(String string) {
        int hndl = Quark.g_quark_try_string(string);
        if (hndl == 0)
            return null;
        else
            return new Quark(hndl);
    }

    public int getHandle() {
        return handle;
    }

    native static final protected int g_quark_from_string(String str);

    native static final protected int g_quark_try_string(String str);

    native static final protected String g_quark_to_string(int quark);
}
