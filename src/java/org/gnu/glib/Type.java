/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 * This class represents the GLib Runtime type identification and management
 * system. It provides the facilities for registering and managing all
 * fundamental data types. It should be for internal use only and not be exposed
 * outside of the library.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class does not have an equivalent in java-gnome 4.0,
 */
public class Type {

    protected static boolean isInitialized = false;

    private int handle;

    /**
     * Construct a new Type from a integer that represents the type.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Type(int handle) {
        this.handle = handle;
    }

    /**
     * Construct a Type by its' name.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Type(String name) {
        if (!Type.isInitialized) {
            Type.g_type_init();
            Type.isInitialized = true;
        }
        handle = Type.g_type_from_name(name);
    }

    /**
     * Returns the unique identifyer used to identify a type in the native
     * libraries.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getTypeHandle() {
        return handle;
    }

    /**
     * Compares a Type with the current object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean typeEquals(Type aType) {
        return handle == aType.getTypeHandle();
    }

    /**
     * Determine if the <tt>Type</tt> parameter is a derivable type, checking
     * whether this <tt>Type</tt> object is a descendant of the <tt>Type</tt>
     * parameter. If the <tt>Type</tt> parameter is an interface, check
     * whether this <tt>Type</tt> object conforms to it.
     * 
     * @param aType
     *            the Type to compare this Type with.
     * @return <code>true</code> if this type is equal or descendant of
     *         <code>aType</code>, <code>false</code> otherwise.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean typeIsA(Type aType) {
        return g_type_is_a(getTypeHandle(), aType.getTypeHandle());
    }

    /**
     * Return a Type instance initialized as INVALID.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type INVALID() {
        int hndl = Type.get_INVALID();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as NONE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type NONE() {
        int hndl = Type.get_NONE();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as INTERFACE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type INTERFACE() {
        int hndl = Type.get_INTERFACE();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as CHAR.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type CHAR() {
        int hndl = Type.get_CHAR();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as BOOLEAN.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type BOOLEAN() {
        int hndl = Type.get_BOOLEAN();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as INT.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type INT() {
        int hndl = Type.get_INT();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as LONG.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type LONG() {
        int hndl = Type.get_LONG();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as FLAGS.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type FLAGS() {
        int hndl = Type.get_FLAGS();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as FLOAT.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type FLOAT() {
        int hndl = Type.get_FLOAT();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as DOUBLE.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type DOUBLE() {
        int hndl = Type.get_DOUBLE();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as STRING.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type STRING() {
        int hndl = Type.get_STRING();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as BOXED.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type BOXED() {
        int hndl = Type.get_BOXED();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as PARAM.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type PARAM() {
        int hndl = Type.get_PARAM();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as OBJECT.
     * 
     * @see #JAVA_OBJECT
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type OBJECT() {
        int hndl = Type.get_OBJECT();
        return new Type(hndl);
    }

    /**
     * Return a Type instance initialized as JAVA_OBJECT.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static final Type JAVA_OBJECT() {
        int hndl = Type.get_JAVA_OBJECT();
        return new Type(hndl);
    }

    /***************************************************************************
     * NATIVE METHODS - represent macros in the libs
     **************************************************************************/

    native static final protected int get_JAVA_OBJECT();

    native static final protected int get_POINTER();

    native static final protected int get_INVALID();

    native static final protected int get_NONE();

    native static final protected int get_INTERFACE();

    native static final protected int get_CHAR();

    native static final protected int get_BOOLEAN();

    native static final protected int get_INT();

    native static final protected int get_LONG();

    native static final protected int get_FLAGS();

    native static final protected int get_FLOAT();

    native static final protected int get_DOUBLE();

    native static final protected int get_STRING();

    native static final protected int get_BOXED();

    native static final protected int get_PARAM();

    native static final protected int get_OBJECT();

    /***************************************************************************
     * BEGINNING OF JNI CODE
     **************************************************************************/
    native static final protected void g_type_init();

    native static final protected String g_type_name(int type);

    native static final protected int g_type_qname(int type);

    native static final protected int g_type_from_name(String name);

    native static final protected int g_type_parent(int type);

    native static final protected int g_type_depth(int type);

    native static final protected boolean g_type_is_a(int type, int type_is_a);
    /***************************************************************************
     * END OF JNI CODE
     **************************************************************************/
}
