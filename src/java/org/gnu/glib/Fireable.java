/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 * Defines the interface for a <code>fire</code> method, which is used by
 * {@link Timer} to signal that its interval has elapsed.
 * 
 * @see org.gnu.glib.Timer
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Fireable</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface Fireable {
    /**
     * A generic signal callback, which returns <code>true</code> if the
     * subsystem that called it should continue firing (that is, continue
     * invoking this callback).
     * 
     * @return <code>true</code> if the caller should continue calling this
     *         method, or <code>false</code> if this should be the last
     *         invocation.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    boolean fire();
}
