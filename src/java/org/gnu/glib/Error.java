/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;


/**
 * The class represents an internal representation of an Error from the GTK and
 * GNOME libraries. It should never be returned to the application layer. The
 * Java-GNOME API should throw an exception that contains the relevant
 * information contained within this object. This resource must be freed when
 * you are finished with it via the free() method.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Error</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Error extends MemStruct {
    /**
     * Construct a new Error object. This object is used to retrieve errors from
     * the native layer. The java-gnome library will rarely return an object of
     * this type. It will convert it into an exception that will be thrown to
     * the application layer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Error(Quark domain, int code, String message) {
        super(Error.g_error_new_literal(domain.getHandle(), code, message));
    }

    public Error(Handle handle) {
        super(handle);
    }

    /**
     * Return the domain for this error.
     * 
     * @return The Quark object that represents the domain for this error.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Quark getDomain() {
        int qhndl = Error.getDomain(getHandle());
        return new Quark(qhndl);
    }

    /**
     * Return the error code associated with this error.
     * 
     * @return The error code for this error.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getErrorCode() {
        return Error.getCode(getHandle());
    }

    /**
     * Return the error message associated with this error.
     * 
     * @return The error message for this error.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getErrorMessage() {
        return Error.getMessage(getHandle());
    }

    native static final protected int getDomain(Handle obj);

    native static final protected int getCode(Handle obj);

    native static final protected String getMessage(Handle obj);

    native static final protected Handle g_error_new_literal(int domain,
            int code, String message);
}
