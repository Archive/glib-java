/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 * Timer: an object that executes a <code>Fireable</code> target object's
 * <code>fire</code> method at a specified interval.
 * <p>
 * For example, here's how an application clock might be implemented, where the
 * application passes in an {@link org.gnu.gtk.Label} object as its pane:
 * 
 * <pre>
 * private final Label clockPane = (Label) glade.getWidget(&quot;clockLabel&quot;);
 * private Timer clock = new Timer(1000, // one second
 *         new Fireable() {
 *             public boolean fire() {
 *                 String dateStr = DateFormat.getDateInstance()
 *                         .format(new Date());
 *                 clockPane.setText(dateStr);
 *                 return true; // continue firing
 *             }
 *         });
 * clock.start();
 * </pre>
 * 
 * <p>
 * Note: a Timer generates events on the application's GUI event queue. It
 * therefore is not accurate for short time periods. It also should only be used
 * to directly fire short/fast methods. Longer methods need to be executed in a
 * separate thread.
 * 
 * @see org.gnu.glib.Fireable
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Timer</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public final class Timer {

    private int interval;

    private Fireable target;

    private int handle = 0;

    /**
     * Create a new Timer object.
     * 
     * @param interval
     *            the time period between <code>fire</code> method executions,
     *            in thousandths of a second.
     * @param target
     *            the object whose fire() method gets called after the specified
     *            time period elapses.
     * @throws IllegalArgumentException
     *             if less than one.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Timer(int interval, Fireable target) {
        if (interval <= 0)
            throw new IllegalArgumentException("invalid time: " + interval);
        this.interval = interval;
        this.target = target;
    }

    /**
     * Returns the interval associated with this Timer.
     * 
     * @return the time period between <code>fire</code> method executions, in
     *         thousandths of a second.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final int getInterval() {
        return interval;
    }

    /**
     * Set the interval associated with this Timer.
     * 
     * @param interval
     *            the time period between <code>fire</code> method executions,
     *            in thousandths of a second.
     * @throws IllegalArgumentException
     *             if less than one.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final void setInterval(int interval) {
        if (interval <= 0)
            throw new IllegalArgumentException("invalid time: " + interval);
        this.interval = interval;
    }

    /**
     * Start this timer object; that is, begin executing its fire method at its
     * specified interval.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized void start() {
        handle = start_timer(interval);
    }

    /**
     * Returns whether this timer is running.
     * 
     * @return true if this timer is currently running.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized boolean isRunning() {
        return handle != 0;
    }

    /**
     * Stop this timer object; that is, stop executing its fire method at its
     * specified interval. This method does not need to be called if the
     * <code>fire</code> method returned <code>false</code>.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized void stop() {
        stop_timer(handle);
    }

    /**
     * Do not call this method; it's only purpose is to ensure that the timer is
     * stopped before it is GC'd.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected final void finalize() throws Throwable {
        stop();
    }

    private native int start_timer(int interval);

    private native void stop_timer(int handle);

    private static native void initIDs();

    static {
        initIDs();
    }
}
