/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;


/**
 * 
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.ParmFlags</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class ParmFlags extends Flags {

    static final private int _READABLE = 1 << 0;

    static final public ParmFlags READABLE = new ParmFlags(_READABLE);

    static final private int _WRITABLE = 1 << 1;

    static final public ParmFlags WRITABLE = new ParmFlags(_WRITABLE);

    static final private int _CONSTRUCT = 1 << 2;

    static final public ParmFlags CONSTRUCT = new ParmFlags(_CONSTRUCT);

    static final private int _CONSTRUCT_ONLY = 1 << 3;

    static final public ParmFlags CONSTRUCT_ONLY = new ParmFlags(
            _CONSTRUCT_ONLY);

    static final private int _LAX_VALIDATION = 1 << 4;

    static final public ParmFlags LAX_VALIDATION = new ParmFlags(
            _LAX_VALIDATION);

    static final private int _PRIVATE = 1 << 5;

    static final public ParmFlags PRIVATE = new ParmFlags(_PRIVATE);

    static final private ParmFlags[] theInterned = new ParmFlags[] {
            new ParmFlags(0), new ParmFlags(1), READABLE, new ParmFlags(3),
            WRITABLE, new ParmFlags(5), new ParmFlags(6), new ParmFlags(7),
            CONSTRUCT, new ParmFlags(9), new ParmFlags(10), new ParmFlags(11),
            new ParmFlags(12), new ParmFlags(13), new ParmFlags(14),
            new ParmFlags(15), CONSTRUCT_ONLY, new ParmFlags(17),
            new ParmFlags(18), new ParmFlags(19), new ParmFlags(20),
            new ParmFlags(21), new ParmFlags(22), new ParmFlags(23),
            new ParmFlags(24), new ParmFlags(25), new ParmFlags(26),
            new ParmFlags(27), new ParmFlags(28), new ParmFlags(29),
            new ParmFlags(30), new ParmFlags(31), LAX_VALIDATION,
            new ParmFlags(33), new ParmFlags(34), new ParmFlags(35),
            new ParmFlags(36), new ParmFlags(37), new ParmFlags(38),
            new ParmFlags(39), new ParmFlags(40), new ParmFlags(41),
            new ParmFlags(42), new ParmFlags(43), new ParmFlags(44),
            new ParmFlags(45), new ParmFlags(46), new ParmFlags(47),
            new ParmFlags(48), new ParmFlags(49), new ParmFlags(50),
            new ParmFlags(51), new ParmFlags(52), new ParmFlags(53),
            new ParmFlags(54), new ParmFlags(55), new ParmFlags(56),
            new ParmFlags(57), new ParmFlags(58), new ParmFlags(59),
            new ParmFlags(60), new ParmFlags(61), new ParmFlags(62),
            new ParmFlags(63), PRIVATE, new ParmFlags(65), new ParmFlags(66),
            new ParmFlags(67), new ParmFlags(68), new ParmFlags(69),
            new ParmFlags(70), new ParmFlags(71), new ParmFlags(72),
            new ParmFlags(73), new ParmFlags(74), new ParmFlags(75),
            new ParmFlags(76), new ParmFlags(77), new ParmFlags(78),
            new ParmFlags(79), new ParmFlags(80), new ParmFlags(81),
            new ParmFlags(82), new ParmFlags(83), new ParmFlags(84),
            new ParmFlags(85), new ParmFlags(86), new ParmFlags(87),
            new ParmFlags(88), new ParmFlags(89), new ParmFlags(90),
            new ParmFlags(91), new ParmFlags(92), new ParmFlags(93),
            new ParmFlags(94), new ParmFlags(95), new ParmFlags(96),
            new ParmFlags(97), new ParmFlags(98), new ParmFlags(99),
            new ParmFlags(100), new ParmFlags(101), new ParmFlags(102),
            new ParmFlags(103), new ParmFlags(104), new ParmFlags(105),
            new ParmFlags(106), new ParmFlags(107), new ParmFlags(108),
            new ParmFlags(109), new ParmFlags(110), new ParmFlags(111),
            new ParmFlags(112), new ParmFlags(113), new ParmFlags(114),
            new ParmFlags(115), new ParmFlags(116), new ParmFlags(117),
            new ParmFlags(118), new ParmFlags(119), new ParmFlags(120),
            new ParmFlags(121), new ParmFlags(122), new ParmFlags(123),
            new ParmFlags(124), new ParmFlags(125), new ParmFlags(126),
            new ParmFlags(127), new ParmFlags(128), new ParmFlags(129),
            new ParmFlags(130), new ParmFlags(131), new ParmFlags(132),
            new ParmFlags(133), new ParmFlags(134), new ParmFlags(135),
            new ParmFlags(136), new ParmFlags(137), new ParmFlags(138),
            new ParmFlags(139), new ParmFlags(140), new ParmFlags(141),
            new ParmFlags(142), new ParmFlags(143), new ParmFlags(144),
            new ParmFlags(145), new ParmFlags(146), new ParmFlags(147),
            new ParmFlags(148), new ParmFlags(149), new ParmFlags(150),
            new ParmFlags(151), new ParmFlags(152), new ParmFlags(153),
            new ParmFlags(154), new ParmFlags(155), new ParmFlags(156),
            new ParmFlags(157), new ParmFlags(158), new ParmFlags(159),
            new ParmFlags(160), new ParmFlags(161), new ParmFlags(162),
            new ParmFlags(163), new ParmFlags(164), new ParmFlags(165),
            new ParmFlags(166), new ParmFlags(167), new ParmFlags(168),
            new ParmFlags(169), new ParmFlags(170), new ParmFlags(171),
            new ParmFlags(172), new ParmFlags(173), new ParmFlags(174),
            new ParmFlags(175), new ParmFlags(176), new ParmFlags(177),
            new ParmFlags(178), new ParmFlags(179), new ParmFlags(180),
            new ParmFlags(181), new ParmFlags(182), new ParmFlags(183),
            new ParmFlags(184), new ParmFlags(185), new ParmFlags(186),
            new ParmFlags(187), new ParmFlags(188), new ParmFlags(189),
            new ParmFlags(190), new ParmFlags(191), new ParmFlags(192),
            new ParmFlags(193), new ParmFlags(194), new ParmFlags(195),
            new ParmFlags(196), new ParmFlags(197), new ParmFlags(198),
            new ParmFlags(199), new ParmFlags(200), new ParmFlags(201),
            new ParmFlags(202), new ParmFlags(203), new ParmFlags(204),
            new ParmFlags(205), new ParmFlags(206), new ParmFlags(207),
            new ParmFlags(208), new ParmFlags(209), new ParmFlags(210),
            new ParmFlags(211), new ParmFlags(212), new ParmFlags(213),
            new ParmFlags(214), new ParmFlags(215), new ParmFlags(216),
            new ParmFlags(217), new ParmFlags(218), new ParmFlags(219),
            new ParmFlags(220), new ParmFlags(221), new ParmFlags(222),
            new ParmFlags(223), new ParmFlags(224), new ParmFlags(225),
            new ParmFlags(226), new ParmFlags(227), new ParmFlags(228),
            new ParmFlags(229), new ParmFlags(230), new ParmFlags(231),
            new ParmFlags(232), new ParmFlags(233), new ParmFlags(234),
            new ParmFlags(235), new ParmFlags(236), new ParmFlags(237),
            new ParmFlags(238), new ParmFlags(239), new ParmFlags(240),
            new ParmFlags(241), new ParmFlags(242), new ParmFlags(243),
            new ParmFlags(244), new ParmFlags(245), new ParmFlags(246),
            new ParmFlags(247), new ParmFlags(248), new ParmFlags(249),
            new ParmFlags(250), new ParmFlags(251), new ParmFlags(252),
            new ParmFlags(253), new ParmFlags(254), new ParmFlags(255) }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private ParmFlags theSacrificialOne = new ParmFlags(0);

    static public ParmFlags intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        ParmFlags already = (ParmFlags) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new ParmFlags(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private ParmFlags(int value) {
        value_ = value;
    }

    public ParmFlags or(ParmFlags other) {
        return intern(value_ | other.value_);
    }

    public ParmFlags and(ParmFlags other) {
        return intern(value_ & other.value_);
    }

    public ParmFlags xor(ParmFlags other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(ParmFlags other) {
        return (value_ & other.value_) == other.value_;
    }

}
