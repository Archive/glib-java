/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;


/**
 * Class for memory management of structs. Should be renamed to Struct once we
 * rename org.javagnome.Struct to org.javagnome.Proxy.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class does not have an equivalent in java-gnome 4.0.
 */
public class MemStruct extends Struct {
    public MemStruct() {
    }

    public MemStruct(Handle handle) {
        super(handle);
    }

    public static MemStruct getMemStructFromHandle(Handle handle) {
        return (MemStruct) handle.getProxiedObject();
    }

    protected void finalize() throws Throwable {
        // log.fine(this.getClass().getName());

        // If an exception is throws in the constructor of a sublcass, then
        // an object with a null handle may be finalized at some point. Don't
        // run nativeFinalize for those objects.
        try {
            if (getHandle() != null) {
                nativeFinalize(getHandle());
            }
        } finally {
            super.finalize();
        }
    }

    static {
        init();
    }

    native static final private void nativeFinalize(Handle handle);

    native static final private void init();
}
