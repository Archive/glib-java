/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.freedesktop.bindings.Constant</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Enum {
    /** holder for the raw enumeration value */
    protected int value_;

    /**
     * This class is only instantiable via subclasses.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected Enum() {
        // nothing to do
    }

    /**
     * Get the raw value of the object.
     * 
     * @return the raw value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final int getValue() {
        return value_;
    }

    /**
     * Get the hash code for this instance. It is the same as its value.
     * 
     * @return the hash code
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final int hashCode() {
        return value_;
    }

    /**
     * Compare this to another object. The comparison is only <code>true</code>
     * when the other object is also a <code>Enum</code> and when the values
     * match.
     * 
     * @param other
     *            the object to compare to
     * @return the result of comparison
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final boolean equals(java.lang.Object other) {
        if (!(other instanceof Enum)) {
            return false;
        }

        Enum otherEnum = (Enum) other;
        return (value_ == otherEnum.value_);
    }
}
