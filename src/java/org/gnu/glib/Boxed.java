/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

// import java.util.logging.Logger;

/**
 * A mechanism to wrap opaque structures registered by the type system.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Boxed</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Boxed extends Struct {

    // private static final Logger log = Logger.getLogger("org.gnu.glib.Boxed");
    /**
     * This class is only instantiable via subclasses.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected Boxed() {
        // nothing to do
    }

    protected Boxed(Handle handle) {
        super(handle);
    }

    /**
     * Gets a <tt>Boxed</tt> instance for the given <tt>Handle</tt>. If no
     * Java object currently exists for the given <tt>Handle</tt>, this
     * method will return <tt>null</tt>. You should then instantiate the
     * required Java class using the class' handle-constructor. For example:
     * 
     * <pre>
     * // Get a Handle from somewhere (typically as a parameter to a method
     * // used as a callback and invoked from the C JNI side).
     * SomeGtkClass finalobj = null;
     * Boxed obj = Boxed.getBoxedFromHandle(handle);
     * if (obj == null) {
     *     finalobj = new SomeGtkClass(handle);
     * } else {
     *     finalobj = (SomeGtkClass) obj;
     * }
     * </pre>
     * 
     * NOTE: This is for internal use only and should never need to be used in
     * application code.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static Boxed getBoxedFromHandle(Handle hndl) {
        return (Boxed) hndl.getProxiedObject();
    }

    /**
     * Check if two objects refer to the same (native) object.
     * 
     * @param other
     *            the reference object with which to compare.
     * @return true if both objects refer to the same object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean equals(Object other) {
        return other instanceof Boxed
                && getHandle().equals(((Boxed) other).getHandle());
    }

    /**
     * Returns a hash code value for the object. This allows for using Boxed
     * objects as keys in hashmaps.
     * 
     * @return a hash code value for the object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int hashCode() {
        return getHandle().hashCode();
    }

    protected void finalize() throws Throwable {
        // log.fine(this.getClass().getName());

        // If an exception is throws in the constructor of a sublcass, then
        // an object with a null handle may be finalized at some point. Don't
        // run nativeFinalize for those objects.
        try {
            if (getHandle() != null) {
                nativeFinalize(getHandle());
            }
        } finally {
            super.finalize();
        }
    }

    static {
        init();
    }

    native static final private void nativeFinalize(Handle handle);

    native static final private void init();
}
