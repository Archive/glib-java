/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 * Generic Exception in Java-GNOME. This exception wraps an org.gnu.glib.Error.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             but it will certainly have a better name.
 */
public class JGException extends Exception {

    private static final long serialVersionUID = 3256725087058082105L;

    private Error error;

    public JGException(Error error) {
        super();
        this.error = error;
    }

    /**
     * @return Returns the error.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Error getError() {
        return error;
    }

    /**
     * @param error
     *            The error to set.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setError(Error error) {
        this.error = error;
    }
}
