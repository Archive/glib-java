/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 * Idle: an object that executes a <code>Fireable</code> target object's
 * <code>fire</code> method at the next available time.  Similar to Swing's
 * invokeLater functionality.
 * <p>
 * For example, here's how an application clock might be implemented, where the
 * application passes in an {@link org.gnu.gtk.Label} object as its pane:
 * 
 * <pre>
 * private Idle later = new Idle(100,
 *         new Fireable() {
 *             public boolean fire() {
 *                 String dateStr = DateFormat.getDateInstance()
 *                         .format(new Date());
 *                 System.out.println("Invoked later!" + dateStr);
 *                 return true; // continue firing
 *             }
 *         });
 * later.start();
 * </pre>
 * 
 * <p>
 * Note: an Idle generates events on the application's GUI event queue. It also
 * should only be used to directly fire short/fast methods. Longer methods need
 * to be executed in a separate thread.
 * 
 * @see org.gnu.glib.Fireable
 * @see org.gnu.glib.Timer
 * @author Dan Williams
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Idle</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public final class Idle {

    private int priority;

    private Fireable target;

    private int sourceID = 0;

    /**
     * Create a new Idle object.
     * 
     * @param priority
     *            The execution priority of the object, from 0 (highest priority)
     *            to Integer.MAX_VALUE (lowest priority).
     * @param target
     *            the object whose fire() method gets called after the specified
     *            time period elapses.
     * @throws IllegalArgumentException
     *             if less than zero.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Idle(int priority, Fireable target) {
        if (priority < 0)
            throw new IllegalArgumentException("invalid priority: " + priority);
        this.priority = priority;
        this.target = target;
    }

    /**
     * Create a new Idle object.
     * 
     * @param target
     *            the object whose fire() method gets called after the specified
     *            time period elapses.
     * @throws IllegalArgumentException
     *             if less than zero.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Idle(Fireable target) {
        this(200, target);  // 200 = G_PRIORITY_DEFAULT_IDLE
    }

    /**
     * Returns the execution priority for this Idle.
     * 
     * @return the priority of this Idle, from 0 (highest priority) to
     * Integer.MAX_VALUE (lowest priority)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized int getPriority() {
        return priority;
    }

    /**
     * Set the execution priority for this Idle.
     * 
     * @param priority
     *            The execution priority of the object, from 0 (highest priority)
     *            to Integer.MAX_VALUE (lowest priority).
     * @throws IllegalArgumentException
     *             if less than zero.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized void setPriority(int priority) {
        if (isRunning()) {
            if (priority < 0)
                throw new IllegalArgumentException("invalid priority: " + priority);
            this.priority = priority;
            set_priority(sourceID, priority);
        }
    }

    /**
     * Start this Idle object; that is, begin executing its fire method at the
     * available execution slot in the mainloop.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized void start() {
        if (!isRunning())
            sourceID = start_idle(priority);
    }

    /**
     * Returns whether this idle is running.
     * 
     * @return true if this idle is currently running.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized boolean isRunning() {
        return sourceID != 0;
    }

    /**
     * Stop this idle object; that is, stop executing its fire method at its
     * specified interval. This method does not need to be called if the
     * <code>fire</code> method returned <code>false</code>.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final synchronized void stop() {
        if (isRunning())
            stop_idle(sourceID);
    }

    /**
     * Do not call this method; it's only purpose is to ensure that the idle is
     * stopped before it is GC'd.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected final void finalize() throws Throwable {
        stop();
    }

    private native int start_idle(int priority);
    private native void set_priority(int id, int priority);
    private native void stop_idle(int id);
    private native static void initIDs();

    static {
        initIDs();
    }
}
