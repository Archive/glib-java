/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

import java.util.Vector;

/**
 * @deprecated This class is deprecated. Use Java's collection classes or arrays
 *             instead.
 * 
 * Wrapper class for glib's <code>GList</code> structure, specialized to hold
 * only lists of strings. This class provides a way to translate between
 * <code>GList</code> and native Java arrays and collections. For instances
 * initially constructed at the Java layer, the class manages all of the memory
 * pointed to by the GLib layer, so, in that case, if an instance of this class
 * gets gc'ed then the GLib memory will be reclaimed. Some instances of this
 * class have their origin in the GLib layer, however. For these instances, no
 * GLib memory is reclaimed when the Java instance gets collected.
 * 
 * <p>
 * This class is not intended to provide all of the functionality of
 * <code>GList</code> directly. If, for example, you are interested in
 * interactively inserting or deleting elements, or iterating over the elements,
 * you should do such things with Java objects, translating to and from
 * instances of this class as needed.
 * </p>
 * 
 * <p>
 * The Java-Gnome bindings library is free software distributed under the terms
 * of the GNU Library General Public License version 2.
 * </p>
 * 
 * @author Dan Bornstein, danfuzz@milk.com
 * @author Copyright 2000 Dan Bornstein, all rights reserved.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.GListString</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public final class GListString extends GObject {
    /**
     * <code>true</code> if the nativepeer's memory should be reclaimed when
     * this instance becomes garbage, <code>false</code> otherwise
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private boolean javaOwnsMemory;

    // ------------------------------------------------------------------------
    // constructors

    /**
     * Construct an instance. It initially contains no elements.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GListString() {
        // allow the native code to store a <code>null</code> pointer
        super(new0());
        javaOwnsMemory = true;

    }

    /**
     * Construct an instance initially containing the elements of the given
     * vector.
     * 
     * @param vec
     *            the vector containing the elements to put in the instance
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GListString(Vector vec) {
        this();

        int sz = vec.size();
        for (int i = 0; i < sz; i++) {
            append(vec.elementAt(i).toString());
        }
    }

    /**
     * Construct an instance initially containing the elemnts of the given
     * array.
     * 
     * @param list
     *            The Array containing the elements to put in this instance.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GListString(String[] list) {
        this();

        for (int i = 0; i < list.length; i++) {
            append(list[i]);
        }
    }

    // ------------------------------------------------------------------------
    // public instance methods

    /**
     * Finalize this instance. This method should never be called, other than by
     * the Java virtual machine.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected void finalize() throws Throwable {
        try {
            if (javaOwnsMemory) {
                finalize0(getHandle());
            }
        }

        finally {
            super.finalize();
        }
    }

    // FIXME: should at least include public methods corresponding to
    // these functions:
    // g_list_prepend
    // g_list_remove
    // g_list_nth_data
    // and methods which return freshly allocated instances of string[]
    // and Vector (and with 1.2, ArrayList) for all of the items in an
    // instance

    // ------------------------------------------------------------------------
    // native public declarations

    /**
     * Append a string to the end of this instance.
     * 
     * @param s
     *            the string to append
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void append(String s) {
        setHandle(append0(getHandle(), s));
    }

    // ------------------------------------------------------------------------
    // native private declarations

    /**
     * Initialize an instance. Since empty <code>GList</code>s are
     * represented by a C <code>NULL</code>, then this actually doesn't do
     * much. However, there <i>are</i> some weird platforms where
     * <code>NULL</code> isn't actually a zero value. We aren't taking any
     * chances here, so we let the platform do the platform thing.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private static native Handle new0();

    /**
     * Free the memory associated with the native peer. This is only ever called
     * when {@link #javaOwnsMemory} is <code>true</code> for the instance
     * being finalized.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    private native void finalize0(Handle handle);

    /**
     * Append a string to this instance, where the string is represented as an
     * array of <code>byte</code>s representing character values in the
     * default encoding.
     * 
     * @param str
     *            The string, represented in ISO-8859-1
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native Handle append0(Handle handle, String str);

}
