/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;


/**
 * @deprecated
 */
public class SpawnError extends Enum {
    /***************************************************************************
     * BEGINNING OF GENERATED CODE
     **************************************************************************/
    static final private int _FORK = 0;

    static final public org.gnu.glib.SpawnError FORK = new org.gnu.glib.SpawnError(
            _FORK);

    static final private int _READ = 1;

    static final public org.gnu.glib.SpawnError READ = new org.gnu.glib.SpawnError(
            _READ);

    static final private int _CHDIR = 2;

    static final public org.gnu.glib.SpawnError CHDIR = new org.gnu.glib.SpawnError(
            _CHDIR);

    static final private int _ACCES = 3;

    static final public org.gnu.glib.SpawnError ACCES = new org.gnu.glib.SpawnError(
            _ACCES);

    static final private int _PERM = 4;

    static final public org.gnu.glib.SpawnError PERM = new org.gnu.glib.SpawnError(
            _PERM);

    static final private int _BIG2 = 5;

    static final public org.gnu.glib.SpawnError BIG2 = new org.gnu.glib.SpawnError(
            _BIG2);

    static final private int _NOEXEC = 6;

    static final public org.gnu.glib.SpawnError NOEXEC = new org.gnu.glib.SpawnError(
            _NOEXEC);

    static final private int _NAMETOOLONG = 7;

    static final public org.gnu.glib.SpawnError NAMETOOLONG = new org.gnu.glib.SpawnError(
            _NAMETOOLONG);

    static final private int _NOENT = 8;

    static final public org.gnu.glib.SpawnError NOENT = new org.gnu.glib.SpawnError(
            _NOENT);

    static final private int _NOMEM = 9;

    static final public org.gnu.glib.SpawnError NOMEM = new org.gnu.glib.SpawnError(
            _NOMEM);

    static final private int _NOTDIR = 10;

    static final public org.gnu.glib.SpawnError NOTDIR = new org.gnu.glib.SpawnError(
            _NOTDIR);

    static final private int _LOOP = 11;

    static final public org.gnu.glib.SpawnError LOOP = new org.gnu.glib.SpawnError(
            _LOOP);

    static final private int _TXTBUSY = 12;

    static final public org.gnu.glib.SpawnError TXTBUSY = new org.gnu.glib.SpawnError(
            _TXTBUSY);

    static final private int _IO = 13;

    static final public org.gnu.glib.SpawnError IO = new org.gnu.glib.SpawnError(
            _IO);

    static final private int _NFILE = 14;

    static final public org.gnu.glib.SpawnError NFILE = new org.gnu.glib.SpawnError(
            _NFILE);

    static final private int _MFILE = 15;

    static final public org.gnu.glib.SpawnError MFILE = new org.gnu.glib.SpawnError(
            _MFILE);

    static final private int _INVAL = 16;

    static final public org.gnu.glib.SpawnError INVAL = new org.gnu.glib.SpawnError(
            _INVAL);

    static final private int _ISDIR = 17;

    static final public org.gnu.glib.SpawnError ISDIR = new org.gnu.glib.SpawnError(
            _ISDIR);

    static final private int _LIBBAD = 18;

    static final public org.gnu.glib.SpawnError LIBBAD = new org.gnu.glib.SpawnError(
            _LIBBAD);

    static final private int _FAILED = 19;

    static final public org.gnu.glib.SpawnError FAILED = new org.gnu.glib.SpawnError(
            _FAILED);

    static final private org.gnu.glib.SpawnError[] theInterned = new org.gnu.glib.SpawnError[] {
            FORK, READ, CHDIR, ACCES, PERM, BIG2, NOEXEC, NAMETOOLONG, NOENT,
            NOMEM, NOTDIR, LOOP, TXTBUSY, IO, NFILE, MFILE, INVAL, ISDIR,
            LIBBAD, FAILED }

    ;

    static private java.util.Hashtable theInternedExtras;

    static final private org.gnu.glib.SpawnError theSacrificialOne = new org.gnu.glib.SpawnError(
            0);

    static public org.gnu.glib.SpawnError intern(int value) {
        if (value < theInterned.length) {
            return theInterned[value];
        }
        theSacrificialOne.value_ = value;
        if (theInternedExtras == null) {
            theInternedExtras = new java.util.Hashtable();
        }
        org.gnu.glib.SpawnError already = (org.gnu.glib.SpawnError) theInternedExtras
                .get(theSacrificialOne);
        if (already == null) {
            already = new org.gnu.glib.SpawnError(value);
            theInternedExtras.put(already, already);
        }
        return already;
    }

    private SpawnError(int value) {
        value_ = value;
    }

    public org.gnu.glib.SpawnError or(org.gnu.glib.SpawnError other) {
        return intern(value_ | other.value_);
    }

    public org.gnu.glib.SpawnError and(org.gnu.glib.SpawnError other) {
        return intern(value_ & other.value_);
    }

    public org.gnu.glib.SpawnError xor(org.gnu.glib.SpawnError other) {
        return intern(value_ ^ other.value_);
    }

    public boolean test(org.gnu.glib.SpawnError other) {
        return (value_ & other.value_) == other.value_;
    }

    /***************************************************************************
     * END OF GENERATED CODE
     **************************************************************************/
}
