/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

/**
 * Interface used as a callback for property notification events ("notify"
 * signal). The notify signal is emitted on an object when one of its properties
 * has been changed. Note that getting this signal doesn't guarantee that the
 * value of the property has actually changed, it may also be emitted when the
 * setter for the property is called to reinstate the previous value.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.PropertyNotificationListener</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public interface PropertyNotificationListener {

    /**
     * Handle a property notification event.
     * 
     * @param obj
     *            The object which received the signal.
     * @param property
     *            The name of the property which changed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void notify(GObject obj, String property);
}
