/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * An EventMap is a Java-GNOME implementation class which encapsulates all
 * static information that maps GTK signals to Java-GNOME events. There should
 * be one private static EventMap for each Java-GNOME class which fires events.
 * 
 * Besides reducing the code needed to add event handling to a widget instance,
 * this mapping is needed by libglade to look up which event is associated with
 * which signal for a given class. It should not be used by client code,
 * however.
 * 
 * @author Tom Ball
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class does not have an equivalent in java-gnome 4.0,
 */
public class EventMap {

    private HashMap events = new HashMap();

    private class Entry {
        String signal;

        String method;

        EventType eventType;

        Class listenerClass;

        HashMap handlerIDs;

        Entry(String s, String m, EventType t, Class l) {
            signal = s;
            method = m;
            eventType = t;
            listenerClass = l;
        }

        synchronized void addHandlerID(Object source, int handlerID) {
            if (handlerIDs == null) {
                handlerIDs = new HashMap();
            }
            handlerIDs.put(source, new Integer(handlerID));
        }

        int getHandlerID(Object source) {
            return ((Integer) handlerIDs.get(source)).intValue();
        }
    }

    public void addEvent(String signal, String method, EventType eventType,
            Class listenerClass) {
        // do not add events with null event types
        if (null == eventType) {
            System.out.println("Signal " + signal
                    + " has a null event type.  Please fix!");
            return;
        }
        events.put(eventType, new Entry(signal, method, eventType,
                listenerClass));
    }

    /**
     * Adds an event to the EventMap. This is a convenience method that uses
     * {@link #addEvent(String, String, EventType, Class)} internally. It gets
     * the signal name by calling {@link EventType#getName()} and it expects the
     * callback method to be named in a very specific format. It should start
     * with 'handle' and then be followed by the signal name in camel case
     * format. <br>
     * <br>
     * 
     * For example, if the signal name is 'file-activated', the generated method
     * name will be 'handleFileActivated'.
     * 
     * @param eventType
     * @param listenerClass
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addEvent(EventType eventType, Class listenerClass) {
        if (eventType == null) {
            throw new IllegalArgumentException("eventType cannot be null.");
        }
        String method = eventType.getName();
        StringTokenizer st = new StringTokenizer(method, "-");
        method = "handle";
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            method += token.substring(0, 1).toUpperCase() + token.substring(1);
        }
        addEvent(eventType.getName(), method, eventType, listenerClass);
    }

    /**
     * Connect all event handlers to this event source.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void initialize(GObject source) {
        Iterator i = events.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            Entry e = (Entry) me.getValue();
            e.addHandlerID(source, source.addEventHandler(e.signal, e.method,
                    source));
        }
    }

    /**
     * Connect an event handler of the type provided.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void initialize(GObject source, EventType type) {
        initialize(source, type, true);
    }

    /**
     * Connect an event handler of the type provided.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void initialize(GObject source, EventType type, boolean shouldCopyIfBoxed) {
        Entry e = (Entry) events.get(type);
        if (null == e)
            return;
        e.addHandlerID(source, source.addEventHandler(e.signal, e.method,
                source, shouldCopyIfBoxed));
    }

    /**
     * Disconnect an event handler of the type provided.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void uninitialize(GObject source, EventType type) {
        Entry e = (Entry) events.get(type);
        if (null == e)
            return;
        source.removeEventHandler(e.getHandlerID(source));
    }

    /**
     * Return the event listener class for a given signal.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Class getEventListenerClass(String signal) {
        Class rv = null;
        Iterator i = events.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            Entry e = (Entry) me.getValue();
            if (e.signal.equals(signal))
                rv = e.listenerClass;
        }
        return rv;
    }

    /**
     * Get the event type for a given signal.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventType getEventType(String signal) {
        EventType rv = null;
        Iterator i = events.entrySet().iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            Entry e = (Entry) me.getValue();
            if (e.signal.equals(signal))
                rv = e.eventType;
        }
        return rv;
    }
}
