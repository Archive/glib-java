/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.glib;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.ListIterator;

/**
 * Base class for all objects that participate in the GLib object system.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Object</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class GObject extends Struct {

    // private static final Logger log =
    // Logger.getLogger("org.gnu.glib.GObject");

    /** Event initialization marker. */
    protected boolean eventsInitialized = false;

    /** Java-side properties container. */
    private Hashtable properties = null;

    /** List of PropertyNotificationListeners. */
    private ArrayList notifyListeners = null;

    /** Notification freeze marker. */
    private boolean freezeNotify = false;

    /** Queue of frozen notifications. */
    private ArrayList frozenNotifies = null;

    /**
     * Create an uninitialized instance. This has potential uses for derived
     * classes.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected GObject() {
    }

    /**
     * Create a new GObject of the given type.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GObject(Type type) {
        super(g_object_new(type.getTypeHandle()));
    }

    /**
     * Create a new GObject with a handle to a native resource returned from a
     * call to the native libraries.
     * 
     * @param handle
     *            The handle that represents a pointer to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GObject(Handle handle) {
        super(handle);
    }

    /**
     * Connect a event to a callback method "func" in object "cbrecv".
     * 
     * @param name
     *            The name of the event to map.
     * @param func
     *            The name of the callback method.
     * @param cbrecv
     *            The name of the object that is to recieve the event.
     * @return The handle id of the event that can be used in a call to
     *         removeEventHandler.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int addEventHandler(String name, String func, Object cbrecv) {
        return addEventHandler(name, func, cbrecv, true);
    }

    /**
     * Connect an event to a callback method that has the same name as the
     * event. For example, the event "clicked" will be mapped to a method
     * "clicked()" in the object cbrecv.
     * 
     * @param name
     *            The name of the event to map.
     * @param cbrecv
     *            The name of the object that is to recieve the event.
     * @return The handle id of the event that can be used in a call to
     *         removeEventHandler.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native int addEventHandler(String name, Object cbrecv);

    /**
     * Connect a event to a callback method that has the same name as the event.
     * For example, the event "clicked" will be mapped to a method "clicked()"
     * in the object cbrecv.
     * 
     * @param name
     *            The name of the event to map.
     * @param cbrecv
     *            The name of the object that is to recieve the event.
     * @param data
     *            User defined data that will be passed to the callback.
     * @return The handle id of the event that can be used in a call to
     *         removeEventHandler.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native int addEventHandler(String name, Object cbrecv, Object data);

    /**
     * Connect a event to a callback method "func" in object "cbrecv".
     * 
     * @param name
     *            The name of the event to map.
     * @param func
     *            The name of the callback method.
     * @param cbrecv
     *            The name of the object that is to recieve the event.
     * @param shouldCopyIfBoxed
     *            Should copy the incoming object associated with the signal.
     *            Only for type Boxed.
     * @return The handle id of the event that can be used in a call to
     *         removeEventHandler.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native int addEventHandler(String name, String func, Object cbrecv,
            boolean shouldCopyIfBoxed);

    /**
     * Connect a event to a callback method "func" in object "cbrecv".
     * 
     * @param name
     *            The name of the event to map.
     * @param func
     *            The name of the callback method.
     * @param cbrecv
     *            The name of the object that is to recieve the event.
     * @param data
     *            User defined data that will be passed to the callback.
     * @return The handle id of the event that can be used in a call to
     *         removeEventHandler.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native int addEventHandler(String name, String func, Object cbrecv,
            Object data);

    /**
     * Connect a event to a static callback method "func" in Class "cbrecv".
     * 
     * @param name
     *            The name of the event to map.
     * @param func
     *            The name of the static callback method.
     * @param cbrecv
     *            The name of the class that is to recieve the event.
     * @return The handle id of the event that can be used in a call to
     *         removeEventHandler.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native int addEventHandler(String name, String func, Class cbrecv);

    /**
     * Connect a event to a static callback method "func" in Class "cbrecv".
     * 
     * @param name
     *            The name of the event to map.
     * @param func
     *            The name of the static callback method.
     * @param cbrecv
     *            The name of the class that is to recieve the event.
     * @param data
     *            User defined data that will be passed to the callback.
     * @return The handle id of the event that can be used in a call to
     *         removeEventHandler.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native int addEventHandler(String name, String func, Class cbrecv,
            Object data);

    /**
     * Disconnect a event from its' callback method.
     * 
     * @param handler
     *            The handler id of the event. This is the value returned from a
     *            call to addEventHandler().
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public native void removeEventHandler(int handler);

    /**
     * Gets the Java <tt>Object</tt> associated with the given key from this
     * object's object association table.
     * 
     * @param key
     *            The association key.
     * @return The Java <tt>Object</tt> associated with this key or
     *         <tt>null</tt> if the named association key does not exist.
     * @see #setData(String,Object)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Object getData(String key) {
        return getData(getHandle(), key);
    }

    /**
     * Sets an association from a String to a Java <tt>Object</tt> to be
     * stored in the GTK object's table of associations. If the object already
     * had an association with the given key, the old association will be
     * destroyed.
     * 
     * @param key
     *            The association key.
     * @param data
     *            The Java <tt>Object</tt> to associate with the given key.
     * @see #getData(String)
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setData(String key, Object data) {
        if (key == null) {
            throw new IllegalArgumentException("key is null.");
        }
        setData(getHandle(), key, data);
    }

    public Class getEventListenerClass(String signal) {
        return null;
    }

    public EventType getEventType(String signal) {
        return null;
    }

    /**
     * Set the given property (<tt>name</tt>) with the given {@link
     * org.gnu.glib.Value} on this object. If there is a GTK property with this
     * name, the GTK property will be set. Otherwise, a Java-side property will
     * be set.
     * <p>
     * <b>NOTE</b>: Prefer using one of the convenience methods over using this
     * method directly.
     * 
     * @param name
     *            The name of the property to set.
     * @param val
     *            The value to set in the property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setProperty(String name, Value val) {
        if (hasGtkProperty(name)) {
            g_object_set_property(getHandle(), name, val.getHandle());
        } else {
            setInternProperty(name, val);
            notifyIntern(name);
        }
    }

    /**
     * Convenience method for setting <tt>int</tt> properties.
     * 
     * @param name
     *            The name of the property to set.
     * @param value
     *            The value to set in the property.
     * @see #setProperty( String, Value )
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setIntProperty(String name, int value) {
        Value val = new Value(Type.INT());
        val.setInteger(value);
        setProperty(name, val);
    }

    /**
     * Convenience method for setting <tt>String</tt> properties.
     * 
     * @param name
     *            The name of the property to set.
     * @param value
     *            The value to set in the property.
     * @see #setProperty( String, Value )
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setStringProperty(String name, String value) {
        Value val = new Value(Type.STRING());
        val.setString(value);
        setProperty(name, val);
    }

    /**
     * Convenience method for setting <tt>boolean</tt> properties.
     * 
     * @param name
     *            The name of the property to set.
     * @param value
     *            The value to set in the property.
     * @see #setProperty( String, Value )
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setBooleanProperty(String name, boolean value) {
        Value val = new Value(Type.BOOLEAN());
        val.setBoolean(value);
        setProperty(name, val);
    }

    /**
     * Convenience method for setting <tt>float</tt> properties.
     * 
     * @param name
     *            The name of the property to set.
     * @param value
     *            The value to set in the property.
     * @see #setProperty( String, Value )
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setFloatProperty(String name, float value) {
        Value val = new Value(Type.FLOAT());
        val.setFloat(value);
        setProperty(name, val);
    }

    /**
     * Convenience method for setting <tt>long</tt> properties.
     * 
     * @param name
     *            The name of the property to set.
     * @param value
     *            The value to set in the property.
     * @see #setProperty( String, Value )
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setLongProperty(String name, long value) {
        Value val = new Value(Type.LONG());
        val.setLong(value);
        setProperty(name, val);
    }

    /**
     * Convenience method for setting <tt>double</tt> properties.
     * 
     * @param name
     *            The name of the property to set.
     * @param value
     *            The value to set in the property.
     * @see #setProperty( String, Value )
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setDoubleProperty(String name, double value) {
        Value val = new Value(Type.DOUBLE());
        val.setDouble(value);
        setProperty(name, val);
    }

    /**
     * Convenience method for setting <tt>Object</tt> properties.
     * 
     * @param name
     *            The name of the property to set.
     * @param value
     *            The value to set in the property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void setJavaObjectProperty(String name, Object value) {
        Value val = null;
        if (hasGtkProperty(name)) {
            Type type = new Type(getGTypeOfProperty(getHandle(), name));
            val = new Value(type);
        } else {
            val = new Value(Type.OBJECT());
        }
        val.setJavaObject(value);
        setProperty(name, val);
    }

    /**
     * Get the {@link org.gnu.glib.Value} of the given property (<tt>name</tt>).
     * If there is a GTK property with this name, the GTK property will be
     * retrived. Otherwise, a Java-side property will be returned.
     * <p>
     * <b>NOTE</b>: Prefer using one of the convenience methods over using this
     * method directly.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Value getProperty(String name) {
        if (hasGtkProperty(name)) {
            return new Value(g_object_get_property(getHandle(), name));
        } else {
            return getInternProperty(name);
        }
    }

    /**
     * Convenience method for retrieving <tt>int</tt> properties.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int getIntProperty(String name) {
        Value val = getProperty(name);
        if (val != null) {
            return val.getInt();
        } else {
            return 0;
        }
    }

    /**
     * Convenience method for retrieving <tt>String</tt> properties.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String getStringProperty(String name) {
        Value val = getProperty(name);
        if (val != null) {
            return val.getString();
        } else {
            return null;
        }
    }

    /**
     * Convenience method for retrieving <tt>boolean</tt> properties.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean getBooleanProperty(String name) {
        Value val = getProperty(name);
        if (val != null) {
            return val.getBoolean();
        } else {
            return false;
        }
    }

    /**
     * Convenience method for retrieving <tt>float</tt> properties.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public float getFloatProperty(String name) {
        Value val = getProperty(name);
        if (val != null) {
            return (float) val.getFloat();
        } else {
            return 0f;
        }
    }

    /**
     * Convenience method for retrieving <tt>long</tt> properties.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public long getLongProperty(String name) {
        Value val = getProperty(name);
        if (val != null) {
            return val.getLong();
        } else {
            return 0;
        }
    }

    /**
     * Convenience method for retrieving <tt>double</tt> properties.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public double getDoubleProperty(String name) {
        Value val = getProperty(name);
        if (val != null) {
            return val.getDouble();
        } else {
            return 0.0;
        }
    }

    /**
     * Convenience method for retrieving <tt>Object</tt> properties.
     * 
     * @param name
     *            The name of the property to retrieve.
     * @return The value of the given property.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Object getJavaObjectProperty(String name) {
        Value val = getProperty(name);
        if (val == null) {
            return null;
        }

        Object obj = val.getJavaObject();
        if (obj == null) {
            return null;
        }

        if (obj instanceof Handle) {
            // I would like to use:
            // GObject gobj = GObject.getGObjectFromHandle( (Handle)obj );
            //
            // But this seems to crash the JVM in getData, so for now we
            // always instantiate a new object.
            int type = getGTypeOfProperty(getHandle(), name);
            return instantiateJGObjectFromGType(type, (Handle) obj);

        } else {
            return obj;
        }
    }

    /**
     * Determines if this object supports the given named property. First the
     * GTK properties are checked, then, if no property with this name is found,
     * the Java-side properties are checked.
     * 
     * @param name
     *            The property to verify.
     * @return True if the given name is a property of this object, false
     *         otherwise.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean hasProperty(String name) {
        if (hasGtkProperty(name)) {
            return true;
        }
        return hasInternProperty(name);
    }

    /**
     * Add a listener that will be activated when a property is updated updated
     * ("notify" signal). The notify signal is emitted on an object when one of
     * its properties has been changed. Note that getting this signal doesn't
     * guarantee that the value of the property has actually changed, it may
     * also be emitted when the setter for the property is called to reinstate
     * the previous value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void addListener(PropertyNotificationListener listen) {
        if (notifyListeners == null) {
            notifyListeners = new ArrayList();
            connectNotifySignal(getHandle(), this, "handlePropertyNotify");
        }
        notifyListeners.add(listen);
    }

    /**
     * Remove the given listener from those activated when a property is
     * updated.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeListener(PropertyNotificationListener listen) {
        int index = notifyListeners.indexOf(listen);
        if (index != -1) {
            notifyListeners.remove(index);
        }
        if (notifyListeners.size() == 0) {
            disconnectNotifySignal(getHandle());
        }
    }

    /**
     * Stops emission of "notify" signals on object. The signals are queued
     * until {@link #thawNotify} is called.
     * 
     * <p>
     * This is necessary for accessors that modify multiple properties to
     * prevent premature notification while the object is still being modified.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void freezeNotify() {
        g_object_freeze_notify(getHandle());
        freezeNotify = true;
    }

    /**
     * Emits a "notify" signal for the given property. The property can be a GTK
     * native property or a Java-side property. If no property exist by the
     * given name, this method does nothing.
     * 
     * @param property_name
     *            The name of a property installed on the class of this object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void notify(String property_name) {
        if (hasGtkProperty(property_name)) {
            g_object_notify(getHandle(), property_name);
        } else {
            if (!freezeNotify) {
                notifyIntern(property_name);
            } else {
                if (frozenNotifies == null) {
                    frozenNotifies = new ArrayList();
                }
                frozenNotifies.add(property_name);
            }
        }
    }

    /**
     * Reverts the effect of a previous call to {@link #freezeNotify}. This
     * causes all queued "notify" signals on object to be emitted.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void thawNotify() {
        // Gtk thawing.
        g_object_thaw_notify(getHandle());
        // Java-side thawing.
        freezeNotify = false;
        if (frozenNotifies != null) {
            ListIterator it = frozenNotifies.listIterator();
            while (it.hasNext()) {
                notifyIntern((String) it.next());
            }
        }
    }

    /**
     * @deprecated This method is deprecated in favor of
     *             {@link #getGObjectFromHandle}.
     * 
     * @see #getGObjectFromHandle
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected GObject retrieveGObject(Handle hndl) {
        return getGObjectFromHandle(hndl);
    }

    /**
     * Gets a <tt>GObject</tt> instance for the given <tt>Handle</tt>. If
     * no Java object currently exists for the given <tt>Handle</tt>, this
     * method will return <tt>null</tt>. You should then instantiate the
     * required Java class using the class' handle-constructor. For example:
     * 
     * <pre>
     * // Get a Handle from somewhere (typically as a parameter to a method
     * // used as a callback and invoked from the C JNI side).
     * SomeGtkClass finalobj = null;
     * GObject obj = GObject.getGObjectFromHandle(handle);
     * if (obj == null) {
     *     finalobj = new SomeGtkClass(handle);
     * } else {
     *     finalobj = (SomeGtkClass) obj;
     * }
     * </pre>
     * 
     * NOTE: This is for internal use only and should never need to be used in
     * application code.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public static GObject getGObjectFromHandle(Handle hndl) {
        return (GObject) hndl.getProxiedObject();
    }

    protected void finalize() throws Throwable {
        // log.fine(getClass().getName());

        // If an exception is thrown in the constructor of a subclass, then
        // an object with a null handle may be finalized at some point. Don't
        // run nativeFinalize for those objects.
        try {
            if (getHandle() != null) {
                nativeFinalize(getHandle());
            }
        } finally {
            super.finalize();
        }
    }

    // 
    // Internal use methods.
    //

    private static void printStackTrace(String message) {
        (new Exception(message)).printStackTrace();
    }

    private void setInternProperty(String key, Value val) {
        if (properties == null) {
            properties = new Hashtable();
        }
        properties.put(key, val);
    }

    private Value getInternProperty(String key) {
        if (properties != null) {
            return (Value) properties.get(key);
        }
        return null;
    }

    private boolean hasGtkProperty(String key) {
        Handle hndl = g_object_class_find_property(getHandle(), key);
        return !hndl.isNull();
    }

    private boolean hasInternProperty(String key) {
        if (properties != null) {
            return properties.get(key) != null;
        }
        return false;
    }

    private void notifyIntern(String key) {
        if (properties != null) {
            Value val = (Value) properties.get(key);
            if (val != null) {
                handlePropertyNotify(getHandle(), key);
            }
        }
    }

    private void handlePropertyNotify(Handle gobject, String propname) {
        GObject obj = getGObjectFromHandle(gobject);

        PropertyNotificationListener listen = null;
        if (notifyListeners == null)
            notifyListeners = new ArrayList();
        ListIterator it = notifyListeners.listIterator();
        while (it.hasNext()) {
            listen = (PropertyNotificationListener) it.next();
            listen.notify(obj, propname);
        }
    }

    public static final void collect() {
        nativeCollect();
    }

    native static final void g_object_ref(Handle handle);

    native static final void g_object_unref(Handle handle);

    native static final protected Object getData(Handle handle, String key);

    native static final protected void setData(Handle handle, String key,
            Object data);

    native static final private void registerLogHandler();

    native static final private Handle g_object_new(int object_type);

    native static final private Handle g_object_class_find_property(
            Handle oclass, String property_name);

    native static final private void g_object_set_property(Handle object,
            String property_name, Handle value);

    native static final private Handle g_object_get_property(Handle object,
            String property_name);

    native static final private void g_object_freeze_notify(Handle object);

    native static final private void g_object_notify(Handle object,
            String property_name);

    native static final private void g_object_thaw_notify(Handle object);

    native static final private void connectNotifySignal(Handle objhandle,
            GObject object, String callback);

    native static final private void disconnectNotifySignal(Handle object);

    native static final private int getGTypeOfProperty(Handle object,
            String prop);

    // Perhaps this could/should be public in the future??
    native static final protected Object instantiateJGObjectFromGType(int type,
            Handle handle);

    /**
     * Return the int to which the native {@link org.gnu.glib.Handle} holds a
     * pointer. The native Handle is assumed to hold a C pointer to an int (
     * gint* ). This method dereferences that pointer and returns the value to
     * which it points as an integer.
     * 
     * @param handle
     *            The native Handle holding a pointer to an int.
     * @return The int that is pointed to by the native Handle.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    native static final protected int getIntFromHandle(Handle handle);

    /**
     * Return the String to which the native {@link org.gnu.glib.Handle} holds a
     * pointer. The native Handle is assumed to hold a C pointer to a String
     * (char* ). This method dereferences that pointer and returns the value to
     * which it points as a String.
     * 
     * @param handle
     *            The native Handle holding a pointer to a String.
     * @return The String that is pointed to by the native Handle.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    native static final protected String getStringFromHandle(Handle handle);

    native static final private void nativeCollect();

    native static final private void nativeFinalize(Handle handle);

    native static final private void init();

    // native static final public void setTestProperty(Handle object, String
    // prop, Handle obj);

    // ------------------------------------------------------------------------
    // static init code
    static {
        registerLogHandler();
        init();
    }

}
