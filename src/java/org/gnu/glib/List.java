/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;


/**
 * @deprecated This class is deprecated. Use Java's collection classes or arrays
 *             instead.
 * 
 * This object represents a doubly-linked list in the GLib system. This should
 * only be used internally. This type should be converted into one of the Java
 * container types prior to being passed to the application layer. Objects of
 * this type should be freed by calling thre free() method. The <strong>data</strong>
 * parameter that is being passed into many of the methods is the handle of a
 * java-gnome object.
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.List</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class List extends Struct {

    /**
     * Construct a List object
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public List() {
        super(List.g_list_alloc());
    }

    /**
     * Contruct a List object using a given handle. This constructor is used by
     * the libraries internally to construct the object with the handle returned
     * by a native method.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public List(Handle handle) {
        super(handle);
    }

    /**
     * Release the resources associated with this object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void free() {
        List.g_list_free(getHandle());
    }

    /**
     * Append an element to the end of the list.
     * 
     * @param data
     *            The handle of the object that is being added to the List.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void append(int data) {
        setHandle(List.g_list_append(getHandle(), data));
    }

    /**
     * Prepend an element to the end of the list.
     * 
     * @param data
     *            The handle of the object that is being added to the List.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void prepend(int data) {
        setHandle(List.g_list_prepend(getHandle(), data));
    }

    /**
     * Insert an element at a specified location in the List.
     * 
     * @param data
     *            The handle of the object that is being added to the List.
     * @param position
     *            The position to perform the insertion.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void insert(int data, int position) {
        setHandle(List.g_list_insert(getHandle(), data, position));
    }

    /**
     * Remove the first instance of an element from the List.
     * 
     * @param data
     *            The item to remove from the list. If two items contain the
     *            same data only the first will be removed.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void remove(int data) {
        setHandle(List.g_list_remove(getHandle(), data));
    }

    /**
     * Remove all instances of an element from the List.
     * 
     * @param data
     *            The item to remove from the List. This method will remove all
     *            instances of the object pointed to by data.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void removeAllInstances(int data) {
        setHandle(List.g_list_remove_all(getHandle(), data));
    }

    /**
     * Return the number of elements contained in the List.
     * 
     * @return The number of elements in the List.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int length() {
        return List.g_list_length(getHandle());
    }

    /**
     * Return the first element from the List. This method will also reposition
     * the current list item to the beginning of the list.
     * 
     * @return The first element from the List.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Handle first() {
        setHandle(List.g_list_first(getHandle()));
        return List.getData(getHandle());
    }

    /**
     * Return the last element from the List. This method will also reposition
     * the current list item to the end of the list.
     * 
     * @return The last element from the List.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Handle last() {
        setHandle(List.g_list_last(getHandle()));
        return List.getData(getHandle());
    }

    /**
     * Return the next element in the List. This method will also move the
     * current list item forward one element.
     * 
     * @return The next element from the List.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Handle next() {
        setHandle(List.g_list_next(getHandle()));
        return List.getData(getHandle());
    }

    /**
     * Return the previous element in the List. This method will also move the
     * current list item backward one element.
     * 
     * @return The previous element from the List.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Handle previous() {
        setHandle(List.g_list_previous(getHandle()));
        return List.getData(getHandle());
    }

    /***************************************************************************
     * BEGINNING OF JNI CODE
     **************************************************************************/
    native static final protected Handle getData(Handle obj);

    native static final protected Handle g_list_append(Handle list, long data);

    native static final protected Handle g_list_prepend(Handle list, long data);

    native static final protected Handle g_list_insert(Handle list, long data,
            int position);

    native static final protected Handle g_list_remove(Handle list, long data);

    native static final protected Handle g_list_remove_all(Handle list, long data);

    native static final protected void g_list_free(Handle list);

    native static final protected Handle g_list_alloc();

    native static final protected int g_list_length(Handle list);

    native static final protected Handle g_list_first(Handle list);

    native static final protected Handle g_list_last(Handle list);

    native static final protected Handle g_list_previous(Handle list);

    native static final protected Handle g_list_next(Handle list);
    /***************************************************************************
     * END OF JNI CODE
     **************************************************************************/
}
