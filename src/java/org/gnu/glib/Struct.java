/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */
package org.gnu.glib;

/**
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.Struct</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class Struct {

    /** Holder for the raw object pointer. */
    private Handle handle = null;

    /**
     * Create an uninitialized instance. This has potential uses for derived
     * classes.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected Struct() {
    }

    /**
     * Create a new Struct with a handle to a native resource returned from a
     * call to the native libraries.
     * 
     * @param handle
     *            The handle that represents a pointer to a native resource.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Struct(Handle handle) {
        setHandle(handle);
    }

    /**
     * Get the raw handle value. This value should never be modified by the
     * application. It's sole use is to pass to native methods.
     * 
     * @return the handle value.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public final Handle getHandle() {
        return handle;
    }

    /**
     * Check if two objects refer to the same (native) object.
     * 
     * @param other
     *            the reference object with which to compare.
     * @return true if both objects refer to the same object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean equals(Object other) {
        return other instanceof Struct
                && handle.equals(((Struct) other).getHandle());
    }

    /**
     * Returns a hash code value for the object. This allows for using Struct as
     * keys in hashmaps.
     * 
     * @return a hash code value for the object.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public int hashCode() {
        return getHandle().hashCode();
    }

    /**
     * Sets this object's native handle.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    protected void setHandle(Handle hndl) {
        handle = hndl;
        handle.setProxiedObject(this);
    }

    /**
     * Get a native handle that refers to a null pointer.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    native static final public Handle getNullHandle();

    // ------------------------------------------------------------------------
    // static init code
    static {
        System.loadLibrary(org.gnu.glib.Config.LIBRARY_NAME
                + org.gnu.glib.Config.API_VERSION);
        init();
    }

    native static final private void init();

}
