/*
 * Java-Gnome Bindings Library
 *
 * Copyright 1998-2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;


/**
 * The MainLoop represents a main event loop. After it has been created, one
 * should call {@link #run()} to cause it to start processing the events. To
 * exit the main loop, call {@link #quit}.
 * 
 * @author Ismael Juma <ismael@juma.me.uk>
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class does not have an equivalent in java-gnome 4.0.
 */
public class MainLoop extends MemStruct {

    /**
     * Creates a new MainLoop with the default context. After creation, the
     * MainLoop is not running.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public MainLoop() {
        super(g_main_loop_new(Struct.getNullHandle(), false));
    }

    /**
     * Runs a main loop until {@link #quit()} is called on the loop.
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void run() {
        g_main_loop_run(getHandle());
    }

    /**
     * Stops the main loop from running.
     * 
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public void quit() {
        g_main_loop_quit(getHandle());
    }

    /**
     * Checks to see if the main loop is currently being run via {@link #run()}.
     * 
     * @return TRUE if the main loop is currently being run.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public boolean isRunning() {
        return g_main_loop_is_running(getHandle());
    }

    /* Native methods */

    native static private Handle g_main_loop_new(Handle context,
            boolean is_running);

    native static private void g_main_loop_run(Handle loop);

    native static private void g_main_loop_quit(Handle loop);

    native static private boolean g_main_loop_is_running(Handle loop);
}
