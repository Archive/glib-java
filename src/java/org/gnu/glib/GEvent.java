/*
 * Java-Gnome Bindings Library
 * Copyright 1998-2004 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome Team Members:
 *   Jean Van Wyk <jeanvanwyk@iname.com>
 *   Jeffrey S. Morgan <jeffrey.morgan@bristolwest.com>
 *   Dan Bornstein <danfuzz@milk.com>
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.glib;

import java.io.Serializable;

import org.gnu.glib.EventType;

/**
 * A base event class for capturing signals emitted by {@link GObject}s..
 * 
 * @author Jeffrey S. Morgan
 *
 * @deprecated This class is part of the java-gnome 2.x family of libraries,
 *             which, due to their inefficiency and complexity, are no longer
 *             being maintained and have been abandoned by the java-gnome
 *             project. This class may have an equivalent in java-gnome 4.0,
 *             see <code>org.gnome.glib.GEvent</code>.
 *             You should be aware that there is a considerably different API
 *             in the new library: the architecture is completely different
 *             and most notably internals are no longer exposed to public view.
 */
public class GEvent implements Serializable {

    /** The type of event */
    protected EventType type;

    /** The object on which the event initially occurred. */
    protected Object source;

    /**
     * Construct a GEvent object with the specified source object and type.
     * 
     * @param source
     *            the object where the event originated.
     * @param type
     *            the event type.
     * @throws IllegalArgumentException
     *             if the source object is null
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public GEvent(Object source, EventType type) {
        if (null == source)
            throw new IllegalArgumentException("null source");
        this.source = source;
        this.type = type;
    }

    /**
     * Returns the object on which the event originally occured
     * 
     * @return Object on which the event originally occured
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public Object getSource() {
        return source;
    }

    /**
     * @return The type of the event.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public EventType getType() {
        return type;
    }

    /**
     * Generates a string representation of the event. Useful for debugging
     * applications.
     * 
     * @return string representation of event.
     * @deprecated Superceeded by java-gnome 4.0; a method along these lines
     *             may well exist in the new bindings, but if it does it likely
     *             has a different name or signature due to the shift to an
     *             algorithmic mapping of the underlying native libraries.
     */
    public String toString() {
        return getClass().getName() + "[source=" + source + ",id="
                + type.getName() + "]";
    }
}
